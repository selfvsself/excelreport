package ru.centp.report;

import org.apache.poi.hssf.record.RowRecord;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class test {
    static Workbook workbook2;

    public static void main(String[] args) throws IOException {
        String filename = "C:\\webapps\\data\\reports\\test3.xlsx";
        String filename2 = "C:\\webapps\\data\\reports\\test22.xlsx";
        String filename3 = "C:\\webapps\\data\\reports\\test222.xml";
        String filename33 = "C:\\webapps\\data\\reports\\test33.xlsx";
        XSSFWorkbook workbook = readWorkbook(filename);

        InputStream inputStream = new FileInputStream("C:\\webapps\\data\\logo3.JPG");
        File file = new File("C:\\webapps\\data\\logo.png");
        byte[] b2 = Files.readAllBytes(Paths.get("C:\\webapps\\data\\logo.png"));
        System.out.println(Arrays.toString(b2));
        byte[] bytes = IOUtils.toByteArray(inputStream);
        System.out.println(Arrays.toString(bytes));
        int pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
        System.out.println(pictureIdx);

        CreationHelper helper = workbook.getCreationHelper();

        Sheet sheet = workbook.getSheetAt(0);
        Drawing drawing = sheet.createDrawingPatriarch();

        ClientAnchor anchor = helper.createClientAnchor();

        anchor.setCol1(1);
        anchor.setRow1(2);

        Picture pict = drawing.createPicture(anchor, pictureIdx);
        pict.resize();


        Row row = sheet.createRow(1);
        Cell cell = row.createCell(1);
        cell.setCellValue("test");



        saveWorkbook(filename33, workbook);
    }

    public static boolean getRowCustomHeightCustom (HSSFRow row) {
        boolean isCustomHeight = false;
        try {
            Field field = row.getClass().getDeclaredField("row");
            field.setAccessible(true);
            RowRecord rowRecord = (RowRecord) field.get(row);
            isCustomHeight = rowRecord.getBadFontHeight();
            field.setAccessible(false);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return isCustomHeight;
    }

    public static XSSFWorkbook readWorkbook(String file) {
        try (InputStream inputStream = new FileInputStream(file)) {
            return new XSSFWorkbook(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static HSSFWorkbook readHSSFWorkbook(String file) {
        try (InputStream inputStream = new FileInputStream(file)) {
            return new HSSFWorkbook(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void saveWorkbook(String fileName, Workbook workbook) {
        try (FileOutputStream outputStream = new FileOutputStream(fileName)) {
            workbook.write(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
