package ru.centp.report;

import ru.centp.report.api.loader.DataLoader;
import ru.centp.report.impl.exporter.ExportMapLoader;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class TestingRunExport {

    static final File fileMaket = new File("C:\\webapps\\data\\reports\\test_report.xlsx");

    public static void main(String[] args) {
        Map<String, String[]> params = new LinkedHashMap<>();
        params.put("columns0", new String[]{"{ROLEID_RET=Роль, LEVACCESSVIRT=Уровень доступа, DISABLEDVIRT=Запись заблокирована}"});
        params.put("params0", new String[]{"{sOrder=id, sFilterMin=null, sFilter={\"userid\":[\"1\"]}, sWhere=cid = 0 and hid is null, sFilterMax=null}"});
        params.put("columns1", new String[]{"{LOGIN=Логин, NAME=Наименование, PASSWORD_EXPIRE=Срок действия пароля, DISABLED=Запись заблокирована, LEVACCESSID_RET=Уровень доступа, FAMILYNAME=Фамилия, FIRSTNAME=Имя, LASTNAME=Отчество, POST=Должность, PHONE=Телефон, EMAIL=E-Mail}"});
        params.put("params1", new String[]{"{sOrder=id, sFilterMin=null, sFilter={\"id\":[\"1\"]}, sWhere=cid = 0 and hid is null, sFilterMax=null}"});



//        ExportMapLoader exportMapLoader = new ExportMapLoader(params);
//        exportMapLoader.getContent();
//        exportMapLoader.saveWorkbook("C:\\webapps\\data\\reports\\test3.xlsx");
//        XLSXReporting reporting = new XLSXReporting(dataLoader, file);
//        reporting.getOutputReport();
//        reporting.saveWorkbook("C:\\webapps\\data\\reports\\test3.xlsx");


    }
}
