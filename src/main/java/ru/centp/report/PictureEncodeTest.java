package ru.centp.report;

import org.postgresql.ds.PGPoolingDataSource;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.Arrays;
import java.util.Base64;

public class PictureEncodeTest {

    public static void main(String[] args) throws IOException, SQLException {
        PGPoolingDataSource source = new PGPoolingDataSource();
        source.setDataSourceName("testPool");
        source.setServerName("127.0.0.1:5432");
        source.setDatabaseName("vegaplus");
        source.setUser("magicbox");
        source.setPassword("magicbox");
        source.setMaxConnections(10);

        File file = new File("C:\\webapps\\data\\logo.png");
        byte[] bytes = Files.readAllBytes(file.toPath());
        System.out.println(Arrays.toString(bytes));
        String str = new String(bytes);
        String str2 = "\\x"+bytesToHex(bytes);
        System.out.println(str2);

        Connection connection = source.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("update users set photo=?::bytea where id=3");
        preparedStatement.setString(1, str2);
        preparedStatement.executeUpdate();

    }

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }
}
