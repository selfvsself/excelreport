package ru.centp.report;

import ru.centp.report.impl.exporter.ImportMapLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class TestImporter {

    public static void main(String[] args) {
        File file = new File("C:\\webapps\\data\\reports\\import_test.xlsx");
        List<String> tableList = new ArrayList<>();
        tableList.add("conv");
        tableList.add("convacts");
        tableList.add("convactparams");
//        tableList.add("exec_bud");
//        tableList.add("exec_bud_kbr");
//        tableList.add("jurnalexp_record_types_kbr");
//        tableList.add("jurnalexp_for_exec_bud_kbr");
        try {
            InputStream inputStream = new FileInputStream(file);
            ImportMapLoader importMapLoader = new ImportMapLoader(inputStream, file.getName());
            String json = importMapLoader.getJsonQuery();
            System.out.println(json);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
