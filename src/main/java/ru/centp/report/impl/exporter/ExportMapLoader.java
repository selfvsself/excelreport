package ru.centp.report.impl.exporter;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import ru.centp.report.api.exporter.ExportToExcel;
import ru.centp.report.impl.model.ColumnItem;
import ru.centp.report.impl.model.ColumnType;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExportMapLoader implements ExportToExcel {

    private static final String DOWNLOAD_LEVEL_SELECT = "select";
    private static final String TABLE_AND_COLUMN_SEPARATOR = "@_";
    private static final int NUM_TABLE_CAPTION_ROW = 0;
    private static final int NUM_COLUMN_CAPTION_ROW = 1;
    private static final int NUM_COLUMN_ADVANCED_INFO_ROW = 2;

    private List<Map<String, String>> itemsList;
    private XSSFWorkbook workbook;
    private String sheetName = "Book 1";
    private XSSFCellStyle headerCellStyle;
    private XSSFCellStyle itemCellStyle, numberItemCellStyle;
    private Map<String, String> tablesCaption;
    private Map<String, Map<String, ColumnItem>> allColumn = new LinkedHashMap<>();
    private final boolean isOneGridExport; //Если выгружаем один грид, для выгрузки всех записей без фильтра выбраной записи в родительских гридах

    //private final Logger logger = Logger.getLogger(getClass().getName());

    public ExportMapLoader(List<Map<String, String>> params, String downloadLevel) {
        tablesCaption = new HashMap<>();
        // Загружаем колонки, они хранятся последнией мапой
        fillVisibleColumn(params.remove(params.size() - 1));
        isOneGridExport = params.size() == 1;
        if (params.size() > 0) {
            //Загружаем грид на котором запустиил действие (самый младший)
            if (downloadLevel.equalsIgnoreCase(DOWNLOAD_LEVEL_SELECT))
                itemsList = getSelectedItems(params);
            else
                itemsList = getAllItems(params);
        }
    }

    private void fillVisibleColumn(Map<String, String> columnMap) {
        for (String key : columnMap.keySet()) {
            String columnId = key.substring(0, key.indexOf(TABLE_AND_COLUMN_SEPARATOR)).toLowerCase();
            String tableId = key.substring(key.indexOf(TABLE_AND_COLUMN_SEPARATOR) + 2).toLowerCase();
            String value = columnMap.get(key);
            ColumnItem columnItem = new ColumnItem(columnId, tableId, true);
            columnItem.setCaption(value);
            allColumn.putIfAbsent(tableId, new LinkedHashMap<>());
            allColumn.get(tableId).put(columnId, columnItem);
        }
    }

    private void fillInvisibleColumn(String tableName, Map<String, String> firstItem) {
        for (String columnId : firstItem.keySet()) {
            ColumnItem columnItem = new ColumnItem(columnId.toLowerCase(), tableName.toLowerCase());
            allColumn.putIfAbsent(tableName, new LinkedHashMap<>());
            allColumn.get(tableName).putIfAbsent(columnId, columnItem);
        }
    }

    //Получаем выделенные объекты (у родительского грида только один объект)
    private List<Map<String, String>> getSelectedItems(List<Map<String, String>> params) {
        List<Map<String, String>> items = getDataFromDatabase(params.remove(0));
        //Из гридов родителей загружаем только выделенную запись
        for (int i = 0; i < params.size(); i++) {
            Map<String, String> mapParams = params.get(i);
            for (Map<String, String> map : items) {
                map.putAll(getDataFromDatabase(mapParams).get(0));
            }
        }
        return items;
    }

    //Получаем все отфильтрованные объекты
    private List<Map<String, String>> getAllItems(List<Map<String, String>> params) {
        List<Map<String, String>> resultList = new ArrayList<>();
        if (params != null && !params.isEmpty()) {
            Map<String, String> gridParams = params.remove(0);
            resultList = getAllItems(params);
            //для самого верхнего родителя, просто получаем объекты
            if (resultList.isEmpty()) {
                resultList = getDataFromDatabase(gridParams);
            }
            // для остальных делаем запрос по каждому объекту родителя
            else {
                List<Map<String, String>> tmpList = new ArrayList<>();
                List<String> parentsCode = new ArrayList<>();
                for (String key : gridParams.keySet()) {
                    if (key.contains("parent_code_")) parentsCode.add(gridParams.get(key).toLowerCase());
                }
                for (Map<String, String> item : resultList) {
                    String parentTableName = item.get("tablename");
                    String sFilter = gridParams.get("sFilter");
                    Iterator<String> parentIterator = parentsCode.iterator();
                    List<Map<String, String>> list = new ArrayList<>();
                    while (parentIterator.hasNext() && (list.isEmpty() || list.get(0).isEmpty())) {
                        if (list.size() == 1 && list.get(0).isEmpty()) list.remove(0);
                        String tmpParentCode = parentIterator.next();
                        String value = item.get("id" + TABLE_AND_COLUMN_SEPARATOR + parentTableName);
                        if (value != null) {
                            String filters = replaceFilters(sFilter, tmpParentCode, value);
                            for (String filter : parentsCode) {
                                if (!filter.equals(tmpParentCode)) {
                                    value = item.get(filter + TABLE_AND_COLUMN_SEPARATOR + parentTableName);
                                    if (value != null) filters = replaceFilters(filters, filter, value);
                                }
                            }
                            gridParams.put("sFilter", filters);
                            list = getDataFromDatabase(gridParams);
                        } else {
                            list.add(new HashMap<>());
                        }
                    }
                    for (Map<String, String> map : list) {
                        item.remove("tablename");
                        map.putAll(item);
                    }
                    tmpList.addAll(list);
                }
                resultList = tmpList;
            }
        }
        return resultList;
    }

    private String replaceFilters(String sFilter, String parentCode, String value) {
        StringBuilder tmpBuilder = new StringBuilder(sFilter);
        if (parentCode != null && !parentCode.trim().isEmpty() &&
                value != null && !value.trim().isEmpty()) {
            String beginStr = "\"" + parentCode + "\":[\"";
            String endStr = "\"]";
            String indexNew = beginStr + value + endStr;
            if (sFilter.contains(beginStr)) {
                int start = tmpBuilder.indexOf(beginStr);
                int end = tmpBuilder.indexOf(endStr, start);
                tmpBuilder.replace(start, end + 2, indexNew);
            }
        }
        return tmpBuilder.toString();
    }

    //Устанавилваем имя листа
    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    private String removeFieldInFilters(String sFilter, String field) {
        return sFilter.replaceAll("(,\"" + field + "\":\\[[\\w\",]+])|(\"" + field + "\":\\[[\\w\",]+],)|(\"" + field + "\":\\[[\\w\",]+])", "");
    }

    //Выполняем запрос с указанными парамтрами, index - это индекс грида (0 - самый младший грид)
    private List<Map<String, String>> getDataFromDatabase(Map<String, String> params) {
        String tableName = params.get("tablename");
        if (tableName == null || tableName.trim().isEmpty()) tableName = "null";
        tablesCaption.put(tableName, params.get("tablecaption"));
        String tableUnit = params.get("tableunit");
        String sWhere = params.get("sWhere");
        if (sWhere != null) {
            sWhere = sWhere.replace(" and hid is null", "");
        }
        String sOrder = params.get("sOrder");
        String sFilter = params.get("sFilter");
        if (sFilter != null) {
            sFilter = removeFieldInFilters(sFilter, "hid");
            if (isOneGridExport) {
                for (String key : params.keySet()) {
                    if (key.contains("parent_code_"))
                        sFilter = removeFieldInFilters(sFilter, params.get(key).toLowerCase());
                }
            }
            if ((sFilter.trim().isEmpty() || sFilter.trim().equalsIgnoreCase("null")) || sFilter.trim().equals("{}")) {
                sFilter = null;
            }
        }
        String sFilterMin = params.get("sFilterMin");
        if (sFilterMin != null && (sFilterMin.trim().isEmpty() || sFilterMin.trim().equalsIgnoreCase("null"))) {
            sFilterMin = null;
        }
        String sFilterMax = params.get("sFilterMax");
        if (sFilterMax != null && (sFilterMax.trim().isEmpty() || sFilterMax.trim().equalsIgnoreCase("null"))) {
            sFilterMax = null;
        }

        List<Map<String, String>> data = new ArrayList<>();
        try {
            /*String sData = tableService.getDataTable(tableName, 1, 10001, null , sWhere, sOrder, sFilter, sFilterMin, sFilterMax, null, tableUnit, null);
            ObjectMapper objectMapper = new ObjectMapper();
            JSONArray arr = new JSONArray(sData);
            for (int i = 0; i < arr.length(); i++) {
                data.add(objectMapper.readValue(arr.getJSONObject(i).toString(), //ArrayToString  .replace("[","\"{").replace("]","}\""),
                        new TypeReference<HashMap<String, String>>() {
                        }));
            }*/
        } catch (Exception ex) {
            //Logger.getLogger(GWTServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (!data.isEmpty()) {
            fillInvisibleColumn(tableName, data.get(0));
        } else {
            data.add(new HashMap<>());
            return data;
        }
        return changeKeyOnIndex(data, tableName);
    }

    //Добавляет к ключам индекс (..._имя таблицы) что бы отличать поля с одинаковыми идентификаторами в разных гридах
    private List<Map<String, String>> changeKeyOnIndex(List<Map<String, String>> mapList, String tableName) {
        List<Map<String, String>> resultList = new ArrayList<>();
        for (Map<String, String> map : mapList) {
            Map<String, String> resultMap = new LinkedHashMap<>();
            for (String key : map.keySet()) {
                String newKey = key + TABLE_AND_COLUMN_SEPARATOR + tableName;
                resultMap.put(newKey, map.get(key));
            }
            resultMap.put("tablename", tableName);
            resultList.add(resultMap);
        }
        return resultList;
    }

    @Override
    public byte[] getContent() {
        createWorkbook();
        createStyles();
        setColumnsType();
        setHeadersRow();
        setItems();
        writeDatabaseInfo();
        setAutoSizeColumn();

        return getByteArray();
    }

    private void writeDatabaseInfo() {
        //String databaseName = tableService.doAction("procedure", "current_database", null, null, null);
        Sheet sheet = workbook.getSheetAt(0);
        Row row = sheet.getRow(NUM_COLUMN_ADVANCED_INFO_ROW);
        int columnIndex = row.getLastCellNum();
        Cell cell = row.createCell(columnIndex);
        cell.setCellStyle(headerCellStyle);
        //cell.setCellValue("database$" + databaseName);

        sheet.getRow(NUM_TABLE_CAPTION_ROW).createCell(columnIndex).setCellStyle(headerCellStyle);
        sheet.getRow(NUM_COLUMN_CAPTION_ROW).createCell(columnIndex).setCellStyle(headerCellStyle);

        int numRows = sheet.getLastRowNum();
        for (int i = NUM_COLUMN_ADVANCED_INFO_ROW + 1; i <= numRows; i++) {
            sheet.getRow(i).createCell(columnIndex).setCellStyle(itemCellStyle);
        }
        sheet.setColumnHidden(columnIndex, true);
    }

    private void setColumnsType() {
        for (String tableName : allColumn.keySet()) {
            List<Map<String, String>> data = new ArrayList<>();
            try {
                /*String sData = tableService.doAction("procedure", "p_system_get_table_fields", "{\"stablename\":\"" + tableName + "\"}", null, null);
                ObjectMapper objectMapper = new ObjectMapper();
                JSONArray arr = new JSONArray(sData);
                for (int i = 0; i < arr.length(); i++) {
                    data.add(objectMapper.readValue(arr.getJSONObject(i).toString(),
                            new TypeReference<HashMap<String, String>>() {
                            }));
                }*/
            } catch (Exception ex) {
               // Logger.getLogger(GWTServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (Map<String, String> column : data) {
                String code = column.get("code");
                String type = column.get("type");
                if (type != null) {
                    ColumnItem columnItem = allColumn.get(tableName).get(code.toLowerCase());
                    switch (type.toLowerCase()) {
                        case "bigint":
                        case "int":
                        case "number":
                            columnItem.setType(ColumnType.NUMBER);
                            break;
                        case "boolean":
                            columnItem.setType(ColumnType.BOOLEAN);
                            break;
                        case "date":
                            columnItem.setType(ColumnType.DATE);
                            break;
                        case "datetime":
                            columnItem.setType(ColumnType.DATETIME);
                            break;
                        case "time":
                            columnItem.setType(ColumnType.TIME);
                            break;
                        case "hidetext":
                            columnItem.setType(ColumnType.HIDE_TEXT);
                            break;
                    }
                }
            }
        }
    }

    private void setAutoSizeColumn() {
        Sheet sheet = workbook.getSheetAt(0);
        Row row = sheet.getRow(sheet.getLastRowNum());
        int cellSize = row.getLastCellNum();
        for (int i = 0; i < cellSize; i++) {
            sheet.autoSizeColumn(i);
        }
    }

    private void setItems() {
        XSSFSheet sheet = workbook.getSheetAt(0);
        Row infoRow = sheet.getRow(NUM_COLUMN_ADVANCED_INFO_ROW);
        for (Map<String, String> itemMap : itemsList) {
            Row itemRow = sheet.createRow(sheet.getLastRowNum() + 1);
            int lastCellNum = infoRow.getLastCellNum();
            for (int i = 0; i < lastCellNum; i++) {
                String[] array = infoRow.getCell(i).getStringCellValue().split("\\$", 2);
                String table = array[0];
                String columnId = array[1];
                ColumnItem columnItem = allColumn.get(table).get(columnId);
                Cell cell = itemRow.createCell(i);
                if (columnItem.getType().equals(ColumnType.NUMBER)) cell.setCellStyle(numberItemCellStyle);
                else cell.setCellStyle(itemCellStyle);
                String value = itemMap.get(columnId + TABLE_AND_COLUMN_SEPARATOR + table);
                if (value != null) {
                    switch (columnItem.getType()) {
                        case BOOLEAN:
                            value = value.replaceAll("[T,t][R,r][U,u][E,e]", "Да");
                            value = value.replaceAll("[F,f][A,a][L,l][S,s][E,e]", "Нет");
                            break;
                        case DATE:
                            Pattern datePattern = Pattern.compile("\\d+-\\d+-\\d+");
                            Matcher dateMatcher = datePattern.matcher(value);
                            while (dateMatcher.find()) {
                                String dValue = value.substring(dateMatcher.start(), dateMatcher.end());
                                try {
                                    LocalDate localDate = LocalDate.parse(dValue.trim());
                                    String fData = localDate.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
                                    value = value.replace(dValue, fData);
                                    dateMatcher = datePattern.matcher(value);
                                } catch (DateTimeParseException e) {
                                    e.printStackTrace();
                                }
                            }
                            break;
                        case TIME:
                            if (!value.trim().isEmpty()) {
                                Pattern timePattern = Pattern.compile("\\d+-\\d+-\\d+\\s\\d{2}:\\d{2}:\\d{2}");
                                Matcher timeMatcher = timePattern.matcher(value);
                                while (timeMatcher.find()) {
                                    String dValue = value.substring(timeMatcher.start(), timeMatcher.end());
                                    try {
                                        LocalDateTime localDateTime = LocalDateTime.parse(dValue, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                                        String fData = localDateTime.format(DateTimeFormatter.ofPattern("HH:mm"));
                                        value = value.replace(dValue, fData);
                                        timeMatcher = timePattern.matcher(value);
                                    } catch (DateTimeParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            break;
                        case DATETIME:
                            if (!value.trim().isEmpty()) {
                                Pattern dateTimePattern = Pattern.compile("\\d+-\\d+-\\d+\\s\\d{2}:\\d{2}:\\d{2}");
                                Matcher dateTimeMatcher = dateTimePattern.matcher(value);
                                while (dateTimeMatcher.find()) {
                                    String dValue = value.substring(dateTimeMatcher.start(), dateTimeMatcher.end());
                                    System.out.println(dValue);
                                    try {
                                        LocalDateTime localDateTime = LocalDateTime.parse(dValue, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                                        String fData = localDateTime.format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"));
                                        value = value.replace(dValue, fData);
                                        dateTimeMatcher = dateTimePattern.matcher(value);
                                    } catch (DateTimeParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            break;
                    }
                    cell.setCellValue(value);
                }
            }
        }
    }

    //Создаем стили для отчета
    private void createStyles() {
        headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
        headerCellStyle.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
        headerCellStyle.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
        headerCellStyle.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
        headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
        headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        headerCellStyle.setWrapText(true);

        XSSFFont font = workbook.createFont();
        font.setColor(headerCellStyle.getFont().getColor());
        font.setCharSet(headerCellStyle.getFont().getCharSet());
        font.setFontHeight(headerCellStyle.getFont().getFontHeight());
        font.setScheme(headerCellStyle.getFont().getScheme());
        font.setFontName(headerCellStyle.getFont().getFontName());
        font.setBold(true);
        font.setItalic(headerCellStyle.getFont().getItalic());
        font.setStrikeout(headerCellStyle.getFont().getStrikeout());
        font.setUnderline(headerCellStyle.getFont().getUnderline());
        font.setTypeOffset(headerCellStyle.getFont().getTypeOffset());

        headerCellStyle.setFont(font);

        itemCellStyle = workbook.createCellStyle();
        itemCellStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        itemCellStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        itemCellStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);
        itemCellStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);

        numberItemCellStyle = workbook.createCellStyle();
        numberItemCellStyle.cloneStyleFrom(itemCellStyle);
        numberItemCellStyle.setAlignment(HorizontalAlignment.RIGHT);
    }

    //Заполняет таблицу "шапкой" (Названиями колонок)
    private void setHeadersRow() {
        XSSFSheet sheet = workbook.getSheetAt(0);
        //Строка с названием таблицы
        XSSFRow tablesCaptionRow = sheet.createRow(NUM_TABLE_CAPTION_ROW);
        tablesCaptionRow.getCTRow().setCustomHeight(false);
        //Строка с названием колонки
        XSSFRow headerRow = sheet.createRow(NUM_COLUMN_CAPTION_ROW);
        headerRow.getCTRow().setCustomHeight(false);
        //Строка с дополнительной информацией для импорта
        XSSFRow advancedInfoRow = sheet.createRow(NUM_COLUMN_ADVANCED_INFO_ROW);
        advancedInfoRow.getCTRow().setCustomHeight(false);
        advancedInfoRow.setZeroHeight(true);

        int i = 0;
        for (String tableName : allColumn.keySet()) {
            Map<String, ColumnItem> columnItemMap = allColumn.get(tableName);
            String tableCaption = tablesCaption.get(tableName);
            for (Map.Entry<String, ColumnItem> entry : columnItemMap.entrySet()) {
                ColumnItem columnItem = entry.getValue();
                if (columnItem.getType() == ColumnType.HIDE_TEXT) continue;
                //Добавили название таблицы
                Cell tableCaptionCell = tablesCaptionRow.createCell(i);
                tableCaptionCell.setCellStyle(headerCellStyle);
                if (tableCaption != null) tableCaptionCell.setCellValue(tableCaption);

                //Добавили название колонки
                Cell columnNameCell = headerRow.createCell(i);
                columnNameCell.setCellStyle(headerCellStyle);
                if (columnItem.getCaption() != null) columnNameCell.setCellValue(columnItem.getCaption());

                //Добавляем дополнительную информацию о колонки (имя_таблицы$идентификатор_колонки)
                Cell advancedInfoCell = advancedInfoRow.createCell(i);
                advancedInfoCell.setCellStyle(headerCellStyle);
                advancedInfoCell.setCellValue(tableName.toLowerCase() + "$" + columnItem.getId().toLowerCase());

                if (!columnItem.isVisible()) sheet.setColumnHidden(i, true);
                i++;
            }
        }

        //Объединяем ячейки с одинаковыми таблицами
        mergedTableCaptionCells(tablesCaptionRow);

        //Делаем границу слева за самой последней ячейкой
        XSSFCellStyle lastCellBorder = workbook.createCellStyle();
        lastCellBorder.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
        XSSFCell lastCell = tablesCaptionRow.createCell(tablesCaptionRow.getLastCellNum() + 1);
        lastCell.setCellStyle(lastCellBorder);
    }

    private void mergedTableCaptionCells(Row tablesCaptionRow) {
        String lastTableCaption = null;
        int firstColumn = 0;
        int lastColumn = 0;
        Iterator<Cell> cellIterator = tablesCaptionRow.cellIterator();
        while (cellIterator.hasNext()) {
            Cell cell = cellIterator.next();
            if (lastTableCaption == null) {
                lastTableCaption = cell.getStringCellValue();
            } else {
                String currentValue = cell.getStringCellValue();
                if (lastTableCaption.equals(currentValue)) {
                    cell.setCellValue("");
                    lastColumn = cell.getColumnIndex();
                } else {
                    lastTableCaption = currentValue;
                    tablesCaptionRow.getSheet().addMergedRegionUnsafe(new CellRangeAddress(0, 0, firstColumn, lastColumn));
                    firstColumn = cell.getColumnIndex();
                }
            }
        }
        tablesCaptionRow.getSheet().addMergedRegionUnsafe(new CellRangeAddress(0, 0, firstColumn, lastColumn));
    }

    private void createWorkbook() {
        workbook = new XSSFWorkbook();
        workbook.createSheet(sheetName);
    }

    private byte[] getByteArray() {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();) {
            workbook.write(bos);
            byte[] bytes = bos.toByteArray();
            return bytes;
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new NullPointerException("Ошибка записи книги в byte code");
    }
}
