package ru.centp.report.impl.exporter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import ru.centp.report.api.exporter.ImportFromExcel;
import ru.centp.report.impl.model.ItemProvider;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.logging.Logger;

public class ImportMapLoader implements ImportFromExcel {

    private final Workbook workbook;
    private final ItemProvider itemProvider;
    //private final Logger logger = Logger.getLogger("3436");

    public ImportMapLoader(InputStream inputStream, String filename) {
        this.itemProvider = new ItemProvider();
        this.workbook = readWorkbook(inputStream, filename);
    }

    @Override
    public String getJsonQuery() {
        String json = null;
        //Значения (Имя столбца: значение)
        getHeaders();
        getItems();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            json = objectMapper.writeValueAsString(itemProvider);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }

    private void getHeaders() {
        Sheet sheet = workbook.getSheetAt(0);
        Row headersRow = sheet.getRow(2);
        if (headersRow != null) {
            Iterator<Cell> cellIterator = headersRow.cellIterator();
            while (cellIterator.hasNext()) {
                Cell headerCell = cellIterator.next();
                if (headerCell.getColumnIndex() == headersRow.getLastCellNum() - 1)
                    itemProvider.setDatabaseName(getCellValue(headerCell).split("\\$", 2)[1]);
                else itemProvider.addHeader(getCellValue(headerCell));
            }
        }
    }

    private void getItems() {
        Sheet sheet = workbook.getSheetAt(0);
        int rowNumber = sheet.getLastRowNum();
        for (int i = 3; i <= rowNumber; i++) {
            Row itemRow = sheet.getRow(i);
            Iterator<Cell> cellIterator = itemRow.cellIterator();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                if (cell != null) {
                    String cellValue = getCellValue(cell);
                    if (cell.getColumnIndex() < itemRow.getLastCellNum() - 1)
                        itemProvider.addItem(cellValue, cell.getColumnIndex());
                }
            }
        }
    }

    private String getCellValue(Cell cell) {
        String result = null;
        switch (cell.getCellTypeEnum()) {
            case STRING:
                if (!cell.getStringCellValue().isEmpty()) result = cell.getStringCellValue();
                break;
            case NUMERIC:
                double v = cell.getNumericCellValue();
                if (v % 1 == 0) result = Integer.toString((int) v);
                else result = Double.toString(cell.getNumericCellValue());
                break;
            case BOOLEAN:
                result = Boolean.toString(cell.getBooleanCellValue());
                break;
        }
        return result;
    }

    private Workbook readWorkbook(InputStream inputStream, String filename) {
        try {
            if (filename.endsWith(".xlsx")) {
                return new XSSFWorkbook(inputStream);
            } else if (filename.endsWith(".xls")) {
                return new HSSFWorkbook(inputStream);
            } else {
                throw new IllegalArgumentException("Формат " + filename.substring(filename.lastIndexOf(".")) + " не поддерживается");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new IllegalArgumentException("Ошибка чтения файла " + filename);
    }
}