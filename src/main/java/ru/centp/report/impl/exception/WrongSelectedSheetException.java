package ru.centp.report.impl.exception;

public class WrongSelectedSheetException extends RuntimeException {

    public WrongSelectedSheetException(String message) {
        super(message);
    }
}
