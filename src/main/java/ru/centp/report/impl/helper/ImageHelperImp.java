package ru.centp.report.impl.helper;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import ru.centp.report.api.helper.ImageHelper;
import ru.centp.report.impl.model.ImageContainer;
import ru.centp.report.impl.model.ItemContainer;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class ImageHelperImp implements ImageHelper {
    private final Set<ImageContainer> images;

    public ImageHelperImp() {
        images = new LinkedHashSet<>();
    }

    @Override
    public void addImage(byte[] image, AreaReference reference, String sheetName) {
        ImageContainer imageContainer = new ImageContainer(image, sheetName);

        Optional<ImageContainer> containerOptional = images.stream()
                .filter(container -> container.equals(imageContainer))
                .findFirst();
        if (containerOptional.isPresent()) {
            containerOptional.get().addCellReference(reference);
        } else {
            imageContainer.addCellReference(reference);
            images.add(imageContainer);
        }
    }

    @Override
    public void addRows(String sheetName, AreaReference copiedReference, int createdRowNum) {
        images.stream()
                .filter(imageContainer -> imageContainer.getSheetName().equalsIgnoreCase(sheetName))
                .forEach(container -> container.addRows(
                        createdRowNum,
                        copiedReference.getFirstCell().getRow(),
                        copiedReference.getLastCell().getRow()));
    }

    @Override
    public void removeRows(String sheetName, int delRowNum, int delRowSize) {
        images.stream()
                .filter(imageContainer -> imageContainer.getSheetName().equalsIgnoreCase(sheetName))
                .forEach(imageContainer -> imageContainer.removeRows(delRowNum, delRowSize));
    }

    @Override
    public void addCols(String sheetName, AreaReference copiedReference, int createdColNum) {
        images.stream()
                .filter(imageContainer -> imageContainer.getSheetName().equalsIgnoreCase(sheetName))
                .forEach(container -> container.addCols(
                        createdColNum,
                        copiedReference.getFirstCell().getCol(),
                        copiedReference.getLastCell().getCol()));
    }

    @Override
    public void removeCols(String sheetName, int delColNum, int delColSize) {
        images.stream()
                .filter(imageContainer -> imageContainer.getSheetName().equalsIgnoreCase(sheetName))
                .forEach(imageContainer -> imageContainer.removeCols(delColNum, delColSize));
    }

    @Override
    public void renderAllImage(Workbook workbook) {
        images.forEach(image -> {
            int pictureIdx = workbook.addPicture(image.getImage(), Workbook.PICTURE_TYPE_PNG);
            CreationHelper helper = workbook.getCreationHelper();
            image.getCells().forEach(area -> {
                ClientAnchor anchor = helper.createClientAnchor();
                anchor.setCol1(area.getFirstCell().getCol());
                anchor.setRow1(area.getFirstCell().getRow());
                Drawing drawing = workbook.getSheet(image.getSheetName()).createDrawingPatriarch();
                Picture pict = drawing.createPicture(anchor, pictureIdx);
                anchor.setCol2(area.getLastCell().getCol());
                anchor.setRow2(area.getLastCell().getRow());
            });
        });
    }
}
