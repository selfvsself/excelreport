package ru.centp.report.impl.helper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RowHelper {

    public int getIndexOnLink(String link) {
        int index = 0;
        Pattern pattern = Pattern.compile("\\.\\d+$");
        Matcher matcher = pattern.matcher(link);
        if (matcher.find()) {
            String strIndex = link.substring(matcher.start() + 1);
            index = Integer.parseInt(strIndex);
        }
        return index;
    }
}
