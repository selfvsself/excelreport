package ru.centp.report.impl.helper;

import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.usermodel.CellCopyPolicy;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.poi.ss.usermodel.CellType.BLANK;
import static org.apache.poi.ss.usermodel.CellType.FORMULA;

public class ColumnHelper {

    RowHelper rowHelper = new RowHelper();

    public void insertColumnsOld(Sheet sheet, int endColumn, final int n) {
        //Запоминаем объединенные ячейки и удаляем, те которые нам нужно изменить
        List<CellRangeAddress> insList = new ArrayList<>();
        int startColumn = endColumn - n + 1;
        for (int i = sheet.getNumMergedRegions() - 1; i >= 0; i--) {
            CellRangeAddress cellAddress = sheet.getMergedRegion(i);
            int cellAddressStart = cellAddress.getFirstColumn();
            int cellAddressEnd = cellAddress.getLastColumn();
            if (cellAddressStart > endColumn &&
                    cellAddressEnd > endColumn) {
                sheet.removeMergedRegion(i);
                cellAddress.setFirstColumn(cellAddressStart + n);
                cellAddress.setLastColumn(cellAddressEnd + n);
                insList.add(cellAddress);
            } else if (cellAddressStart < startColumn &&
                    cellAddressEnd > endColumn) {
                sheet.removeMergedRegion(i);
                cellAddress.setLastColumn(cellAddressEnd + n);
                insList.add(cellAddress);
            } else if (cellAddressStart < startColumn &&
                    cellAddressEnd > startColumn &&
                    cellAddressEnd < endColumn) {
                cellAddress.setFirstColumn(startColumn + n);
                cellAddress.setLastColumn(cellAddressEnd + n);
                insList.add(cellAddress);
            } else if (cellAddressStart >= startColumn &&
                    cellAddressEnd <= endColumn) {
                cellAddress.setFirstColumn(cellAddressStart + n);
                cellAddress.setLastColumn(cellAddressEnd + n);
                insList.add(cellAddress);
            } else if (cellAddressStart > startColumn &&
                    cellAddressStart < endColumn &&
                    cellAddressEnd > endColumn) {
                sheet.removeMergedRegion(i);
                cellAddress.setLastColumn(endColumn);
                insList.add(cellAddress);
                CellRangeAddress cellAddress2 = new CellRangeAddress(cellAddress.getFirstRow(),
                        cellAddress.getLastRow(), cellAddress.getFirstColumn(),
                        cellAddress.getLastColumn());
                cellAddress2.setFirstColumn(cellAddressStart + n);
                cellAddress2.setLastColumn(endColumn + n);
                insList.add(cellAddress2);
            }
        }

        //Возвращаем измененные объединенные ячейки
        for (CellRangeAddress cellAddresses : insList) {
            try {
                sheet.addMergedRegion(cellAddresses);
            } catch (IllegalStateException ignore) {

            }
        }

        int lastColumn = 0;
        //Передвигаем столбцы на необходимое количество вправо
        for (Row cells : sheet) {
            XSSFRow row = (XSSFRow) cells;
            if (row.getLastCellNum() > lastColumn) lastColumn = row.getLastCellNum();
            for (int i = row.getLastCellNum() + n - 1; i > endColumn; i--) {
                XSSFCell cell = row.getCell(i);
                if (cell == null) cell = row.createCell(i);
                XSSFCell prevCell = row.getCell(i - n);
                if (prevCell == null) prevCell = row.createCell(i - n);
                //Очищаем ячейку в котоую будем копировать значение
                cell.setCellType(BLANK);
                cell.copyCellFrom(prevCell, new CellCopyPolicy());
                //Обновляем формулы в сдвинутых ячейках
                if (cell.getCellTypeEnum() == FORMULA) {
                    String formula = cell.getCellFormula();

                    Pattern pattern = Pattern.compile("[A-Z]+\\d+");
                    Matcher matcher = pattern.matcher(formula);
                    Map<String, String> replaceAddress = new HashMap<>();

                    while (matcher.find()) {
                        String oldAddress = formula.substring(matcher.start(), matcher.end());
                        CellAddress cellAddress = new CellAddress(oldAddress);
                        if (cellAddress.getColumn() > endColumn - n + 1) {
                            String oldColumn = CellReference.convertNumToColString(cellAddress.getColumn());
                            String newColumn = CellReference.convertNumToColString(cellAddress.getColumn() + n);
                            String newAddress = oldAddress.replace(oldColumn, newColumn);
                            replaceAddress.put(oldAddress, newAddress);
                        }
                    }
                    for (String oldValue : replaceAddress.keySet()) {
                        formula = formula.replaceAll(oldValue, replaceAddress.get(oldValue));
                    }
                    cell.setCellFormula(formula);
                }
            }
        }

        //Двигаем скрытые столбцы
        for (int i = lastColumn; i >= startColumn; i--) {
            //Копируем ширину столбца
            if (i > endColumn) sheet.setColumnWidth(i, sheet.getColumnWidth(i - n));
            if (sheet.isColumnHidden(i)) {
                if (i > endColumn) sheet.setColumnHidden(i, false);
                sheet.setColumnHidden(i + n, true);
            }
        }

        //Изменяем ссылки на ячейки
        for (Name name : sheet.getWorkbook().getAllNames()) {
            AreaReference areaReference = new AreaReference(name.getRefersToFormula(), SpreadsheetVersion.EXCEL2007);
            int firstAreaColumn = areaReference.getFirstCell().getCol();
            int lastAreaColumn = areaReference.getLastCell().getCol();
            if (firstAreaColumn > endColumn && lastAreaColumn > endColumn) {

            }
        }
    }

    public void insertOneColumn(Sheet sheet, int destColumn) {
        //Запоминаем объединенные ячейки и удаляем, те которые нам нужно изменить
        List<CellRangeAddress> insList = new ArrayList<>();
        for (int i = sheet.getNumMergedRegions() - 1; i >= 0; i--) {
            CellRangeAddress cellAddress = sheet.getMergedRegion(i);
            int cellAddressStart = cellAddress.getFirstColumn();
            int cellAddressEnd = cellAddress.getLastColumn();
            if (cellAddressStart < destColumn &&
                    cellAddressEnd > destColumn) {
                sheet.removeMergedRegion(i);
                cellAddress.setLastColumn(cellAddressEnd + 1);
                insList.add(cellAddress);
            } else if (cellAddressStart >= destColumn) {
                sheet.removeMergedRegion(i);
                cellAddress.setFirstColumn(cellAddressStart + 1);
                cellAddress.setLastColumn(cellAddressEnd + 1);
                insList.add(cellAddress);
            }
        }

        //Возвращаем измененные объединенные ячейки
        for (CellRangeAddress cellAddresses : insList) {
            sheet.addMergedRegion(cellAddresses);
        }

        int lastColumn = 0;
        for (Row cells : sheet) {
            XSSFRow row = (XSSFRow) cells;
            if (row.getLastCellNum() > lastColumn) lastColumn = row.getLastCellNum();
            for (int i = row.getLastCellNum(); i >=destColumn; i--) {
                XSSFCell cell = row.getCell(i);
                if (cell == null) cell = row.createCell(i);
                XSSFCell prevCell = row.getCell(i - 1);
                if (prevCell == null) prevCell = row.createCell(i - 1);
                //Очищаем ячейку в котоую будем копировать значение
                cell.setCellType(BLANK);
                cell.copyCellFrom(prevCell, new CellCopyPolicy());
                //Обновляем формулы в сдвинутых ячейках
                if (cell.getCellTypeEnum() == FORMULA) {
                    String formula = cell.getCellFormula();

                    Pattern pattern = Pattern.compile("[A-Z]+\\d+");
                    Matcher matcher = pattern.matcher(formula);
                    Map<String, String> replaceAddress = new HashMap<>();

                    while (matcher.find()) {
                        String oldAddress = formula.substring(matcher.start(), matcher.end());
                        CellAddress cellAddress = new CellAddress(oldAddress);
                        if (cellAddress.getColumn() >= destColumn) {
                            String oldColumn = CellReference.convertNumToColString(cellAddress.getColumn());
                            String newColumn = CellReference.convertNumToColString(cellAddress.getColumn() + 1);
                            String newAddress = oldAddress.replace(oldColumn, newColumn);
                            replaceAddress.put(oldAddress, newAddress);
                        }
                    }
                    for (String oldValue : replaceAddress.keySet()) {
                        formula = formula.replaceAll(oldValue, replaceAddress.get(oldValue));
                    }
                    cell.setCellFormula(formula);
                }
            }
        }

        //Двигаем скрытые столбцы
        for (int i = lastColumn + 1; i > destColumn; i--) {
            //Копируем ширину столбца
            if (i > destColumn + 1) sheet.setColumnWidth(i, sheet.getColumnWidth(i - 1));
            if (sheet.isColumnHidden(i - 1)) {
                sheet.setColumnHidden(i - 1, false);
                sheet.setColumnHidden(i, true);
            }
        }
    }

    public void insertColumns(Sheet sheet, int destColumn, int n) {
        for (int i = 0; i < n; i++) {
            insertOneColumn(sheet, destColumn);
        }
    }

    public void deleteColumn(Sheet sheet, int firstColumn, final int n) {
        //Запоминаем объединенные ячейки и удаляем, те которые нам нужно изменить
        List<CellRangeAddress> insList = new ArrayList<>();
        int endColumn = firstColumn + n - 1;
        for (int i = sheet.getNumMergedRegions() - 1; i >= 0; i--) {
            CellRangeAddress cellAddress = sheet.getMergedRegion(i);
            int cellAddressStart = cellAddress.getFirstColumn();
            int cellAddressEnd = cellAddress.getLastColumn();
            if (cellAddressStart > endColumn &&
                    cellAddressEnd > endColumn) {
                sheet.removeMergedRegion(i);
                cellAddress.setFirstColumn(cellAddressStart - n);
                cellAddress.setLastColumn(cellAddressEnd - n);
                insList.add(cellAddress);
            } else if (cellAddressStart < firstColumn &&
                    cellAddressEnd > endColumn) {
                sheet.removeMergedRegion(i);
                cellAddress.setLastColumn(cellAddressEnd - n);
                insList.add(cellAddress);
            } else if (cellAddressStart < firstColumn &&
                    cellAddressEnd > firstColumn &&
                    cellAddressEnd < endColumn) {
                sheet.removeMergedRegion(i);
                cellAddress.setLastColumn(firstColumn - 1);
                if (cellAddress.getLastColumn() - cellAddress.getFirstColumn() > 0) insList.add(cellAddress);
            } else if (cellAddressStart >= firstColumn &&
                    cellAddressEnd <= endColumn) {
                sheet.removeMergedRegion(i);
            } else if (cellAddressStart > firstColumn &&
                    cellAddressStart < endColumn &&
                    cellAddressEnd > endColumn) {
                sheet.removeMergedRegion(i);
                cellAddress.setFirstColumn(firstColumn);
                cellAddress.setLastColumn(cellAddressEnd - n );
                if (cellAddress.getLastColumn() - cellAddress.getFirstColumn() > 0) insList.add(cellAddress);
            }
        }

        //Возвращаем измененные объединенные ячейки
        for (CellRangeAddress cellAddresses : insList) {
            try {
                sheet.addMergedRegion(cellAddresses);
            } catch (IllegalStateException ignore) {

            }
        }

        int lastColumn = 0;
        for (Row cells : sheet) {
            XSSFRow row = (XSSFRow) cells;
            if (row.getLastCellNum() > lastColumn) lastColumn = row.getLastCellNum();
            int lastCellNum = row.getLastCellNum();
            for (int i = firstColumn; i < lastCellNum; i++) {
                XSSFCell cell = row.getCell(i);
                if (cell == null) cell = row.createCell(i);
                XSSFCell nextCell = row.getCell(i + n);
                if (nextCell == null) nextCell = row.createCell(i + n);
                cell.copyCellFrom(nextCell, new CellCopyPolicy());
                //Обновляем формулы в сдвинутых ячейках
                if (cell.getCellTypeEnum() == FORMULA) {
                    String formula = cell.getCellFormula();

                    Pattern pattern = Pattern.compile("[A-Z]+\\d+");
                    Matcher matcher = pattern.matcher(formula);
                    Map<String, String> replaceAddress = new HashMap<>();

                    while (matcher.find()) {
                        String oldAddress = formula.substring(matcher.start(), matcher.end());
                        CellAddress cellAddress = new CellAddress(oldAddress);
                        if (cellAddress.getColumn() > firstColumn) {
                            String oldColumn = CellReference.convertNumToColString(cellAddress.getColumn());
                            String newColumn = CellReference.convertNumToColString(cellAddress.getColumn() - n);
                            String newAddress = oldAddress.replace(oldColumn, newColumn);
                            replaceAddress.put(oldAddress, newAddress);
                        }
                    }
                    for (String oldValue : replaceAddress.keySet()) {
                        formula = formula.replaceAll(oldValue, replaceAddress.get(oldValue));
                    }
                    cell.setCellFormula(formula);
                }
                sheet.setColumnWidth(cell.getColumnIndex(), sheet.getColumnWidth(nextCell.getColumnIndex()));
                row.removeCell(nextCell);
            }
        }
        //Двигаем скрытые столбцы
        for (int i = firstColumn; i < lastColumn; i++) {
            if (sheet.isColumnHidden(i)) {
                sheet.setColumnHidden(i, false);
                if (i >= firstColumn + n) sheet.setColumnHidden(i - n, true);
            }
        }
    }

    public void copyColumn(Sheet sheet, int firstColumn, int lastColumn, int destinationColumn, CellCopyPolicy cellCopyPolicy) {
        destinationColumn -= firstColumn;
        for (Row cells : sheet) {
            XSSFRow row = (XSSFRow) cells;
            for (int i = firstColumn; i <= lastColumn; i++) {
                XSSFCell cell = row.getCell(i);
                if (cell == null) cell = row.createCell(i);
                XSSFCell nextCell = row.getCell(i + destinationColumn);
                if (nextCell == null) nextCell = row.createCell(i + destinationColumn);
                nextCell.setCellType(BLANK);
                nextCell.copyCellFrom(cell, cellCopyPolicy);
                sheet.setColumnWidth(nextCell.getColumnIndex(), sheet.getColumnWidth(cell.getColumnIndex()));
                if (nextCell.getCellTypeEnum() == FORMULA) {
                    String formula = nextCell.getCellFormula();

                    Pattern pattern = Pattern.compile("[A-Z]+\\d+");
                    Matcher matcher = pattern.matcher(formula);
                    Map<String, String> replaceAddress = new HashMap<>();

                    while (matcher.find()) {
                        String oldAddress = formula.substring(matcher.start(), matcher.end());
                        CellAddress cellAddress = new CellAddress(oldAddress);
                        if (cellAddress.getColumn() >= firstColumn) {
                            String oldColumn = CellReference.convertNumToColString(cellAddress.getColumn());
                            String newColumn = CellReference.convertNumToColString(cellAddress.getColumn() + destinationColumn);
                            String newAddress = oldAddress.replace(oldColumn, newColumn);
                            replaceAddress.put(oldAddress, newAddress);
                        }
                    }
                    for (String oldValue : replaceAddress.keySet()) {
                        formula = formula.replaceAll(oldValue, replaceAddress.get(oldValue));
                    }
                    nextCell.setCellFormula(formula);
                }
            }
        }

        List<CellRangeAddress> insList = new ArrayList<>();
        int n = lastColumn - firstColumn ;
        for (CellRangeAddress cellAddresses : sheet.getMergedRegions()) {
            if (cellAddresses.getFirstColumn() >= firstColumn && cellAddresses.getLastColumn() <= lastColumn) {
                cellAddresses.setFirstColumn(cellAddresses.getFirstColumn() + destinationColumn);
                cellAddresses.setLastColumn(cellAddresses.getLastColumn() + destinationColumn);
                insList.add(cellAddresses);
            }
        }

        //Возвращаем измененные объединенные ячейки
        for (CellRangeAddress cellAddresses : insList) {
            try {
                sheet.addMergedRegion(cellAddresses);
            } catch (IllegalStateException ignore) {

            }
        }

        //Копируем скрытые столбцы
        for (int i = lastColumn + 1; i > firstColumn; i--) {
            //Копируем ширину столбца
            if (sheet.isColumnHidden(i - 1)) {
                sheet.setColumnHidden(i + destinationColumn - 1, true);
            }
        }
    }

    public int getIndexOnLink(String link) {
        return rowHelper.getIndexOnLink(link);
    }
}
