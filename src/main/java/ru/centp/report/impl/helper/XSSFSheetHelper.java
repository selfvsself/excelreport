package ru.centp.report.impl.helper;

import org.apache.poi.ss.formula.FormulaParseException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.*;
import ru.centp.report.api.helper.SheetHelper;
import ru.centp.report.impl.model.DataValidationContainer;
import ru.centp.report.impl.model.FormulaElement;
import ru.centp.report.impl.model.FormulaParser;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.poi.ss.usermodel.CellType.FORMULA;

public class XSSFSheetHelper implements SheetHelper {

    private Map<String, List<CellRangeAddress>> workbookCellRangeMap;
    private Map<String, Map<CellAddress, FormulaParser>> allFormulasMap;
    private XSSFWorkbook workbook;
    private Map<String, Integer> lastColumnMap;
    private Map<String, List<DataValidationContainer>> dataValidationMap;

    //При создании объекта записываем и удаляем информацию о объединенных ячейках из книги, для уменьшения времени обработки больших книг
    public XSSFSheetHelper(XSSFWorkbook workbook) {
        this.workbook = workbook;
        lastColumnMap = new HashMap<>();
        workbookCellRangeMap = new HashMap<>();
        allFormulasMap = new HashMap<>();
        dataValidationMap = new HashMap<>();
        saveAllFormulas(workbook);
        saveAndDeleteAllMergedRegions(workbook);
        saveAndDeleteDataValidation(workbook);
    }

    @Override
    public void restoreAllDataValidation(Workbook workbook) {
        for (String sheetName : dataValidationMap.keySet()) {
            Sheet sheet = workbook.getSheet(sheetName);
            if (sheet != null) {
                DataValidationHelper validationHelper = sheet.getDataValidationHelper();
                List<DataValidationContainer> containerList = dataValidationMap.get(sheetName);
                for (DataValidationContainer validationContainer : containerList) {
                    DataValidation dataValidation = validationHelper.createValidation(validationContainer.getDataValidationConstraint(),
                            validationContainer.getCellRangeAddressList());
                    dataValidation.setEmptyCellAllowed(validationContainer.isEmptyCellAllowed());
                    dataValidation.setErrorStyle(validationContainer.getErrorStyle());
                    dataValidation.setShowErrorBox(validationContainer.isShowErrorBox());
                    dataValidation.setShowPromptBox(validationContainer.isShowPromptBox());
                    dataValidation.setSuppressDropDownArrow(validationContainer.isSuppressDropDownArrow());
                    dataValidation.createErrorBox(
                            validationContainer.getErrorBoxTitle(),
                            validationContainer.getErrorBoxText());
                    dataValidation.createPromptBox(
                            validationContainer.getPromptBoxTitle(),
                            validationContainer.getPromptBoxText());
                    sheet.addValidationData(dataValidation);
                }
            }
        }
    }

    private void saveAndDeleteDataValidation(XSSFWorkbook workbook) {
        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
            XSSFSheet sheet = workbook.getSheetAt(i);
            List<DataValidationContainer> containerList = new ArrayList<>();
            for (XSSFDataValidation dataValidation : sheet.getDataValidations()) {
                DataValidationContainer container = new DataValidationContainer(dataValidation);
                containerList.add(container);
            }
            if (!containerList.isEmpty()) {
                dataValidationMap.put(sheet.getSheetName(), containerList);
                int sizeDataValidation = containerList.size();
                for (int j = 0; j < sizeDataValidation; j++) {
                    sheet.getCTWorksheet().getDataValidations().removeDataValidation(0);
                }
            }
        }
    }

    private void saveAllFormulas(XSSFWorkbook workbook) {
        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
            XSSFSheet sheet = workbook.getSheetAt(i);
            Map<CellAddress, FormulaParser> sheetFormulasMap = new HashMap<>();
            Iterator<Row> rowIterator = sheet.rowIterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    if (cell.getCellTypeEnum() == FORMULA) {
                        sheetFormulasMap.put(cell.getAddress(), new FormulaParser(cell.getCellFormula()));
                    }
                }
            }
            allFormulasMap.put(sheet.getSheetName(), sheetFormulasMap);
        }
    }

    @Override
    public void addFormula(Cell cell) {
        if (cell.getCellTypeEnum() == FORMULA &&
                allFormulasMap.get(getKey(cell.getSheet().getSheetName())) != null)
            allFormulasMap.get(getKey(cell.getSheet().getSheetName())).put(
                    cell.getAddress(),
                    new FormulaParser(cell.getCellFormula()));
    }

    @Override
    public void deleteFormula(Cell cell) {
        if (allFormulasMap.get(getKey(cell.getSheet().getSheetName())) != null)
            allFormulasMap.get(getKey(cell.getSheet().getSheetName())).remove(cell.getAddress());
    }

    private void saveAndDeleteAllMergedRegions(XSSFWorkbook workbook) {
        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
            XSSFSheet sheet = workbook.getSheetAt(i);
            workbookCellRangeMap.put(sheet.getSheetName(), sheet.getMergedRegions());
            int sheetSize = sheet.getNumMergedRegions();
            for (int y = sheetSize; y > 0; y--) sheet.removeMergedRegion(y - 1);
        }
    }

    /**
     * Метод возвращает название таблицы в правильном регистре
     */
    private String getKey(String sheetName) {
        String result = null;
        for (String key : workbookCellRangeMap.keySet()) {
            if (key.equalsIgnoreCase(sheetName)) {
                result = key;
                break;
            }
        }
        return result;
    }

    /**
     * Метод возвращает имя предыдущей ссылки
     */
    public String getPreviousLinkName(String currentName) {
        String prevName = currentName;
        int index = getIndexOnLink(prevName);
        if (index > 1) prevName = prevName.replaceAll("\\." + index + "$", "." + (index - 1));
        else {
            String str = Integer.toString(index);
            int lastChar = prevName.length() - (str.length() + 1);
            prevName = prevName.substring(0, lastChar);
        }
        return prevName;
    }

    /**
     * Метод возвращает индекс передавайемой ссылки
     */
    public int getIndexOnLink(String link) {
        int index = 0;
        Pattern pattern = Pattern.compile("\\.\\d+$");
        Matcher matcher = pattern.matcher(link);
        if (matcher.find()) {
            String strIndex = link.substring(matcher.start() + 1);
            index = Integer.parseInt(strIndex);
        }
        return index;
    }

    public void calculateAllFormulas() {
        XSSFFormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();

        for (String sheetName : allFormulasMap.keySet()) {
            XSSFSheet sheet = workbook.getSheet(sheetName);
            Map<CellAddress, FormulaParser> sheetFormulasMap = allFormulasMap.get(sheetName);
            for (CellAddress cellAddress : sheetFormulasMap.keySet()) {
//                FormulaParser formulaParser = sheetFormulasMap.get(cellAddress);
                XSSFCell cell = sheet.getRow(cellAddress.getRow()).getCell(cellAddress.getColumn());
                if (cell.getCellTypeEnum() == FORMULA) {
                    try {
                        CellType cellType = formulaEvaluator.evaluateFormulaCellEnum(cell);
                        switch (cellType) {
                            case STRING:
                                cell.setCellValue(formulaEvaluator.evaluate(cell).getStringValue());
                                break;
                            case NUMERIC:
                                cell.setCellValue(formulaEvaluator.evaluate(cell).getNumberValue());
                                break;
                            case BOOLEAN:
                                cell.setCellValue(formulaEvaluator.evaluate(cell).getBooleanValue());
                                break;
                        }
                    } catch (RuntimeException ignore) {
                    }
                }
            }
        }
    }

    private void calculateFormulaForRow(String sheetName, int desRow, int n) {
        for (String sheetNameKey : allFormulasMap.keySet()) {
            if (sheetNameKey.equalsIgnoreCase(sheetName)) {
                Map<CellAddress, FormulaParser> sheetFormulasMap = allFormulasMap.get(sheetNameKey);
                Map<CellAddress, FormulaParser> addFormulas = new HashMap<>();
                Iterator<CellAddress> addressIterator = sheetFormulasMap.keySet().iterator();
                while (addressIterator.hasNext()) {
                    CellAddress cellAddress = addressIterator.next();
                    FormulaParser formulaParser = sheetFormulasMap.get(cellAddress);
                    for (FormulaElement element : formulaParser.getElements()) {
                        if (element.getSheetName() == null || element.getSheetName().equals(sheetName)) {
                            if (n >= 0) {
                                if (element.getRow() >= desRow) {
                                    element.setRow(element.getRow() + n);
                                }
                            } else {
                                if (element.getRow() > desRow) {
                                    element.setRow(element.getRow() + n);
                                }
                            }
                        }
                    }
                    if (cellAddress.getRow() >= desRow) {
                        addressIterator.remove();
                        if (n > 0 || (n < 0 && cellAddress.getRow() >= desRow - n))
                            addFormulas.put(
                                    new CellAddress(cellAddress.getRow() + n, cellAddress.getColumn()),
                                    formulaParser);
                    }
                }
                sheetFormulasMap.putAll(addFormulas);
            } else {
                Map<CellAddress, FormulaParser> sheetFormulasMap = allFormulasMap.get(sheetNameKey);
                Map<CellAddress, FormulaParser> addFormulas = new HashMap<>();
                Iterator<CellAddress> addressIterator = sheetFormulasMap.keySet().iterator();
                while (addressIterator.hasNext()) {
                    CellAddress cellAddress = addressIterator.next();
                    FormulaParser formulaParser = sheetFormulasMap.get(cellAddress);
                    for (FormulaElement element : formulaParser.getElements()) {
                        if (element.getSheetName() != null && element.getSheetName().equals(sheetName)) {
                            if (n >= 0) {
                                if (element.getRow() >= desRow) {
                                    element.setRow(element.getRow() + n);
                                }
                            } else {
                                if (element.getRow() > desRow) {
                                    element.setRow(element.getRow() + n);
                                }
                            }
                        }
                    }
                }
                sheetFormulasMap.putAll(addFormulas);
            }
        }
    }

    private void copyFormulasWhenRowsCopy(String sheetName, int starRow, int endRow, int destRowNum) {
        int delta = destRowNum - starRow;
        Map<CellAddress, FormulaParser> addMap = new HashMap<>();
        Map<CellAddress, FormulaParser> sheetFormulasMap = allFormulasMap.get(getKey(sheetName));
        for (CellAddress cellAddress : sheetFormulasMap.keySet()) {
            FormulaParser formulaParser = sheetFormulasMap.get(cellAddress);
            if (cellAddress.getRow() >= starRow && cellAddress.getRow() <= endRow) {
                FormulaParser addFormulaParser = new FormulaParser(formulaParser.formatAsString());
                for (FormulaElement element : addFormulaParser.getElements()) {
//                    if (element.getSheetName() == null || element.getSheetName().equals(sheetName)) {
//                        if (element.getRow() >= starRow && cellAddress.getRow() <= endRow) {
                    element.setRow(element.getRow() + delta);
//                        }
//                    }
                }
                addMap.put(new CellAddress(cellAddress.getRow() + delta, cellAddress.getColumn()),
                        addFormulaParser);
            }
        }
        sheetFormulasMap.putAll(addMap);
    }

    private void copyFormulasWhenColumnCopy(String sheetName, int starColumn, int endColumn, int destColumnNum) {
        int delta = destColumnNum - starColumn;
        Map<CellAddress, FormulaParser> addMap = new HashMap<>();
        Map<CellAddress, FormulaParser> sheetFormulasMap = allFormulasMap.get(getKey(sheetName));
        for (CellAddress cellAddress : sheetFormulasMap.keySet()) {
            FormulaParser formulaParser = sheetFormulasMap.get(cellAddress);
            if (cellAddress.getColumn() >= starColumn && cellAddress.getColumn() <= endColumn) {
                FormulaParser addFormulaParser = new FormulaParser(formulaParser.formatAsString());
                for (FormulaElement element : addFormulaParser.getElements()) {
//                    if (element.getSheetName() == null || element.getSheetName().equals(sheetName)) {
//                        if (element.getColumn() >= starColumn && cellAddress.getColumn() <= endColumn) {
                    element.setColumn(element.getColumn() + delta);
//                        }
//                    }
                }
                addMap.put(new CellAddress(cellAddress.getRow(), cellAddress.getColumn() + delta),
                        addFormulaParser);
            }
        }
        sheetFormulasMap.putAll(addMap);
    }

    private void calculateFormulaForColumn(String sheetName, int desColumn, int n) {
        for (String sheetNameKey : allFormulasMap.keySet()) {
            if (sheetNameKey.equalsIgnoreCase(sheetName)) {
                Map<CellAddress, FormulaParser> sheetFormulasMap = allFormulasMap.get(sheetNameKey);
                Map<CellAddress, FormulaParser> addFormulas = new HashMap<>();
                Iterator<CellAddress> addressIterator = sheetFormulasMap.keySet().iterator();
                while (addressIterator.hasNext()) {
                    CellAddress cellAddress = addressIterator.next();
                    FormulaParser formulaParser = sheetFormulasMap.get(cellAddress);
                    for (FormulaElement element : formulaParser.getElements()) {
                        if (element.getSheetName() == null || element.getSheetName().equals(sheetName)) {
                            if (n >= 0) {
                                if (element.getColumn() >= desColumn) {
                                    element.setColumn(element.getColumn() + n);
                                }
                            } else {
                                if (element.getColumn() > desColumn) {
                                    element.setColumn(element.getColumn() + n);
                                }
                            }
                        }
                    }
                    if (cellAddress.getColumn() >= desColumn) {
                        addressIterator.remove();
                        if (n > 0 || (n < 0 && cellAddress.getColumn() >= desColumn - n))
                            addFormulas.put(
                                    new CellAddress(cellAddress.getRow(), cellAddress.getColumn() + n),
                                    formulaParser);
                    }
                }
                sheetFormulasMap.putAll(addFormulas);
            } else {
                Map<CellAddress, FormulaParser> sheetFormulasMap = allFormulasMap.get(sheetNameKey);
                Map<CellAddress, FormulaParser> addFormulas = new HashMap<>();
                Iterator<CellAddress> addressIterator = sheetFormulasMap.keySet().iterator();
                while (addressIterator.hasNext()) {
                    CellAddress cellAddress = addressIterator.next();
                    FormulaParser formulaParser = sheetFormulasMap.get(cellAddress);
                    for (FormulaElement element : formulaParser.getElements()) {
                        if (element.getSheetName() != null && element.getSheetName().equals(sheetName)) {
                            if (n >= 0) {
                                if (element.getColumn() >= desColumn) {
                                    element.setColumn(element.getColumn() + n);
                                }
                            } else {
                                if (element.getColumn() > desColumn) {
                                    element.setColumn(element.getColumn() + n);
                                }
                            }
                        }
                    }
                }
                sheetFormulasMap.putAll(addFormulas);
            }
        }
    }

    /**
     * Метод вставляет строки на лист
     *
     * @param _sheet  Лист в котором проводим операцию
     * @param desRow Номер строки в начиная с которой добавляем строки
     * @param n      Количество добавляемых строк
     */
    @Override
    public void insertRows(Sheet _sheet, int desRow, int n) {
        XSSFSheet sheet = (XSSFSheet) _sheet;
        int lastRow = sheet.getLastRowNum();
        lastRow = Math.max(lastRow + n, desRow + n);

        for (int i = lastRow; i > desRow; i--) {
            XSSFRow createdRow = sheet.getRow(i);
            if (createdRow == null) createdRow = sheet.createRow(i);
            XSSFRow movedRow = sheet.getRow(i - n);
            if (movedRow == null) movedRow = sheet.createRow(i - n);

            //Ячейки с какой по какую начинаем копировать строку
            int firstCellNum = Math.min(createdRow.getFirstCellNum(), movedRow.getFirstCellNum());
            firstCellNum = Math.max(firstCellNum, 0);
            int lastCellNum = Math.max(createdRow.getLastCellNum(), movedRow.getLastCellNum());
            lastCellNum = Math.max(lastCellNum, 1);

            for (int y = firstCellNum; y < lastCellNum; y++) {
                //Получаем старую ячейку
                XSSFCell movedCell = movedRow.getCell(y);
                if (movedCell != null) {
                    //Очищаем новую ячейку
                    XSSFCell createdCell = createdRow.getCell(y);
                    if (createdCell != null) createdRow.removeCell(createdCell);
                    createdCell = createdRow.createCell(y, movedCell.getCellType());
                    createdCell.copyCellFrom(movedCell, new CellCopyPolicy());
                    createdCell.setCellStyle(movedCell.getCellStyle());
                } else {
                    XSSFCell createdCell = createdRow.getCell(y);
                    if (createdCell != null) createdRow.removeCell(createdCell);
                }
            }

            if (movedRow.getZeroHeight()) {
                createdRow.setZeroHeight(true);
            } else {
                createdRow.setZeroHeight(false);
            }

            createdRow.setRowStyle(movedRow.getRowStyle());

            createdRow.setHeight(movedRow.getHeight());
            createdRow.getCTRow().setCustomHeight(movedRow.getCTRow().getCustomHeight());
        }

        calculateFormulaForRow(sheet.getSheetName(), desRow, n);
        calculateDataValidationForRow(sheet.getSheetName(), desRow, n);

        //Запоминаем объединенные ячейки и удаляем, те которые нам нужно изменить
        List<CellRangeAddress> cellRangeList = workbookCellRangeMap.get(sheet.getSheetName());
        for (int i = 0; i < cellRangeList.size(); i++) {
            CellRangeAddress cellAddress = cellRangeList.get(i);
            int cellAddressStart = cellAddress.getFirstRow();
            int cellAddressEnd = cellAddress.getLastRow();
            if (cellAddressStart < desRow &&
                    cellAddressEnd > desRow) {
                cellAddress.setLastRow(cellAddressEnd + n);
            } else if (cellAddressStart >= desRow) {
                cellAddress.setFirstRow(cellAddressStart + n);
                cellAddress.setLastRow(cellAddressEnd + n);
            }
        }

        //Очищаем добавленные строки
        for (int i = desRow; i < desRow + n; i++) {
            XSSFRow deleteRow = sheet.getRow(i);
            if (deleteRow != null) sheet.removeRow(deleteRow);
        }
    }

    private void calculateDataValidationForRow(String sheetName, int desRow, int n) {
        List<DataValidationContainer> validationContainers = dataValidationMap.get(getKey(sheetName));
        List<Integer> removesAddress = new ArrayList<>();
        List<DataValidationContainer> removesContainers = new ArrayList<>();
        if (validationContainers != null) {
            for (DataValidationContainer container : validationContainers) {
                int addressSize = container.getCellRangeAddressList().countRanges();
                for (int j = 0; j < addressSize; j++) {
                    CellRangeAddress address = container.getCellRangeAddressList().getCellRangeAddress(j);
                    if (n < 0 &&
                            address.getFirstRow() >= desRow &&
                            address.getFirstRow() < desRow - n &&
                            address.getLastRow() >= desRow &&
                            address.getLastRow() < desRow - n) {
                        removesAddress.add(j);
                        continue;
                    }
                    if (address.getFirstRow() >= desRow) {
                        address.setFirstRow(address.getFirstRow() + n);
                    }
                    if (address.getLastRow() >= desRow) {
                        address.setLastRow(address.getLastRow() + n);
                    }
                }
                for (int j = removesAddress.size(); j > 0; j--) {
                    container.getCellRangeAddressList().remove(removesAddress.remove(j - 1));
                }
                if (container.getCellRangeAddressList().countRanges() == 0) {
                    removesContainers.add(container);
                }
            }
            for (DataValidationContainer container : removesContainers) {
                validationContainers.remove(container);
            }
            if (validationContainers.isEmpty()) dataValidationMap.remove(getKey(sheetName));
        }
    }

    /**
     * Копируем строки внутри листа
     *
     * @param _sheet      Лист в котором проводим операцию
     * @param starRow    Номер первой строки которые копируем
     * @param endRow     Номер последней строки которую копируем
     * @param destRowNum Номер строки куда вставляем скопированные строки
     */
    @Override
    public void copyRows(Sheet _sheet, int starRow, int endRow, int destRowNum) {
        XSSFSheet sheet = (XSSFSheet) _sheet;
        int delta = destRowNum - starRow;
        for (int i = endRow; i >= starRow; i--) {
            XSSFRow copiedRow = sheet.getRow(i);
            if (copiedRow == null) copiedRow = sheet.createRow(i);
            XSSFRow destRow = sheet.getRow(i + delta);
            if (destRow == null) destRow = sheet.createRow(i + delta);

            if (copiedRow.getZeroHeight()) {
                destRow.setZeroHeight(true);
            }
            destRow.setRowStyle(copiedRow.getRowStyle());
            destRow.setHeight(copiedRow.getHeight());
            destRow.getCTRow().setCustomHeight(copiedRow.getCTRow().getCustomHeight());

            //Ячейки с какой по какую начинаем копировать строку
            int firstCellNum = Math.min(copiedRow.getFirstCellNum(), destRow.getFirstCellNum());
            firstCellNum = Math.max(firstCellNum, 0);
            int lastCellNum = Math.max(copiedRow.getLastCellNum(), destRow.getLastCellNum());
            lastCellNum = Math.max(lastCellNum, 1);

            for (int y = firstCellNum; y < lastCellNum; y++) {
                //Получаем старую ячейку
                XSSFCell copiedCell = copiedRow.getCell(y);
                if (copiedCell != null) {
                    //Очищаем новую ячейку
                    XSSFCell destCell = destRow.getCell(y);
                    if (destCell != null) destRow.removeCell(destCell);
                    destCell = destRow.createCell(y, copiedCell.getCellType());
                    destCell.copyCellFrom(copiedCell, new CellCopyPolicy());
                    destCell.setCellStyle(copiedCell.getCellStyle());
                } else {
                    XSSFCell destCell = destRow.getCell(y);
                    if (destCell != null) destRow.removeCell(destCell);
                }
            }
        }

        copyFormulasWhenRowsCopy(sheet.getSheetName(), starRow, endRow, destRowNum);
        copyDataValidatorWhenRowsCopy(sheet.getSheetName(), starRow, endRow, destRowNum);
        //Копируем объединенные области
        List<CellRangeAddress> cellRangeList = workbookCellRangeMap.get(sheet.getSheetName());
        for (int i = 0; i < cellRangeList.size(); i++) {
            CellRangeAddress cellAddresses = cellRangeList.get(i);
            if (cellAddresses.getFirstRow() >= starRow && cellAddresses.getLastRow() <= endRow) {
                int firstRow = cellAddresses.getFirstRow() + delta;
                int lastRow = cellAddresses.getLastRow() + delta;
                int firstCol = cellAddresses.getFirstColumn();
                int lastCol = cellAddresses.getLastColumn();
                cellRangeList.add(new CellRangeAddress(firstRow, lastRow, firstCol, lastCol));
            }
        }
    }

    private void copyDataValidatorWhenRowsCopy(String sheetName, int starRow, int endRow, int destRowNum) {
        int delta = destRowNum - starRow;
        List<DataValidationContainer> validationContainers = dataValidationMap.get(getKey(sheetName));
        List<DataValidationContainer> addedContainers = new ArrayList<>();
        if (validationContainers != null) {
            for (DataValidationContainer container : validationContainers) {
                DataValidationContainer createdContainer = new DataValidationContainer(container, new CellRangeAddressList());
                for (CellRangeAddress address : container.getCellRangeAddressList().getCellRangeAddresses()) {
                    if (address.getFirstRow() >= starRow &&
                            address.getFirstRow() <= endRow &&
                            address.getLastRow() >= starRow &&
                            address.getLastRow() <= endRow) {
                        createdContainer.getCellRangeAddressList().addCellRangeAddress(new CellRangeAddress(
                                address.getFirstRow() + delta,
                                address.getLastRow() + delta,
                                address.getFirstColumn(),
                                address.getLastColumn()));
                    }
                }
                if (createdContainer.getCellRangeAddressList().countRanges() > 0) {
                    addedContainers.add(createdContainer);
                }
            }
            validationContainers.addAll(addedContainers);
        }
    }

    /**
     * Удаляем строки из листа
     *
     * @param _sheet      Лист в котором проводим операцию
     * @param destRowNum Номер строки которую удаляем
     * @param n          Количество удаляемых строк
     */
    @Override
    public void removeRows(Sheet _sheet, int destRowNum, int n) {
        XSSFSheet sheet = (XSSFSheet) _sheet;
        int lastRowNum = sheet.getLastRowNum();
        for (int i = destRowNum; i < lastRowNum; i++) {
            XSSFRow deletedRow = sheet.getRow(i);
            if (deletedRow == null) deletedRow = sheet.createRow(i);
            XSSFRow movedRow = sheet.getRow(i + n);
            if (movedRow == null) movedRow = sheet.createRow(i + n);

            //Ячейки с какой по какую начинаем копировать строку
            int firstCellNum = Math.min(deletedRow.getFirstCellNum(), movedRow.getFirstCellNum());
            firstCellNum = Math.max(firstCellNum, 0);
            int lastCellNum = Math.max(deletedRow.getLastCellNum(), movedRow.getLastCellNum());
            lastCellNum = Math.max(lastCellNum, 1);

            for (int y = firstCellNum; y < lastCellNum; y++) {
                //Получаем старую ячейку
                XSSFCell movedCell = movedRow.getCell(y);
                if (movedCell != null) {
                    //Очищаем новую ячейку
                    XSSFCell deletedCell = deletedRow.getCell(y);
                    if (deletedCell != null) deletedRow.removeCell(deletedCell);
                    deletedCell = deletedRow.createCell(y, movedCell.getCellType());
                    deletedCell.copyCellFrom(movedCell, new CellCopyPolicy());
                    deletedCell.setCellStyle(movedCell.getCellStyle());
                } else {
                    XSSFCell deletedCell = deletedRow.getCell(y);
                    if (deletedCell != null) deletedRow.removeCell(deletedCell);
                }
            }

            if (movedRow.getZeroHeight()) {
                deletedRow.setZeroHeight(true);
            } else {
                deletedRow.setZeroHeight(false);
            }

            deletedRow.setRowStyle(movedRow.getRowStyle());
            deletedRow.setHeight(movedRow.getHeight());
            deletedRow.getCTRow().setCustomHeight(movedRow.getCTRow().getCustomHeight());
            sheet.removeRow(movedRow);
        }

        calculateFormulaForRow(sheet.getSheetName(), destRowNum, -n);
        calculateDataValidationForRow(sheet.getSheetName(), destRowNum, -n);

        //Запоминаем объединенные ячейки и удаляем, те которые нам нужно изменить
        List<CellRangeAddress> cellRangeList = workbookCellRangeMap.get(sheet.getSheetName());
        int endColumn = destRowNum + n - 1;
        for (int i = cellRangeList.size() - 1; i >= 0; i--) {
            CellRangeAddress cellAddress = cellRangeList.get(i);
            int cellAddressStart = cellAddress.getFirstRow();
            int cellAddressEnd = cellAddress.getLastRow();
            if (cellAddressStart >= destRowNum && cellAddressEnd <= endColumn) {
                cellRangeList.remove(i);
            } else if (cellAddressStart >= destRowNum) {
                cellRangeList.remove(i);
                int firstRow = Math.max(destRowNum - 1, cellAddressStart - n);
                cellAddress.setFirstRow(firstRow);
                int lastRow = Math.max(destRowNum - 1, cellAddressEnd - n);
                cellAddress.setLastRow(lastRow);
                int sizeCellRangeRow = lastRow - firstRow;
                int sizeCellRangeColumn = cellAddress.getLastColumn() - cellAddress.getFirstColumn();
                if (sizeCellRangeRow > 0 || sizeCellRangeColumn > 0) cellRangeList.add(cellAddress);
            } else if (cellAddressEnd >= destRowNum) {
                cellRangeList.remove(i);
                int lastRow = Math.max(destRowNum - 1, cellAddressEnd - n);
                cellAddress.setLastRow(lastRow);
                int sizeCellRangeRow = lastRow - cellAddressStart;
                int sizeCellRangeColumn = cellAddress.getLastColumn() - cellAddress.getFirstColumn();
                if (sizeCellRangeRow > 0 || sizeCellRangeColumn > 0) cellRangeList.add(cellAddress);
            }
        }
    }

    /**
     * Метод вставляет столбцы на лист
     *
     * @param _sheet     Лист в котором проводим операцию
     * @param desColumn Номер столбца в начиная с которого добавляем столбцы
     * @param n         Количество добавляемых столбцов
     */
    @Override
    public void insertColumns(Sheet _sheet, int desColumn, int n) {
        XSSFSheet sheet = (XSSFSheet) _sheet;
        if (lastColumnMap.get(sheet.getSheetName()) != null) {
            int lastColumn = lastColumnMap.get(sheet.getSheetName()) + n;
            lastColumnMap.put(sheet.getSheetName(), lastColumn);
        }
        int lastColumnAllRows = 0;
        for (Row row : sheet) {
            XSSFRow workedRow = (XSSFRow) row;
            int lastColumn = workedRow.getLastCellNum();
            lastColumn = Math.max(lastColumn + n, desColumn + n);
            if (row.getLastCellNum() + n > lastColumnAllRows) lastColumnAllRows = row.getLastCellNum() + n;
            for (int i = lastColumn; i > desColumn; i--) {
                XSSFCell cell = workedRow.getCell(i);
                if (cell == null) cell = workedRow.createCell(i);
                XSSFCell prevCell = workedRow.getCell(i - n);
                if (prevCell == null) {
                    workedRow.removeCell(cell);
                } else {
                    cell.setCellType(prevCell.getCellTypeEnum());
                    cell.copyCellFrom(prevCell, new CellCopyPolicy());
                    workedRow.removeCell(prevCell);
//                    workedRow.createCell(i - n);
                }
            }
        }

        calculateFormulaForColumn(sheet.getSheetName(), desColumn, n);
        calculateDataValidationForColumns(sheet.getSheetName(), desColumn, n);

        //Двигаем скрытые столбцы
        for (int i = lastColumnAllRows; i >= desColumn; i--) {
            //Копируем ширину столбца
            if (i >= desColumn + n) sheet.setColumnWidth(i, sheet.getColumnWidth(i - n));
            if (sheet.isColumnHidden(i)) {
                sheet.setColumnHidden(i, false);
                sheet.setColumnHidden(i + n, true);
            }
        }

        //Запоминаем объединенные ячейки и удаляем, те которые нам нужно изменить
        List<CellRangeAddress> cellRangeList = workbookCellRangeMap.get(sheet.getSheetName());
        for (int i = 0; i < cellRangeList.size(); i++) {
            CellRangeAddress cellAddress = cellRangeList.get(i);
            int cellAddressStart = cellAddress.getFirstColumn();
            int cellAddressEnd = cellAddress.getLastColumn();
            if (cellAddressStart < desColumn &&
                    cellAddressEnd > desColumn) {
                cellAddress.setLastColumn(cellAddressEnd + n);
            } else if (cellAddressStart >= desColumn) {
                cellAddress.setFirstColumn(cellAddressStart + n);
                cellAddress.setLastColumn(cellAddressEnd + n);
            }
        }
    }

    private void calculateDataValidationForColumns(String sheetName, int desCol, int n) {
        List<DataValidationContainer> validationContainers = dataValidationMap.get(getKey(sheetName));
        List<Integer> removesAddress = new ArrayList<>();
        List<DataValidationContainer> removesContainers = new ArrayList<>();
        if (validationContainers != null) {
            for (DataValidationContainer container : validationContainers) {
                int addressSize = container.getCellRangeAddressList().countRanges();
                for (int j = 0; j < addressSize; j++) {
                    CellRangeAddress address = container.getCellRangeAddressList().getCellRangeAddress(j);
                    if (n < 0 &&
                            address.getFirstColumn() >= desCol &&
                            address.getFirstColumn() < desCol - n &&
                            address.getLastColumn() >= desCol &&
                            address.getLastColumn() < desCol - n) {
                        removesAddress.add(j);
                        continue;
                    }
                    if (address.getFirstColumn() >= desCol) {
                        address.setFirstColumn(address.getFirstColumn() + n);
                    }
                    if (address.getLastColumn() >= desCol) {
                        address.setLastColumn(address.getLastColumn() + n);
                    }
                }
                for (int j = removesAddress.size(); j > 0; j--) {
                    container.getCellRangeAddressList().remove(removesAddress.remove(j - 1));
                }
                if (container.getCellRangeAddressList().countRanges() == 0) {
                    removesContainers.add(container);
                }
            }
            for (DataValidationContainer container : removesContainers) {
                validationContainers.remove(container);
            }
            if (validationContainers.isEmpty()) dataValidationMap.remove(getKey(sheetName));
        }
    }

    /**
     * Копируем столбцы внутри листа
     *
     * @param _sheet         Лист в котором проводим операцию
     * @param startColumn   Номер первого столбца которые копируем
     * @param endColumn     Номер последней столбца которую копируем
     * @param destColumnNum Номер столбца куда вставляем скопированные столбцы
     */
    @Override
    public void copyColumns(Sheet _sheet, int startColumn, int endColumn, int destColumnNum) {
        XSSFSheet sheet = (XSSFSheet) _sheet;
        int delta = destColumnNum - startColumn;
        for (Row row : sheet) {
            XSSFRow workingRow = (XSSFRow) row;
            for (int i = startColumn; i <= endColumn; i++) {
                XSSFCell copiedCell = workingRow.getCell(i);
                if (copiedCell != null) {
                    XSSFCell destCell = workingRow.getCell(i + delta);
                    if (destCell != null) workingRow.removeCell(destCell);
                    destCell = workingRow.createCell(i + delta, copiedCell.getCellType());
                    destCell.copyCellFrom(copiedCell, new CellCopyPolicy());
                } else {
                    XSSFCell destCell = workingRow.getCell(i + delta);
                    if (destCell != null) workingRow.removeCell(destCell);
                }
            }
        }

        copyFormulasWhenColumnCopy(sheet.getSheetName(), startColumn, endColumn, destColumnNum);
        copyDataValidatorWhenColumnCopy(sheet.getSheetName(), startColumn, endColumn, destColumnNum);

        //Копируем скрытые столбцы
        for (int i = startColumn; i <= endColumn; i++) {
            //Копируем ширину столбца
            sheet.setColumnWidth(i + delta, sheet.getColumnWidth(i));
            if (sheet.isColumnHidden(i)) {
                sheet.setColumnHidden(i + delta, true);
            }
        }

        //Копируем объединенные области
        List<CellRangeAddress> cellRangeList = workbookCellRangeMap.get(sheet.getSheetName());
        for (int i = 0; i < cellRangeList.size(); i++) {
            CellRangeAddress cellAddresses = cellRangeList.get(i);
            if (cellAddresses.getFirstColumn() >= startColumn && cellAddresses.getLastColumn() <= endColumn) {
                int firstRow = cellAddresses.getFirstRow();
                int lastRow = cellAddresses.getLastRow();
                int firstCol = cellAddresses.getFirstColumn() + delta;
                int lastCol = cellAddresses.getLastColumn() + delta;
                cellRangeList.add(new CellRangeAddress(firstRow, lastRow, firstCol, lastCol));
            }
        }
    }

    private void copyDataValidatorWhenColumnCopy(String sheetName, int starCol, int endCol, int destColNum) {
        int delta = destColNum - starCol;
        List<DataValidationContainer> validationContainers = dataValidationMap.get(getKey(sheetName));
        List<DataValidationContainer> addedContainers = new ArrayList<>();
        if (validationContainers != null) {
            for (DataValidationContainer container : validationContainers) {
                DataValidationContainer createdContainer = new DataValidationContainer(container, new CellRangeAddressList());
                for (CellRangeAddress address : container.getCellRangeAddressList().getCellRangeAddresses()) {
                    if (address.getFirstColumn() >= starCol &&
                            address.getFirstColumn() <= endCol &&
                            address.getLastColumn() >= starCol &&
                            address.getLastColumn() <= endCol) {
                        createdContainer.getCellRangeAddressList().addCellRangeAddress(new CellRangeAddress(
                                address.getFirstRow(),
                                address.getLastRow(),
                                address.getFirstColumn() + delta,
                                address.getLastColumn() + delta));
                    }
                }
                if (createdContainer.getCellRangeAddressList().countRanges() > 0) {
                    addedContainers.add(createdContainer);
                }
            }
            validationContainers.addAll(addedContainers);
        }
    }

    /**
     * Удаляем столбцы из листа
     *
     * @param _sheet         Лист в котором проводим операцию
     * @param destColumnNum Номер столбца который удаляем
     * @param n             Количество удаляемых столбцов
     */
    @Override
    public void removeColumns(Sheet _sheet, int destColumnNum, int n) {
        XSSFSheet sheet = (XSSFSheet) _sheet;
        if (lastColumnMap.get(sheet.getSheetName()) != null) {
            int lastColumn = lastColumnMap.get(sheet.getSheetName()) - n;
            lastColumnMap.put(sheet.getSheetName(), lastColumn);
        }
        int lastColumnAllRows = 0;
        for (Row row : sheet) {
            XSSFRow workedRow = (XSSFRow) row;
            int lastColumn = workedRow.getLastCellNum();
            if (row.getLastCellNum() > lastColumnAllRows) lastColumnAllRows = lastColumn;
            lastColumn = Math.max(lastColumn, destColumnNum + n);
            for (int i = destColumnNum; i < lastColumn - n; i++) {
                XSSFCell deletedCell = workedRow.getCell(i);
                if (deletedCell == null) deletedCell = workedRow.createCell(i);
                XSSFCell movedCell = workedRow.getCell(i + n);
                if (movedCell == null) {
                    workedRow.removeCell(deletedCell);
                } else {
                    deletedCell.setCellType(movedCell.getCellTypeEnum());
                    deletedCell.copyCellFrom(movedCell, new CellCopyPolicy());
                    workedRow.removeCell(movedCell);
                }
            }
        }
        for (Row row : sheet) {
            XSSFRow workedRow = (XSSFRow) row;
            int lastColumn = workedRow.getLastCellNum();
            for (int i = lastColumnAllRows - n; i < lastColumn; i++) {
                XSSFCell removeCell = workedRow.getCell(i);
                if (removeCell != null) workedRow.removeCell(removeCell);
            }
        }

        calculateFormulaForColumn(sheet.getSheetName(), destColumnNum, -n);
        calculateDataValidationForColumns(sheet.getSheetName(), destColumnNum, -n);

        //Двигаем скрытые столбцы
        for (int i = destColumnNum; i < lastColumnAllRows; i++) {
            //Копируем ширину столбца
            if (i >= destColumnNum + n) sheet.setColumnWidth(i - n, sheet.getColumnWidth(i));
            if (sheet.isColumnHidden(i)) {
                sheet.setColumnHidden(i, false);
                if (i >= destColumnNum + n) sheet.setColumnHidden(i - n, true);
            }
        }

        //Запоминаем объединенные ячейки и удаляем, те которые нам нужно изменить
        List<CellRangeAddress> cellRangeList = workbookCellRangeMap.get(sheet.getSheetName());
        int endColumn = destColumnNum + n - 1;
        for (int i = cellRangeList.size() - 1; i >= 0; i--) {
            CellRangeAddress cellAddress = cellRangeList.get(i);
            int cellAddressStart = cellAddress.getFirstColumn();
            int cellAddressEnd = cellAddress.getLastColumn();
            if (cellAddressStart >= destColumnNum && cellAddressEnd <= endColumn) {
                cellRangeList.remove(i);
            } else if (cellAddressStart >= destColumnNum) {
                cellRangeList.remove(i);
                int firstCol = Math.max(destColumnNum - 1, cellAddressStart - n);
                cellAddress.setFirstColumn(firstCol);
                int lastCol = Math.max(destColumnNum - 1, cellAddressEnd - n);
                cellAddress.setLastColumn(lastCol);
                int sizeCellRangeCol = lastCol - firstCol;
                int sizeCellRangeRow = cellAddress.getLastRow() - cellAddress.getFirstRow();
                if (sizeCellRangeCol > 0 || sizeCellRangeRow > 0) cellRangeList.add(cellAddress);
            } else if (cellAddressEnd >= destColumnNum) {
                cellRangeList.remove(i);
                int lastCol = Math.max(destColumnNum - 1, cellAddressEnd - n);
                cellAddress.setLastColumn(lastCol);
                int sizeCellRangeCol = lastCol - cellAddressStart;
                int sizeCellRangeRow = cellAddress.getLastRow() - cellAddress.getFirstRow();
                if (sizeCellRangeRow > 0 || sizeCellRangeCol > 0) cellRangeList.add(cellAddress);
            }
        }
    }

    /**
     * Метод востанавливает в книгу информацию о объединенных ячейках
     *
     * @param workbook
     */
    @Override
    public void restoreMergedRegions(Workbook workbook) {
        for (String sheetName : workbookCellRangeMap.keySet()) {
            Sheet sheet = workbook.getSheet(sheetName);
            for (CellRangeAddress address : workbookCellRangeMap.get(sheetName)) sheet.addMergedRegionUnsafe(address);
        }
    }

    /**
     * Метод при копировании листа копирует информацию в новый лист о объединенных ячейках
     *
     * @param sheetName    Имя копируемого листа
     * @param newSheetName Имя создаваемого листа
     */
    @Override
    public void copySheet(String sheetName, String newSheetName) {
        String regSheetName = getKey(sheetName);
        List<CellRangeAddress> copiedList = workbookCellRangeMap.get(regSheetName);
        if (copiedList != null) {
            List<CellRangeAddress> mergedList = new ArrayList<>();

            for (CellRangeAddress address : copiedList) {
                CellRangeAddress cellRangeAddress = new CellRangeAddress(address.getFirstRow(), address.getLastRow(),
                        address.getFirstColumn(), address.getLastColumn());
                mergedList.add(cellRangeAddress);
            }
            workbookCellRangeMap.put(newSheetName, mergedList);
        }

        Map<CellAddress, FormulaParser> copiedFormulasMap = allFormulasMap.get(regSheetName);
        if (copiedFormulasMap != null) {
            Map<CellAddress, FormulaParser> addFormulasMap = new HashMap<>();

            for (CellAddress cellAddress : copiedFormulasMap.keySet()) {
                FormulaParser formulaParser = copiedFormulasMap.get(cellAddress);
                FormulaParser addFormulaParser = new FormulaParser(formulaParser.formatAsString());
                CellAddress addCellAddress = new CellAddress(cellAddress.getRow(), cellAddress.getColumn());
                addFormulasMap.put(addCellAddress, addFormulaParser);
            }
            allFormulasMap.put(newSheetName, addFormulasMap);
        }

        List<DataValidationContainer> copiedValidationList = dataValidationMap.get(regSheetName);
        if (copiedValidationList != null) {
            List<DataValidationContainer> createdValidationList = new ArrayList<>();

            for (DataValidationContainer container : copiedValidationList) {
                DataValidationContainer validationContainer = new DataValidationContainer(container);
                createdValidationList.add(validationContainer);
            }
            dataValidationMap.put(newSheetName, createdValidationList);
        }
    }

    /**
     * При удалении листа удаляет информацию о объединенных ячейках
     *
     * @param sheetName Имя листа
     */
    @Override
    public void deleteSheet(String sheetName) {
        String regSheetName = getKey(sheetName);
        workbookCellRangeMap.remove(regSheetName);
        allFormulasMap.remove(regSheetName);
        dataValidationMap.remove(regSheetName);
    }

    /**
     * Метод получает объединяемую область в который входит ячейка
     *
     * @param sheetName Имя листа
     * @param cell      Ячейка которая должна входить в объединяему область
     * @return Возращает null, если ячейка не воходит ни в одну объединяемую область
     */
    private CellRangeAddress getRangeMergedCell(String sheetName, Cell cell) {
        List<CellRangeAddress> rangeList = workbookCellRangeMap.get(getKey(sheetName));
        CellRangeAddress cellRangeAddress = null;
        for (CellRangeAddress address : rangeList) {
            if (address.isInRange(cell)) {
                cellRangeAddress = address;
                break;
            }
        }
        return cellRangeAddress;
    }

    /**
     * Получаем суммарную ширину всех ячеек входящих в объединенную область
     *
     * @param sheet            Лист в котором происходит поиск
     * @param cellRangeAddress Объединяемая область
     * @return Возвращает int ширину всех ячеек в области
     */
    private int getWidthMergedCells(Sheet sheet, CellRangeAddress cellRangeAddress) {
        int width = 0;
        for (int i = cellRangeAddress.getFirstColumn(); i <= cellRangeAddress.getLastColumn(); i++) {
            width += sheet.getColumnWidth(i);
        }
        return width;
    }

    /**
     * Получаем номер последнего столбца в листе
     *
     * @param sheet Лист в котором происходит поиск
     * @return int номер последнего столбца
     */
    private int getLastColumnInSheet(Sheet sheet) {
        int lastCol = 0;
        if (lastColumnMap.get(sheet.getSheetName()) == null) {
            for (int i = 0; i < sheet.getLastRowNum(); i++) {
                Row row = sheet.getRow(i);
                if (row != null && lastCol < row.getLastCellNum()) lastCol = row.getLastCellNum();
            }
            lastColumnMap.put(sheet.getSheetName(), lastCol);
        } else {
            lastCol = lastColumnMap.get(sheet.getSheetName());
        }
        return lastCol;
    }

    /**
     * Получаем ячейку которая шириной как объединенная область и содержит весь текст
     * ячейки которая входит в объединенную область, для получения ее высоты
     *
     * @param sheet      Лист в котором надо получить ячейку
     * @param lastColumn Последняя колонка на листе
     * @param rowNumber  Номер строки на которую надо добавить ячейку
     * @return Возращает ячейку
     */
    private Cell getHelperCell(Sheet sheet, int lastColumn, int rowNumber) {
        Row row = sheet.getRow(rowNumber);
        Cell resultCell = row.getCell(lastColumn);
        while (resultCell != null) resultCell = row.getCell(++lastColumn);
        resultCell = row.createCell(lastColumn);
        return resultCell;
    }

    /**
     * Если ячейка вхоидт в объединяему область то получаем для нее ячейку которая шириной как объединяемая область
     * для того что бы работала автовысота на объединенных ячейках
     *
     * @param sheetName Имя листа в котором надо получить ячейку
     * @param cell      Ячейка в которую пишем значение
     * @return Возвращает null если ячейка не находится в объединенной области
     */
    @Override
    public Cell getHelperCellForAutoHeightMergedCell(String sheetName, Cell cell) {
        Cell helperCell = null;
        CellRangeAddress cellRangeAddress = getRangeMergedCell(sheetName, cell);
        if (cellRangeAddress != null) {
            XSSFSheet sheet = workbook.getSheet(getKey(sheetName));
            int width = getWidthMergedCells(sheet, cellRangeAddress);
            int lastColumn = getLastColumnInSheet(sheet) + 50;
            helperCell = getHelperCell(sheet, lastColumn, cell.getRowIndex());
            helperCell.setCellStyle(cell.getCellStyle());
            sheet.setColumnHidden(helperCell.getColumnIndex(), true);
            sheet.setColumnWidth(helperCell.getColumnIndex(), width);
        }
        return helperCell;
    }

    @Override
    public void restoreAllFormulas(Workbook workbook) {
        for (String sheetName : allFormulasMap.keySet()) {
            Sheet sheet = workbook.getSheet(sheetName);
            Map<CellAddress, FormulaParser> sheetFormulasMap = allFormulasMap.get(sheetName);
            for (CellAddress cellAddress : sheetFormulasMap.keySet()) {
                FormulaParser formulaParser = sheetFormulasMap.get(cellAddress);
                Cell cell = sheet.getRow(cellAddress.getRow()).getCell(cellAddress.getColumn());
                try {
                    if (cell.getCellTypeEnum() != FORMULA) cell.setCellType(FORMULA);
                    cell.setCellFormula(formulaParser.formatAsString());
                } catch (FormulaParseException ignored) {
                }
            }
        }
    }
}
