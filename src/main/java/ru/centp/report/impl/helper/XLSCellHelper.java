package ru.centp.report.impl.helper;

import org.apache.poi.hssf.record.ExtendedFormatRecord;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.model.ThemesTable;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellBorder;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTBorder;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTBorderPr;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTXf;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STBorderStyle;
import ru.centp.report.api.helper.CellHelper;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XLSCellHelper implements CellHelper {

    @Override
    public Cell getCellByName(Workbook workbook, String cellName) {
        return getCellByNameWithIndex(workbook, cellName, 0, 0);
    }

    private Cell getCellByNameWithIndex(Workbook workbook, String cellName, int x, int y) {
        Name name = workbook.getName(cellName);
        CellReference cellReference = new CellReference(name.getRefersToFormula());

        Sheet sheet = workbook.getSheet(cellReference.getSheetName());
        Row row = sheet.getRow(cellReference.getRow() + y);
        if (row == null) row = sheet.createRow(cellReference.getRow() + y);
        Cell cell = row.getCell(cellReference.getCol() + x);
        if (cell == null) cell = row.createCell(cellReference.getCol() + x);
        return cell;
    }

    @Override
    public Cell getCellByNameAndIndexRow(Workbook workbook, String cellName, String link) {
        int indexRow = 0;
        Pattern pattern = Pattern.compile("\\.\\d+");
        Matcher matcher = pattern.matcher(link);
        if (matcher.find()) {
            indexRow = Integer.parseInt(link.substring(matcher.start() + 1, matcher.end()));
        }

        return getCellByNameWithIndex(workbook, cellName, 0, indexRow);
    }

    @Override
    public Cell getCellByNameAndIndexColumn(Workbook workbook, String cellName, String link) {
        int indexColumn = 0;
        Pattern pattern = Pattern.compile("\\.\\d+");
        Matcher matcher = pattern.matcher(link);
        if (matcher.find()) {
            indexColumn = Integer.parseInt(link.substring(matcher.start() + 1, matcher.end()));
        }

        return getCellByNameWithIndex(workbook, cellName, indexColumn, 0);
    }

    @Override
    public Cell getCellByReference(Workbook workbook, CellReference reference) {
        Sheet sheet = workbook.getSheet(reference.getSheetName());
        Row row = sheet.getRow(reference.getRow());
        if (row == null) row = sheet.createRow(reference.getRow());
        Cell cell = row.getCell(reference.getCol());
        if (cell == null) cell = row.createCell(reference.getCol());
        return cell;
    }

    @Override
    public String covertRelativeFormulaToAbsolute(Cell destCell, String relativeFormula) {
        int oldRow = destCell.getRowIndex();
        int oldCol = destCell.getColumnIndex();

        Map<String, String> replaceMap = new HashMap<>();

        Pattern patternRelativeAddress = Pattern.compile(PATTERN_RELATIVE_FORMULA);
        Matcher matcherRelativeAddress = patternRelativeAddress.matcher(relativeFormula);

        while (matcherRelativeAddress.find()) {
            String address = relativeFormula.substring(matcherRelativeAddress.start(), matcherRelativeAddress.end());
            Pattern patternRow = Pattern.compile("R\\[-?\\d+]");
            Matcher matcherRow = patternRow.matcher(address);
            int row = 0;
            if (matcherRow.find()) {
                row = Integer.parseInt(address.substring(matcherRow.start() + 2, matcherRow.end() - 1));
            }
            Pattern patternColumn = Pattern.compile("C\\[-?\\d+]");
            Matcher matcherColumn = patternColumn.matcher(address);
            int col = 0;
            if (matcherColumn.find()) {
                col = Integer.parseInt(address.substring(matcherColumn.start() + 2, matcherColumn.end() - 1));
            }

            row = Math.abs(oldRow + row);
            col = Math.abs(oldCol + col);

            String replaceAddress = CellReference.convertNumToColString(col) + (row + 1);

            replaceMap.put(address, replaceAddress);
        }

        for (String address : replaceMap.keySet()) {
            relativeFormula = relativeFormula.replace(address, replaceMap.get(address));
        }
        return relativeFormula;
    }

    @Override
    public HSSFCellStyle setUpDiagonalBorder(BorderStyle borderStyle, Cell _cell) {
        HSSFCell cell = (HSSFCell) _cell;
        HSSFCellStyle style = (HSSFCellStyle) _cell.getSheet().getWorkbook().createCellStyle();
        style.cloneStyleFrom(cell.getCellStyle());
        try {
            Field field = style.getClass().getDeclaredField("_format");
            field.setAccessible(true);
            ExtendedFormatRecord formatRecord = (ExtendedFormatRecord)field.get(style);
            formatRecord.setIndentNotParentBorder(true);
            formatRecord.setAdtlDiagLineStyle(borderStyle.getCode());
            setShowDiagonalUpBorder(formatRecord, true);
            field.setAccessible(false);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return style;
    }

    @Override
    public HSSFCellStyle setBottomDiagonalBorder(BorderStyle borderStyle, Cell _cell) {
        HSSFCell cell = (HSSFCell) _cell;
        HSSFCellStyle style = (HSSFCellStyle) _cell.getSheet().getWorkbook().createCellStyle();
        style.cloneStyleFrom(cell.getCellStyle());
        try {
            Field field = style.getClass().getDeclaredField("_format");
            field.setAccessible(true);
            ExtendedFormatRecord formatRecord = (ExtendedFormatRecord)field.get(style);
            formatRecord.setIndentNotParentBorder(true);
            formatRecord.setAdtlDiagLineStyle(borderStyle.getCode());
            setShowDiagonalDownBorder(formatRecord, true);
            field.setAccessible(false);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return style;
    }

    private void setShowDiagonalDownBorder(ExtendedFormatRecord formatRecord, boolean up) {
        int diag = formatRecord.getDiag();
        if (diag == 0) diag = 1;
        else if (diag == 2) diag = 3;
        formatRecord.setDiag((short)diag);
    }

    private void setShowDiagonalUpBorder(ExtendedFormatRecord formatRecord, boolean up) {
        int diag = formatRecord.getDiag();
        if (diag == 0) diag = 2;
        else if (diag == 1) diag = 3;
        formatRecord.setDiag((short)diag);
    }

    @Override
    public BorderStyle getDiagonalBorderStyle(Cell _cell) {
        BorderStyle result = null;
        HSSFCell cell = (HSSFCell) _cell;
        HSSFCellStyle style = cell.getCellStyle();
        try {
            Field field = style.getClass().getDeclaredField("_format");
            field.setAccessible(true);
            ExtendedFormatRecord formatRecord = (ExtendedFormatRecord)field.get(style);
            formatRecord.setIndentNotParentBorder(true);
            short index = formatRecord.getAdtlDiagLineStyle();
            result = BorderStyle.valueOf(index);
            field.setAccessible(false);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public HSSFCellStyle setDiagonalColor(Cell _cell, Color _color) {
        HSSFCell cell = (HSSFCell) _cell;
        HSSFColor color = (HSSFColor) _color;
        HSSFCellStyle style = cell.getSheet().getWorkbook().createCellStyle();
        style.cloneStyleFrom(cell.getCellStyle());
        try {
            Field field = style.getClass().getDeclaredField("_format");
            field.setAccessible(true);
            ExtendedFormatRecord formatRecord = (ExtendedFormatRecord)field.get(style);
            formatRecord.setAdtlDiag(color.getIndex());
            field.setAccessible(false);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return style;
    }
}
