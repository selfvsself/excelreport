package ru.centp.report.impl.helper;

import com.sun.org.apache.bcel.internal.generic.RET;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.model.ThemesTable;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellBorder;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.*;
import ru.centp.report.api.helper.CellHelper;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XLSXCellHelper implements CellHelper {

    @Override
    public Cell getCellByReference(Workbook workbook, CellReference reference) {
        Sheet sheet = workbook.getSheet(reference.getSheetName());
        Row row = sheet.getRow(reference.getRow());
        if (row == null) row = sheet.createRow(reference.getRow());
        Cell cell = row.getCell(reference.getCol());
        if (cell == null) cell = row.createCell(reference.getCol());
        return cell;
    }

    @Override
    public String covertRelativeFormulaToAbsolute(Cell destCell, String relativeFormula) {
        int oldRow = destCell.getRowIndex();
        int oldCol = destCell.getColumnIndex();

        Map<String, String> replaceMap = new HashMap<>();

        Pattern patternRelativeAddress = Pattern.compile(PATTERN_RELATIVE_FORMULA);
        Matcher matcherRelativeAddress = patternRelativeAddress.matcher(relativeFormula);

        while (matcherRelativeAddress.find()) {
            String address = relativeFormula.substring(matcherRelativeAddress.start(), matcherRelativeAddress.end());
            Pattern patternRow = Pattern.compile("R\\[-?\\d+]");
            Matcher matcherRow = patternRow.matcher(address);
            int row = 0;
            if (matcherRow.find()) {
                row = Integer.parseInt(address.substring(matcherRow.start() + 2, matcherRow.end() - 1));
            }
            Pattern patternColumn = Pattern.compile("C\\[-?\\d+]");
            Matcher matcherColumn = patternColumn.matcher(address);
            int col = 0;
            if (matcherColumn.find()) {
                col = Integer.parseInt(address.substring(matcherColumn.start() + 2, matcherColumn.end() - 1));
            }

            row = Math.abs(oldRow  + row);
            col = Math.abs(oldCol + col);

            String replaceAddress = CellReference.convertNumToColString(col) + (row + 1);

            replaceMap.put(address, replaceAddress);
        }

        for (String address : replaceMap.keySet()) {
            relativeFormula = relativeFormula.replace(address, replaceMap.get(address));
        }
        return relativeFormula;
    }

    @Override
    public XSSFCellStyle setUpDiagonalBorder(BorderStyle borderStyle, Cell cell) {
        return setBorderDiagonal(borderStyle, (XSSFCell) cell, 1);
    }

    @Override
    public XSSFCellStyle setBottomDiagonalBorder(BorderStyle borderStyle, Cell cell) {
        return setBorderDiagonal(borderStyle, (XSSFCell) cell, 0);
    }

    @Override
    public BorderStyle getDiagonalBorderStyle(Cell _cell) {
        XSSFCell cell = (XSSFCell) _cell;
        XSSFWorkbook workbook = cell.getSheet().getWorkbook();
        XSSFCellStyle style = cell.getCellStyle();
        StylesTable stylesTable = workbook.getStylesSource();
        CTXf ctXf = style.getCoreXf();
        CTBorder ctBorder = getCTBorder(stylesTable, ctXf);
        if (!ctBorder.isSetDiagonal()) ctBorder.addNewDiagonal();
        return BorderStyle.valueOf((short) (ctBorder.getDiagonal().xgetStyle().enumValue().intValue() - 1));
    }

    @Override
    public XSSFCellStyle setDiagonalColor(Cell _cell, Color _color) {
        XSSFCell cell = (XSSFCell) _cell;
        XSSFColor color = (XSSFColor) _color;
        XSSFWorkbook workbook = cell.getSheet().getWorkbook();
        XSSFCellStyle style = workbook.createCellStyle();
        style.cloneStyleFrom(cell.getCellStyle());
        StylesTable stylesTable = workbook.getStylesSource();
        ThemesTable theme = stylesTable.getTheme();
        CTXf ctXf = style.getCoreXf();
        CTBorder ctBorder = getCTBorder(stylesTable, ctXf);
        CTBorderPr ctBorderPr = ctBorder.isSetDiagonal() ? ctBorder.getDiagonal() : ctBorder.addNewDiagonal();
        ctBorderPr.setColor(color.getCTColor());
        int id = stylesTable.putBorder(new XSSFCellBorder(ctBorder, theme));
        ctXf.setBorderId(id);
        ctXf.setApplyBorder(true);
        return style;
    }

    private XSSFCellStyle setBorderDiagonal(BorderStyle borderStyle, XSSFCell cell, int direction) {
        XSSFWorkbook workbook = cell.getSheet().getWorkbook();
        XSSFCellStyle style = workbook.createCellStyle();
        style.cloneStyleFrom(cell.getCellStyle());
        StylesTable stylesTable = workbook.getStylesSource();
        ThemesTable theme = stylesTable.getTheme();
        CTXf ctXf = style.getCoreXf();
        CTBorder ctBorder = getCTBorder(stylesTable, ctXf);
        CTBorderPr ctBorderPr = ctBorder.isSetDiagonal() ? ctBorder.getDiagonal() : ctBorder.addNewDiagonal();
        if (direction == 0) {
            if (borderStyle.equals(BorderStyle.NONE) && ctBorder.getDiagonal().isSetStyle()) {
                ctBorder.unsetDiagonalDown();
            } else {
                ctBorder.setDiagonalDown(true);
                ctBorderPr.setStyle(STBorderStyle.Enum.forInt(borderStyle.getCode() + 1));
            }
        } else if (direction == 1) {
            if (borderStyle.equals(BorderStyle.NONE) && ctBorder.getDiagonal().isSetStyle()) {
                ctBorder.unsetDiagonalUp();
            } else {
                ctBorder.setDiagonalUp(true);
                ctBorderPr.setStyle(STBorderStyle.Enum.forInt(borderStyle.getCode() + 1));
            }
        }
        int id = stylesTable.putBorder(new XSSFCellBorder(ctBorder, theme));
        ctXf.setBorderId(id);
        ctXf.setApplyBorder(true);
        return style;
    }

    private CTBorder getCTBorder(StylesTable stylesTable, CTXf ctXf) {
        CTBorder ctBorder;
        if (ctXf.getApplyBorder()) {
            int id = (int) ctXf.getBorderId();
            XSSFCellBorder cellBorder = stylesTable.getBorderAt(id);
            ctBorder = (CTBorder) cellBorder.getCTBorder().copy();
        } else {
            ctBorder = CTBorder.Factory.newInstance();
        }
        return ctBorder;
    }

    @Override
    public Cell getCellByName(Workbook workbook, String cellName) {
        return getCellByNameWithIndex(workbook, cellName, 0, 0);
    }

    private Cell getCellByNameWithIndex(Workbook workbook, String cellName, int x, int y) {
        Name name = workbook.getName(cellName);
        CellReference cellReference = new CellReference(name.getRefersToFormula());

        Sheet sheet = workbook.getSheet(cellReference.getSheetName());
        Row row = sheet.getRow(cellReference.getRow() + y);
        if (row == null) row = sheet.createRow(cellReference.getRow() + y);
        Cell cell = row.getCell(cellReference.getCol() + x);
        if (cell == null) cell = row.createCell(cellReference.getCol() + x);
        return cell;
    }

    @Override
    public Cell getCellByNameAndIndexRow(Workbook workbook, String cellName, String link) {
        int indexRow = 0;
        Pattern pattern = Pattern.compile("\\.\\d+");
        Matcher matcher = pattern.matcher(link);
        if (matcher.find()) {
            indexRow = Integer.parseInt(link.substring(matcher.start() + 1, matcher.end()));
        }

        return getCellByNameWithIndex(workbook, cellName, 0, indexRow);
    }

    @Override
    public Cell getCellByNameAndIndexColumn(Workbook workbook, String cellName, String link) {
        int indexColumn = 0;
        Pattern pattern = Pattern.compile("\\.\\d+");
        Matcher matcher = pattern.matcher(link);
        if (matcher.find()) {
            indexColumn = Integer.parseInt(link.substring(matcher.start() + 1, matcher.end()));
        }

        return getCellByNameWithIndex(workbook, cellName, indexColumn, 0);
    }
}