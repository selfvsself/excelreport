package ru.centp.report.impl.loader;

import ru.centp.report.api.loader.DataLoader;
import ru.centp.report.impl.model.ReportCommand;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JSONDataLoader implements DataLoader {

    private List<HashMap<String, String>> jsonMap;

    public JSONDataLoader(List<HashMap<String,String>> jsonMap) {
        if (jsonMap != null)
            this.jsonMap = jsonMap;
        else throw new NullPointerException("Json Map in JSONDataLoader = null");
    }

    @Override
    public List<ReportCommand> getCommands() {
        List<ReportCommand> resultList = parseJSONMap(jsonMap);

        return resultList;
    }

    private List<ReportCommand> parseJSONMap(List<HashMap<String,String>> jsonMap) {
        List<ReportCommand> resultList = new ArrayList<>();
        for (Map<String, String> map : jsonMap) {
            for (String key : map.keySet()) {
                ReportCommand command = new ReportCommand();
                command.setCommand("write_value_cell");
                command.setSheetName("Book 1");
                command.getArguments()[0] = key;
                command.setStrValue(map.get(key));
                resultList.add(command);
            }
        }
        return resultList;
    }
}
