package ru.centp.report.impl.loader;

import ru.centp.report.api.loader.DataLoader;
import ru.centp.report.impl.model.ReportCommand;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class SqlQueryDataLoader implements DataLoader {

    private DataSource dataSource;
    private final static String SQL_QUERY_FOR_GET_COMMANDS = "SELECT * FROM tr_excel_commands";

    public SqlQueryDataLoader(DataSource dataSource) {
        if (dataSource != null)
            this.dataSource = dataSource;
        else throw new NullPointerException("DataSource in SqlDataLoader = null");
    }

    @Override
    public List<ReportCommand> getCommands() {
        List<ReportCommand> resultList = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            ResultSet resultSet = getResultSet(connection, SQL_QUERY_FOR_GET_COMMANDS);
            resultList = parseResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        resultList.sort(Comparator.comparingInt(ReportCommand::getId));
        return resultList;
    }

    private ResultSet getResultSet(Connection connection, String sqlQuery) throws SQLException {
        Statement stmt = connection.createStatement();
        ResultSet resultSet = stmt.executeQuery(sqlQuery);
        return resultSet;
    }

    private List<ReportCommand> parseResultSet(ResultSet resultSet) throws SQLException {
        List<ReportCommand> resultList = new ArrayList<>();
        int i = 0;
        while (resultSet.next()) {
            ReportCommand command = new ReportCommand();
            command.setIdent(resultSet.getInt("ident"));
            command.setId(resultSet.getInt("id"));
            command.setCommand(resultSet.getString("command"));
            command.setSheetName(resultSet.getString("sheet_name"));
            command.getArguments()[0] = (resultSet.getString("argument1"));
            command.getArguments()[1] = (resultSet.getString("argument2"));
            command.getArguments()[2] = (resultSet.getString("argument3"));
            command.getArguments()[3] = (resultSet.getString("argument4"));
            command.getArguments()[4] = (resultSet.getString("argument5"));
            command.getArguments()[5] = (resultSet.getString("argument6"));
            command.getArguments()[6] = (resultSet.getString("argument7"));
            command.getArguments()[7] = (resultSet.getString("argument8"));
            command.getArguments()[8] = (resultSet.getString("argument9"));
            command.getArguments()[9] = (resultSet.getString("argument10"));
            command.getArguments()[10] = (resultSet.getString("argument11"));
            command.setDataType(resultSet.getDouble("data_type"));
            command.setStrValue(resultSet.getString("str_value"));
            command.setNumValue(resultSet.getDouble("num_value"));
            String dateTimeValue = resultSet.getString("date_value");
            if (dateTimeValue != null) {
                LocalDateTime dateTime = resultSet.getDate("date_value").toLocalDate().atStartOfDay();
                command.setDateValue(dateTime);
            }
            command.setBlobValue(resultSet.getBytes("blob_value"));
            if (command.getDataType() == 3) {
                try (Connection connection = dataSource.getConnection()) {
                    CallableStatement callableStatement = connection.prepareCall("{call p_system_file_load(?, ?, ?)}");
                    callableStatement.setLong(1, command.getId());
                    callableStatement.setString(2, "blob_value");
                    callableStatement.setString(3, "excel_commands");

                    if (callableStatement.execute()) {
                        ResultSet resultProc = callableStatement.getResultSet();
                        if (resultProc.next()) {
                            String queryResult = (String) resultProc.getString(1);
                            if (queryResult.startsWith("\\x")) queryResult = queryResult.substring(2);
                            byte[] result = hexStringToByteArray(queryResult);
                            command.setBlobValue(result);
                        }
                    }

                }
            }
            command.setClobValue(resultSet.getString("clob_value"));
            resultList.add(command);
            i++;
        }
        return resultList;
    }

    public byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
}
