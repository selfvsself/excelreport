package ru.centp.report.impl.loader;

import ru.centp.report.api.loader.DataLoader;
import ru.centp.report.impl.model.ReportCommand;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SqlDataLoader implements DataLoader {

//    private final TableService tableService;
    private String procedureName = "P_EXCEL_GET_COMMAND";

//    public SqlDataLoader(TableService tableService) {
//        if (tableService != null)
//            this.tableService = tableService;
//        else throw new NullPointerException("TableService in SqlDataLoader = null");
//    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    @Override
    public List<ReportCommand> getCommands() {
//        List<HashMap<String, String>> jsonMap = getMapOnTableservice(tableService);
//        List<ReportCommand> resultList = parseJSONMap(jsonMap);
//        return resultList;
        return new ArrayList<>();
    }

//    private List<HashMap<String, String>> getMapOnTableservice(TableService tableService) {
//        String sData = tableService.doAction("procedure", procedureName, null, null, null);
//
//        List<HashMap<String, String>> result = new ArrayList<>();
//        if (sData != null && !sData.trim().isEmpty())
//            try {
//                ObjectMapper mapper = new ObjectMapper();
//                JSONArray arr = new JSONArray(sData);
//                for (int i = 0; i < arr.length(); i++)
//                    result.add(mapper.readValue(arr.getJSONObject(i).toString(), new TypeReference<HashMap<String, String>>() {
//                    }));  //ArrayToString
//            } catch (Exception e) {
//                e.printStackTrace();
//                HashMap<String, String> err = new HashMap<>();
//                err.put("value", sData);
//                result.add(err);
//            }
//        return result;
//    }

    private List<ReportCommand> parseJSONMap(List<HashMap<String, String>> jsonMap) {
        List<ReportCommand> resultList = new ArrayList<>();
        for (Map<String, String> map : jsonMap) {
            ReportCommand command = new ReportCommand();
            command.setIdent(map.get("ident"));
            command.setId(map.get("id"));
            command.setCommand(map.get("command"));
            command.setSheetName(map.get("sheet_name"));
            command.getArguments()[0] = map.get("argument1");
            command.getArguments()[1] = map.get("argument2");
            command.getArguments()[2] = map.get("argument3");
            command.getArguments()[3] = map.get("argument4");
            command.getArguments()[4] = map.get("argument5");
            command.getArguments()[5] = map.get("argument6");
            command.getArguments()[6] = map.get("argument7");
            command.getArguments()[7] = map.get("argument8");
            command.getArguments()[8] = map.get("argument9");
            command.getArguments()[9] = map.get("argument10");
            command.getArguments()[10] = map.get("argument11");
            command.setDataType(map.get("data_type"));
            command.setStrValue(map.get("str_value"));
            command.setNumValue(map.get("num_value"));
            String dateTimeValue = map.get("date_value");
            if (dateTimeValue != null) {
                LocalDateTime dateTime = LocalDateTime.parse(dateTimeValue);
                command.setDateValue(dateTime);
            }
            //TODO Сделать поле для bytea
            command.setClobValue(map.get("clob_value"));
            resultList.add(command);
        }
        return resultList;
    }
}
