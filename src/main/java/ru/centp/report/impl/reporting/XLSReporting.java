package ru.centp.report.impl.reporting;

import org.apache.http.util.TextUtils;
import org.apache.poi.hssf.record.RowRecord;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.*;
import ru.centp.report.api.helper.CellHelper;
import ru.centp.report.api.helper.ImageHelper;
import ru.centp.report.api.helper.SheetHelper;
import ru.centp.report.api.loader.DataLoader;
import ru.centp.report.api.reporting.Reporting;
import ru.centp.report.impl.helper.HSSFSheetHelper;
import ru.centp.report.impl.helper.ImageHelperImp;
import ru.centp.report.impl.helper.XLSCellHelper;
import ru.centp.report.impl.model.OutputReport;
import ru.centp.report.impl.model.ReportCommand;

import java.io.*;
import java.lang.reflect.Field;
import java.util.*;
import java.util.regex.Pattern;

public class XLSReporting implements Reporting {

    private final DataLoader dataLoader;
    private List<ReportCommand> commands;
    private final File fileMaket;
    private String fileName;
    private String fileType = "";
    private CellHelper cellHelper;
    private SheetHelper sheetHelper;
    private ImageHelper imageHelper;
    private HSSFWorkbook workbook;
    private short lastColorIndex = 57;

    private Map<String, Map<String, AreaReference>> tempLinkMap = new HashMap<>();
    private Map<String, List<String>> pageBreakLink = new HashMap<>();

    public XLSReporting(DataLoader dataLoader, File fileMaket) {
        this.dataLoader = dataLoader;
        this.commands = this.dataLoader.getCommands();
        if (fileMaket == null) throw new NullPointerException(String.format("Файл макет равен null"));
        if (!fileMaket.exists())
            throw new NullPointerException(String.format("Файл макета %s не существует", fileMaket.getPath()));
        if (!fileMaket.isFile())
            throw new NullPointerException(String.format("Ошибка чтения файла %s", fileMaket.getPath()));
        this.fileMaket = fileMaket;
        fileName = fileMaket.getName();
        int separator = fileName.lastIndexOf(".");
        if (separator > 0) {
            fileType = fileName.substring(separator);
            fileName = fileName.substring(0, separator);
        }
    }

    @Override
    public OutputReport getOutputReport() {
        workbook = readWorkbook(fileMaket.getPath());
        sheetHelper = new HSSFSheetHelper(workbook);
        cellHelper = new XLSCellHelper();
        imageHelper = new ImageHelperImp();

        for (ReportCommand command : commands) {
            switch (command.getCommand().toUpperCase()) {
                case "CELL_FORMULA_WRITE":
                    command.setDataType(-99);
                    cellValueWrite(command);
                    break;
                case "CELL_VALUE_WRITE":
                    cellValueWrite(command);
                    break;
                case "LINE_APPEND":
                    lineAppend(command);
                    break;
                case "COLUMN_APPEND":
                    columnAppend(command);
                    break;
                case "LINE_CELL_FORMULA_WRITE":
                    command.setDataType(-99);
                    lineCellValueWrite(command);
                    break;
                case "LINE_CELL_VALUE_WRITE":
                    lineCellValueWrite(command);
                    break;
                case "COLUMN_CELL_FORMULA_WRITE":
                    command.setDataType(-99);
                    columnCellValueWrite(command);
                    break;
                case "COLUMN_CELL_VALUE_WRITE":
                    columnCellValueWrite(command);
                    break;
                case "LINE_DELETE":
                    lineDelete(command);
                    break;
                case "COLUMN_DELETE":
                    columnDelete(command);
                    break;
                case "CELL_ATTRIBUTE_SET":
                    cellAttributeSet(command);
                    break;
                case "LINE_CELL_ATTRIBUTE_SET":
                    lineCellAttributeSet(command);
                    break;
                case "COLUMN_CELL_ATTRIBUTE_SET":
                    columnCellAttributeSet(command);
                    break;
                case "SHEET_COPY":
                    sheetCopy(command);
                    break;
                case "SHEET_DELETE":
                    sheetDelete(command);
                    break;
                case "SHEET_PAGE_SETUP":
                    sheetPageSetup(command);
                    break;
                case "BOOK_SETUP":
                    bookSetup(command);
                    break;
                case "LINE_PAGE_BREAK":
                    addPageBreak(command);
                    break;
            }
        }
        sheetHelper.restoreMergedRegions(workbook);
        sheetHelper.restoreAllFormulas(workbook);
        sheetHelper.restoreAllDataValidation(workbook);
        imageHelper.renderAllImage(workbook);
//        sheetHelper.calculateAllFormulas();
        workbook.setForceFormulaRecalculation(true);
        addAllPageBreak();
        byte[] content = getByteArray(workbook);
        return new OutputReport(content, getFileName());
    }

    private void addAllPageBreak() {
        for (String sheetName : pageBreakLink.keySet()) {
            Sheet sheet = workbook.getSheet(sheetName);
            if (sheet != null) {
                if (sheet.getAutobreaks()) sheet.setAutobreaks(false);

                for (String areaLinkName : pageBreakLink.get(sheetName)) {
                    AreaReference link = getAreaReference(sheetName, areaLinkName);
                    if (link != null) {
                        sheet.setRowBreak(link.getFirstCell().getRow() - 1);
                    }
                }
            }
        }
    }

    private void addPageBreak(ReportCommand command) {

        String sheetName = command.getSheetName();
        String prevName = command.getArguments()[0];

        if (!pageBreakLink.containsKey(sheetName))
            pageBreakLink.put(sheetName, new ArrayList<>());

        pageBreakLink.get(sheetName).add(prevName);
    }

    private void cellValueWrite(ReportCommand command) {
        String sheetName = command.getSheetName();
        String linkName0 = command.getArguments()[0];
        String linkName1 = command.getArguments()[1];
        String linkName3 = command.getArguments()[3];
        HSSFCell linkCell = getCellByLink(sheetName, linkName0, linkName1, linkName3);
        if (linkCell != null) {
            setCellValue(linkCell, command);
        }
    }

    private boolean getRowCustomHeightCustom(HSSFRow row) {
        boolean isCustomHeight = false;
        try {
            Field field = row.getClass().getDeclaredField("row");
            field.setAccessible(true);
            RowRecord rowRecord = (RowRecord) field.get(row);
            isCustomHeight = rowRecord.getBadFontHeight();
            field.setAccessible(false);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return isCustomHeight;
    }

    private void setCellValue(HSSFCell cell, ReportCommand command) {
        Cell helperCell = null;
        boolean isCustomHeight = getRowCustomHeightCustom(cell.getRow());
        if (!isCustomHeight)
            helperCell = sheetHelper.getHelperCellForAutoHeightMergedCell(command.getSheetName(), cell);
        switch ((int) command.getDataType()) {
            case 0:
                if (cell.getCellTypeEnum() == CellType.FORMULA) sheetHelper.deleteFormula(cell);
                try {
                    cell.setCellType(CellType.STRING);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                cell.setCellValue(command.getStrValue());
                if (helperCell != null) {
                    try {
                        helperCell.setCellType(cell.getCellTypeEnum());
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
                    helperCell.setCellValue(command.getStrValue());
                }
                break;
            case 1:
                if (cell.getCellTypeEnum() == CellType.FORMULA) sheetHelper.deleteFormula(cell);
                try {
                    cell.setCellType(CellType.NUMERIC);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                cell.setCellValue(command.getNumValue());
                if (helperCell != null) {
                    try {
                        helperCell.setCellType(cell.getCellTypeEnum());
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
                    helperCell.setCellValue(command.getNumValue());
                }
                break;
            case 2:
                if (cell.getCellTypeEnum() == CellType.FORMULA) sheetHelper.deleteFormula(cell);
                cell.setCellValue(command.getDateValue());
                if (helperCell != null) {
                    try {
                        helperCell.setCellType(cell.getCellTypeEnum());
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
                    helperCell.setCellValue(command.getDateValue());
                }
                break;
            case 3:
                //ссылка на область в которой должна лежать картинка
                AreaReference areaReference = null;
                if (command.getArguments()[1] != null)
                    areaReference = getAreaReference(command.getSheetName(), command.getArguments()[1]);
                else  if (command.getArguments()[0] != null)
                    areaReference = getAreaReference(command.getSheetName(), command.getArguments()[0]);
                //нижняя правая граница картинки
                CellReference cell2 = new CellReference(cell.getRowIndex(), cell.getColumnIndex());
                if (areaReference != null) {
                    CellReference firstCell = areaReference.getFirstCell();
                    CellReference lastCell = areaReference.getLastCell();
                    if (firstCell != null && lastCell != null) {
                        int dX = lastCell.getCol() - firstCell.getCol();
                        int dY = lastCell.getRow() - firstCell.getRow();
                        cell2 = new CellReference(cell.getRowIndex() + dY + 1, cell.getColumnIndex() + dX + 1);
                    }
                }
                if (command.getBlobValue() != null)
                    imageHelper.addImage(command.getBlobValue(),
                            new AreaReference(new CellReference(cell.getRowIndex(), cell.getColumnIndex()), cell2),
                            cell.getSheet().getSheetName());
                break;
            case -99:
                try {
                    cell.setCellType(CellType.FORMULA);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                String formula = command.getArguments()[5];
                if (formula == null) formula = command.getArguments()[3];
                if (formula.startsWith("=")) formula = formula.substring(1);
                if (Pattern.compile(CellHelper.PATTERN_RELATIVE_FORMULA).matcher(formula).find()) {
                    formula = cellHelper.covertRelativeFormulaToAbsolute(cell, formula);
                }
                cell.setCellFormula(formula);
                sheetHelper.addFormula(cell);
                break;
        }
    }

    private HSSFCell getCellByLink(String sheetName, String linkName0, String linkName1, String linkName3) {
        HSSFCell resultCell = null;
        if (linkName1 == null || linkName3 == null) {
            AreaReference reference = getAreaReference(sheetName, linkName0);
            //Проверяем что ссылка на одну ячейку
            if (reference != null) {
                Cell linkCell = cellHelper.getCellByReference(workbook, reference.getFirstCell());
                HSSFSheet sheet = workbook.getSheet(sheetName);
                if (sheet != null) {
                    HSSFRow row = sheet.getRow(linkCell.getRowIndex());
                    HSSFCell cell = row.getCell(linkCell.getColumnIndex());
                    resultCell = cell;
                }
            }
        } else {
            AreaReference referenceRow = getAreaReference(sheetName, linkName1);
            AreaReference referenceColumn = getAreaReference(sheetName, linkName3);
            if (referenceRow != null && referenceColumn != null) {
                HSSFSheet sheet = workbook.getSheet(sheetName);
                if (sheet != null) {
                    HSSFRow row = sheet.getRow(referenceRow.getFirstCell().getRow());
                    HSSFCell cell = row.getCell(referenceColumn.getFirstCell().getCol());
                    resultCell = cell;
                }
            }
        }

        if (resultCell == null) {
            String linkName = linkName3;
            if (linkName == null || linkName.trim().isEmpty()) {
                linkName = linkName1;
            }
            if (linkName == null || linkName.trim().isEmpty()) {
                linkName = linkName0;
            }
            if (linkName != null && !linkName.trim().isEmpty()) {
                if (Pattern.compile(CellHelper.PATTERN_RELATIVE_CELL_ADDRESS).matcher(linkName).find()) {
                    String sheetNameTmp = sheetName;
                    if (sheetNameTmp.contains(" ")) sheetNameTmp = "'" + sheetNameTmp + "'";
                    resultCell = (HSSFCell) cellHelper.getCellByRelativeReference(workbook, sheetNameTmp, linkName);
                } else if (Pattern.compile(CellHelper.PATTERN_CELL_ADDRESS).matcher(linkName).find()) {
                    String sheetNameTmp = sheetName;
                    if (sheetNameTmp.contains(" ")) sheetNameTmp = "'" + sheetNameTmp + "'";
                    resultCell = (HSSFCell) cellHelper.getCellByReference(workbook, new CellReference(sheetNameTmp + "!" + linkName));
                }
            }
        }

        return resultCell;
    }

    private AreaReference getAreaReference(String sheetName, String linkName) {
        Map<String, AreaReference> tmpSheetLinkMap = tempLinkMap.get(sheetName.toUpperCase());
        if (tmpSheetLinkMap == null) {
            tmpSheetLinkMap = new HashMap<>();
            for (Name copyName : workbook.getAllNames()) {
                if (!copyName.getRefersToFormula().contains("#REF") && copyName.getSheetName().equalsIgnoreCase(sheetName)) {
                    String key = copyName.getNameName().toUpperCase();
                    AreaReference tmpReference = new AreaReference(copyName.getRefersToFormula(), SpreadsheetVersion.EXCEL2007);
                    tmpSheetLinkMap.put(key, tmpReference);
                }
            }
            //Заполняем ссылки по адресу
            for (ReportCommand command : commands) {
                if (command.getSheetName() != null && command.getSheetName().equalsIgnoreCase(sheetName)) {
                    String address = command.getArguments()[2];
                    if (address != null &&
                            !address.trim().isEmpty() && (
                            Pattern.compile(CellHelper.PATTERN_LINE_ADDRESS).matcher(address).find() ||
                                    Pattern.compile(CellHelper.PATTERN_COLUMN_ADDRESS).matcher(address).find())) {
                        tmpSheetLinkMap.put(address, new AreaReference(address, SpreadsheetVersion.EXCEL97));
                    }
                }
            }
            tempLinkMap.put(sheetName.toUpperCase(), tmpSheetLinkMap);
        }
        AreaReference reference = null;
        if (linkName != null) {
            reference = tmpSheetLinkMap.get(linkName.toUpperCase());
            if (reference == null) {
                Name name = workbook.getName(linkName);
                if (name != null)
                    reference = new AreaReference(name.getRefersToFormula(), SpreadsheetVersion.EXCEL2007);
            }
        }
        return reference;
    }

    private void lineAppend(ReportCommand command) {
        String sheetName = command.getSheetName();

        String prevName = command.getArguments()[0];
        String copiedName = command.getArguments()[2];
        String createdName = command.getArguments()[1];

        AreaReference prevReference = getAreaReference(sheetName, prevName);
        AreaReference copiedReference = getAreaReference(sheetName, copiedName);
        if (prevReference != null && copiedReference != null) {
            int createdFirstRow = prevReference.getLastCell().getRow() + 1;
            int lengthCopiedRow = copiedReference.getLastCell().getRow() - copiedReference.getFirstCell().getRow() + 1;
            int createdLastRow = createdFirstRow + lengthCopiedRow - 1;
            int createdFirstColumn = copiedReference.getFirstCell().getCol();
            int createdLastColumn = copiedReference.getLastCell().getCol();
            CellReference firstCreatedCell = new CellReference(sheetName, createdFirstRow, createdFirstColumn, false, false);
            CellReference lastCreatedCell = new CellReference(sheetName, createdLastRow, createdLastColumn, false, false);
            AreaReference createdReference = new AreaReference(firstCreatedCell, lastCreatedCell);

            HSSFSheet sheet = workbook.getSheet(command.getSheetName());
            if (sheet != null) {
                sheetHelper.insertRows(sheet, createdFirstRow, lengthCopiedRow);
                addReferenceToTempLinkMap(sheetName, createdName, createdReference);
                copiedReference = getAreaReference(sheetName, copiedName);
                sheetHelper.copyRows(sheet, copiedReference.getFirstCell().getRow(), copiedReference.getLastCell().getRow(), createdFirstRow);
                imageHelper.addRows(sheet.getSheetName(), copiedReference, createdFirstRow);
            }
        }
    }

    private void addReferenceToTempLinkMap(String sheetName, String linkName, AreaReference reference) {
        Map<String, AreaReference> tmpSheetLinkMap = tempLinkMap.get(sheetName.toUpperCase());
        if (tmpSheetLinkMap == null) {
            tmpSheetLinkMap = new HashMap<>();
        }

        int lengthRows = reference.getLastCell().getRow() - reference.getFirstCell().getRow();
        int lengthCol = reference.getLastCell().getCol() - reference.getFirstCell().getCol();

        //Если эти значения -1 то значит мы выделяем всю строку, либо весь столбец
        int firstCol = reference.getFirstCell().getCol();
        int firstRow = reference.getFirstCell().getRow();

        for (String key : tmpSheetLinkMap.keySet()) {
            AreaReference areaReference = tmpSheetLinkMap.get(key);
            if ((lengthRows > 0 && lengthRows < 65534) || lengthCol > 200 || firstCol == -1) {
                if (reference.getFirstCell().getRow() > areaReference.getFirstCell().getRow() &&
                        reference.getLastCell().getRow() <= areaReference.getLastCell().getRow()) {
                    int deltaRow = reference.getLastCell().getRow() - reference.getFirstCell().getRow() + 1;

                    CellReference firstCreatedCell = areaReference.getFirstCell();
                    int newRowNum = areaReference.getLastCell().getRow() + deltaRow;
                    int newColumnNum = areaReference.getLastCell().getCol();
                    CellReference lastCreatedCell = new CellReference(sheetName, newRowNum, newColumnNum, false, false);
                    AreaReference changedReference = new AreaReference(firstCreatedCell, lastCreatedCell);
                    tmpSheetLinkMap.put(key, changedReference);
                } else if (reference.getLastCell().getRow() <= areaReference.getFirstCell().getRow()) {
                    int deltaRow = reference.getLastCell().getRow() - reference.getFirstCell().getRow() + 1;

                    int firstCellRow = areaReference.getFirstCell().getRow() + deltaRow;
                    int firstCellColumn = areaReference.getFirstCell().getCol();
                    CellReference firstCreatedCell = new CellReference(sheetName, firstCellRow, firstCellColumn, false, false);

                    int lastCellRow = areaReference.getLastCell().getRow() + deltaRow;
                    int lastCellColumn = areaReference.getLastCell().getCol();
                    CellReference lastCreatedCell = new CellReference(sheetName, lastCellRow, lastCellColumn, false, false);
                    AreaReference changedReference = new AreaReference(firstCreatedCell, lastCreatedCell);
                    tmpSheetLinkMap.put(key, changedReference);
                }
            } else if (lengthCol > 0 || firstRow == -1 || lengthRows >= 65535) {
                if (reference.getFirstCell().getCol() > areaReference.getFirstCell().getCol() &&
                        reference.getLastCell().getCol() <= areaReference.getLastCell().getCol()) {
                    int deltaCol = reference.getLastCell().getCol() - reference.getFirstCell().getCol() + 1;

                    CellReference firstCreatedCell = areaReference.getFirstCell();
                    int newRowNum = areaReference.getLastCell().getRow();
                    int newColNum = areaReference.getLastCell().getCol() + deltaCol;
                    CellReference lastCreatedCell = new CellReference(sheetName, newRowNum, newColNum, false, false);
                    AreaReference changedReference = new AreaReference(firstCreatedCell, lastCreatedCell);
                    tmpSheetLinkMap.put(key, changedReference);
                } else if (reference.getLastCell().getCol() <= areaReference.getFirstCell().getCol()) {
                    int deltaRow = reference.getLastCell().getCol() - reference.getFirstCell().getCol() + 1;

                    int firstCellCol = areaReference.getFirstCell().getCol() + deltaRow;
                    int firstCellRow = areaReference.getFirstCell().getRow();
                    CellReference firstCreatedCell = new CellReference(sheetName, firstCellRow, firstCellCol, false, false);

                    int lastCellCol = areaReference.getLastCell().getCol() + deltaRow;
                    int lastCellRow = areaReference.getLastCell().getRow();
                    CellReference lastCreatedCell = new CellReference(sheetName, lastCellRow, lastCellCol, false, false);
                    AreaReference changedReference = new AreaReference(firstCreatedCell, lastCreatedCell);
                    tmpSheetLinkMap.put(key, changedReference);
                }
            }
        }

        tmpSheetLinkMap.put(linkName.toUpperCase(), reference);
        tempLinkMap.put(sheetName.toUpperCase(), tmpSheetLinkMap);
    }

    private void columnAppend(ReportCommand command) {
        String sheetName = command.getSheetName();

        String prevName = command.getArguments()[0];
        String copiedName = command.getArguments()[2];
        String createdName = command.getArguments()[1];

        AreaReference prevReference = getAreaReference(sheetName, prevName);
        AreaReference copiedReference = getAreaReference(sheetName, copiedName);
        if (prevReference != null && copiedReference != null) {
            int createdFirstRow = copiedReference.getFirstCell().getRow();
            int createdLastRow = copiedReference.getLastCell().getRow();
            int createdFirstColumn = prevReference.getLastCell().getCol() + 1;
            int lengthCopiedColumn = copiedReference.getLastCell().getCol() - copiedReference.getFirstCell().getCol() + 1;
            int createdLastColumn = createdFirstColumn + lengthCopiedColumn - 1;
            CellReference firstCreatedCell = new CellReference(sheetName, createdFirstRow, createdFirstColumn, false, false);
            CellReference lastCreatedCell = new CellReference(sheetName, createdLastRow, createdLastColumn, false, false);
            AreaReference createdReference = new AreaReference(firstCreatedCell, lastCreatedCell);

            HSSFSheet sheet = workbook.getSheet(command.getSheetName());
            if (sheet != null) {
                sheetHelper.insertColumns(sheet, createdLastColumn, lengthCopiedColumn);
                addReferenceToTempLinkMap(sheetName, createdName, createdReference);
                copiedReference = getAreaReference(sheetName, copiedName);
                sheetHelper.copyColumns(sheet, copiedReference.getFirstCell().getCol(), copiedReference.getLastCell().getCol(), createdFirstColumn);
                imageHelper.addCols(sheet.getSheetName(), copiedReference, createdFirstColumn);
            }
        }
    }

    private void lineCellValueWrite(ReportCommand command) {
        String sheetName = command.getSheetName();
        String linkNameRow = command.getArguments()[0];
        String linkName = command.getArguments()[1];
        String parentLineLink = command.getArguments()[2];
        HSSFCell linkCell = getCellByLinkLine(sheetName, linkName, linkNameRow, parentLineLink);
        if (linkCell != null) {
            setCellValue(linkCell, command);
        }
    }

    private HSSFCell getCellByLinkLine(String sheetName, String linkName, String linkNameRow, String parentLineLink) {
        HSSFCell resultCell = null;
        HSSFCell linkCell = getCellByLink(sheetName, linkName, null, null);
        AreaReference rowReference = getAreaReference(sheetName, linkNameRow);
        if (linkCell != null && rowReference != null) {
            int delta = 0;
            AreaReference parentRowReference = getAreaReference(sheetName, parentLineLink);
            if (parentRowReference != null) {
                int cellRow = linkCell.getRowIndex();
                int parentLineRow = parentRowReference.getFirstCell().getRow();
                delta = cellRow - parentLineRow;
                if (delta < 0) delta = 0;
            }
            HSSFSheet sheet = workbook.getSheet(sheetName);
            if (sheet != null) {
                HSSFRow row = sheet.getRow(rowReference.getFirstCell().getRow() + delta);
                HSSFCell cell = row.getCell(linkCell.getColumnIndex());
                if (cell == null) {
                    cell = row.createCell(linkCell.getColumnIndex());
                    cell.setCellStyle(linkCell.getCellStyle());
                }
                resultCell = cell;
            }
        }
        return resultCell;
    }

    private void columnCellValueWrite(ReportCommand command) {
        String sheetName = command.getSheetName();
        String linkNameColumn = command.getArguments()[0];
        String linkName = command.getArguments()[1];
        String parentColumnLink = command.getArguments()[2];
        HSSFCell linkCell = getCellByLinkColumn(sheetName, linkName, linkNameColumn, parentColumnLink);
        if (linkCell != null) {
            setCellValue(linkCell, command);
        }
    }

    private HSSFCell getCellByLinkColumn(String sheetName, String linkName, String linkNameColumn, String parentColumnLink) {
        HSSFCell resultCell = null;
        HSSFCell linkCell = getCellByLink(sheetName, linkName, null, null);
        AreaReference columnReference = getAreaReference(sheetName, linkNameColumn);
        if (linkCell != null && columnReference != null) {
            int delta = 0;
            AreaReference parentRowReference = getAreaReference(sheetName, parentColumnLink);
            if (parentRowReference != null) {
                int cellColumn = linkCell.getColumnIndex();
                int parentColumn = parentRowReference.getFirstCell().getCol();
                delta = cellColumn - parentColumn;
                if (delta < 0) delta = 0;
            }
            HSSFSheet sheet = workbook.getSheet(sheetName);
            if (sheet != null) {
                HSSFRow row = sheet.getRow(linkCell.getRowIndex());
                HSSFCell cell = row.getCell(columnReference.getFirstCell().getCol() + delta);
                if (cell == null) {
                    cell = row.createCell(linkCell.getColumnIndex());
                    cell.setCellStyle(linkCell.getCellStyle());
                }
                resultCell = cell;
            }
        }
        return resultCell;
    }

    private void lineDelete(ReportCommand command) {
        String linkCell = command.getArguments()[0];
        String sheetName = command.getSheetName();

        AreaReference deletingReference = getAreaReference(sheetName, linkCell);
        HSSFSheet sheet = workbook.getSheet(sheetName);
        if (deletingReference != null && sheet != null) {
            int destDeletingRow = deletingReference.getFirstCell().getRow();
            int sizeDeletingRows = deletingReference.getLastCell().getRow() - deletingReference.getFirstCell().getRow() + 1;

            sheetHelper.removeRows(sheet, destDeletingRow, sizeDeletingRows);
            deleteReferenceOnTempLinkMap(sheetName, linkCell);
            imageHelper.removeRows(sheetName, destDeletingRow, sizeDeletingRows);
        }
    }

    private void deleteReferenceOnTempLinkMap(String sheetName, String linkName) {
        Map<String, AreaReference> tmpSheetMap = tempLinkMap.get(sheetName.toUpperCase());
        AreaReference reference = null;
        if (tmpSheetMap != null) {
            reference = tmpSheetMap.remove(linkName.toUpperCase());
        }

        if (reference != null) {
            int lengthRows = reference.getLastCell().getRow() - reference.getFirstCell().getRow();
            int lengthCol = reference.getLastCell().getCol() - reference.getFirstCell().getCol();

            //Если эти значения -1 то значит мы выделяем всю строку, либо весь столбец
            int firstCol = reference.getFirstCell().getCol();
            int firstRow = reference.getFirstCell().getRow();

            for (String key : tmpSheetMap.keySet()) {
                AreaReference areaReference = tmpSheetMap.get(key);
                if ((lengthRows > 0 && lengthRows < 65534) || lengthCol > 200 || firstCol == -1) {
                    if (reference.getFirstCell().getRow() > areaReference.getFirstCell().getRow() &&
                            reference.getLastCell().getRow() < areaReference.getLastCell().getRow()) {
                        int deltaRow = reference.getLastCell().getRow() - reference.getFirstCell().getRow() + 1;

                        CellReference firstCreatedCell = areaReference.getFirstCell();
                        int newRowNum = areaReference.getLastCell().getRow() - deltaRow;
                        int newColumnNum = areaReference.getLastCell().getCol();
                        CellReference lastCreatedCell = new CellReference(sheetName, newRowNum, newColumnNum, false, false);
                        AreaReference changedReference = new AreaReference(firstCreatedCell, lastCreatedCell);
                        tmpSheetMap.put(key, changedReference);
                    }
                    if (reference.getLastCell().getRow() < areaReference.getFirstCell().getRow()) {
                        int deltaRow = reference.getLastCell().getRow() - reference.getFirstCell().getRow() + 1;

                        int firstCellRow = areaReference.getFirstCell().getRow() - deltaRow;
                        int firstCellColumn = areaReference.getFirstCell().getCol();
                        CellReference firstCreatedCell = new CellReference(sheetName, firstCellRow, firstCellColumn, false, false);

                        int lastCellRow = areaReference.getLastCell().getRow() - deltaRow;
                        int lastCellColumn = areaReference.getLastCell().getCol();
                        CellReference lastCreatedCell = new CellReference(sheetName, lastCellRow, lastCellColumn, false, false);
                        AreaReference changedReference = new AreaReference(firstCreatedCell, lastCreatedCell);
                        tmpSheetMap.put(key, changedReference);
                    }
                } else if (lengthCol > 0 || firstRow == -1 || lengthRows >= 65535) {
                    if (reference.getFirstCell().getCol() > areaReference.getFirstCell().getCol() &&
                            reference.getLastCell().getCol() < areaReference.getLastCell().getCol()) {
                        int deltaRow = reference.getLastCell().getCol() - reference.getFirstCell().getCol() + 1;

                        CellReference firstCreatedCell = areaReference.getFirstCell();
                        int newColNum = areaReference.getLastCell().getCol() - deltaRow;
                        int newRowNum = areaReference.getLastCell().getRow();
                        CellReference lastCreatedCell = new CellReference(sheetName, newRowNum, newColNum, false, false);
                        AreaReference changedReference = new AreaReference(firstCreatedCell, lastCreatedCell);
                        tmpSheetMap.put(key, changedReference);
                    }
                    if (reference.getLastCell().getCol() < areaReference.getFirstCell().getCol()) {
                        int deltaRow = reference.getLastCell().getCol() - reference.getFirstCell().getCol() + 1;

                        int firstCellCol = areaReference.getFirstCell().getCol() - deltaRow;
                        int firstCellRow = areaReference.getFirstCell().getRow();
                        CellReference firstCreatedCell = new CellReference(sheetName, firstCellRow, firstCellCol, false, false);

                        int lastCellCol = areaReference.getLastCell().getCol() - deltaRow;
                        int lastCellRow = areaReference.getLastCell().getRow();
                        CellReference lastCreatedCell = new CellReference(sheetName, lastCellRow, lastCellCol, false, false);
                        AreaReference changedReference = new AreaReference(firstCreatedCell, lastCreatedCell);
                        tmpSheetMap.put(key, changedReference);
                    }
                }
            }
        }
    }

    private void columnDelete(ReportCommand command) {
        String linkCell = command.getArguments()[0];
        String sheetName = command.getSheetName();

        AreaReference deletingReference = getAreaReference(sheetName, linkCell);
        HSSFSheet sheet = workbook.getSheet(command.getSheetName());
        if (deletingReference != null && sheet != null) {
            int destDeletingCol = deletingReference.getFirstCell().getCol();
            int sizeDeletingCol = deletingReference.getLastCell().getCol() - deletingReference.getFirstCell().getCol() + 1;

            sheetHelper.removeColumns(sheet, destDeletingCol, sizeDeletingCol);
            deleteReferenceOnTempLinkMap(sheetName, linkCell);
            imageHelper.removeCols(sheetName, destDeletingCol, sizeDeletingCol);
        }
    }

    private void cellAttributeSet(ReportCommand command) {
        String sheetName = command.getSheetName();
        String linkName0 = command.getArguments()[0];
        String linkName1 = command.getArguments()[1];
        String linkName3 = command.getArguments()[3];
        Cell linkCell = getCellByLink(sheetName, linkName0, linkName1, linkName3);
        if (linkCell instanceof HSSFCell) {
            changeCellAttribute((HSSFCell) linkCell, command);
        }
    }

    private void changeCellAttribute(HSSFCell cell, ReportCommand command) {
        String attribute = command.getArguments()[3];
        if (attribute == null) attribute = command.getArguments()[5];
        if (attribute == null) attribute = "";
        String attributeValue = command.getArguments()[4];
        if (attributeValue == null) attributeValue = command.getArguments()[6];
        if (attributeValue == null) attributeValue = "";

        HSSFColor createdColor;
        HSSFCellStyle createdStyle = workbook.createCellStyle();
        createdStyle.cloneStyleFrom(cell.getCellStyle());
        BorderStyle createdBorderStyle;
        switch (attribute.toUpperCase()) {
            case "ROWHEIGHT":
                try {
                    float rowHeight = Float.parseFloat(attributeValue);
                    cell.getRow().setHeightInPoints(rowHeight);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
                break;
            case "COLUMNWIDTH":
                try {
                    int columnIndex = cell.getColumnIndex();
                    int columnWidth = Integer.parseInt(attributeValue) * 259;
                    cell.getSheet().setColumnWidth(columnIndex, columnWidth);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
                break;
            case "BORDERS(XLEDGEBOTTOM).LINESTYLE":
                createdBorderStyle = getBorderStyle(attributeValue);
                createdStyle.setBorderBottom(createdBorderStyle);
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLEDGETOP).LINESTYLE":
                createdBorderStyle = getBorderStyle(attributeValue);
                createdStyle.setBorderTop(createdBorderStyle);
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLEDGELEFT).LINESTYLE":
                createdBorderStyle = getBorderStyle(attributeValue);
                createdStyle.setBorderLeft(createdBorderStyle);
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLEDGERIGHT).LINESTYLE":
                createdBorderStyle = getBorderStyle(attributeValue);
                createdStyle.setBorderRight(createdBorderStyle);
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLINSIDEVERTICAL).LINESTYLE":
                createdBorderStyle = getBorderStyle(attributeValue);
                createdStyle.setBorderLeft(createdBorderStyle);
                createdStyle.setBorderRight(createdBorderStyle);
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLINSIDEHORIZONTAL).LINESTYLE":
                createdBorderStyle = getBorderStyle(attributeValue);
                createdStyle.setBorderTop(createdBorderStyle);
                createdStyle.setBorderBottom(createdBorderStyle);
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLDIAGONALDOWN).LINESTYLE":
                createdBorderStyle = getBorderStyle(attributeValue);
                createdStyle = (HSSFCellStyle) cellHelper.setBottomDiagonalBorder(createdBorderStyle, cell);
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLDIAGONALUP).LINESTYLE":
                createdBorderStyle = getBorderStyle(attributeValue);
                createdStyle = (HSSFCellStyle) cellHelper.setUpDiagonalBorder(createdBorderStyle, cell);
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLEDGEBOTTOM).WEIGHT":
                createdBorderStyle = getBorderWeigh(createdStyle.getBorderBottomEnum(), attributeValue);
                createdStyle.setBorderBottom(createdBorderStyle);
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLEDGETOP).WEIGHT":
                createdBorderStyle = getBorderWeigh(createdStyle.getBorderTopEnum(), attributeValue);
                createdStyle.setBorderTop(createdBorderStyle);
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLEDGELEFT).WEIGHT":
                createdBorderStyle = getBorderWeigh(createdStyle.getBorderLeftEnum(), attributeValue);
                createdStyle.setBorderLeft(createdBorderStyle);
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLEDGERIGHT).WEIGHT":
                createdBorderStyle = getBorderWeigh(createdStyle.getBorderRightEnum(), attributeValue);
                createdStyle.setBorderRight(createdBorderStyle);
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLINSIDEVERTICAL).WEIGHT":
                createdBorderStyle = getBorderWeigh(createdStyle.getBorderLeftEnum(), attributeValue);
                createdStyle.setBorderLeft(createdBorderStyle);
                createdStyle.setBorderRight(getBorderWeigh(createdStyle.getBorderRightEnum(), attributeValue));
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLINSIDEHORIZONTAL).WEIGHT":
                createdBorderStyle = getBorderWeigh(createdStyle.getBorderTopEnum(), attributeValue);
                createdStyle.setBorderTop(createdBorderStyle);
                createdStyle.setBorderBottom(getBorderWeigh(createdStyle.getBorderBottomEnum(), attributeValue));
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLDIAGONALDOWN).WEIGHT":
                createdBorderStyle = cellHelper.getDiagonalBorderStyle(cell);
                createdStyle = (HSSFCellStyle) cellHelper.setBottomDiagonalBorder(getBorderWeigh(createdBorderStyle, attributeValue), cell);
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLDIAGONALUP).WEIGHT":
                createdBorderStyle = cellHelper.getDiagonalBorderStyle(cell);
                createdStyle = (HSSFCellStyle) cellHelper.setUpDiagonalBorder(getBorderWeigh(createdBorderStyle, attributeValue), cell);
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLEDGEBOTTOM).COLORINDEX":
            case "BORDERS(XLEDGEBOTTOM).COLOR":
                createdColor = getColorByString(attributeValue);
                createdStyle.setBottomBorderColor(createdColor.getIndex());
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLEDGETOP).COLORINDEX":
            case "BORDERS(XLEDGETOP).COLOR":
                createdColor = getColorByString(attributeValue);
                createdStyle.setTopBorderColor(createdColor.getIndex());
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLEDGELEFT).COLORINDEX":
            case "BORDERS(XLEDGELEFT).COLOR":
                createdColor = getColorByString(attributeValue);
                createdStyle.setLeftBorderColor(createdColor.getIndex());
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLEDGERIGHT).COLORINDEX":
            case "BORDERS(XLEDGERIGHT).COLOR":
                createdColor = getColorByString(attributeValue);
                createdStyle.setRightBorderColor(createdColor.getIndex());
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLINSIDEVERTICAL).COLORINDEX":
            case "BORDERS(XLINSIDEVERTICAL).COLOR":
                createdColor = getColorByString(attributeValue);
                createdStyle.setLeftBorderColor(createdColor.getIndex());
                createdStyle.setRightBorderColor(createdColor.getIndex());
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLINSIDEHORIZONTAL).COLORINDEX":
            case "BORDERS(XLINSIDEHORIZONTAL).COLOR":
                createdColor = getColorByString(attributeValue);
                createdStyle.setBottomBorderColor(createdColor.getIndex());
                createdStyle.setTopBorderColor(createdColor.getIndex());
                cell.setCellStyle(createdStyle);
                break;
            case "BORDERS(XLDIAGONALUP).COLORINDEX":
            case "BORDERS(XLDIAGONALUP).COLOR":
            case "BORDERS(XLDIAGONALDOWN).COLORINDEX":
            case "BORDERS(XLDIAGONALDOWN).COLOR":
                createdColor = getColorByString(attributeValue);
                createdStyle = (HSSFCellStyle) cellHelper.setDiagonalColor(cell, createdColor);
                cell.setCellStyle(createdStyle);
                break;
            case "HORIZONTALALIGNMENT":
                switch (attributeValue.toUpperCase()) {
                    case "XLHALIGNCENTER":
                    case "CENTER":
                        createdStyle.setAlignment(HorizontalAlignment.CENTER);
                        cell.setCellStyle(createdStyle);
                        break;
                    case "XLHALIGNRIGHT":
                    case "RIGHT":
                        createdStyle.setAlignment(HorizontalAlignment.RIGHT);
                        cell.setCellStyle(createdStyle);
                        break;
                    case "XLHALIGNCENTERACROSSSELECTION":
                    case "CENTERACROSSSELECTION":
                        createdStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
                        cell.setCellStyle(createdStyle);
                        break;
                    case "XLHALIGNDISTRIBUTED":
                    case "DISTRIBUTED":
                        createdStyle.setAlignment(HorizontalAlignment.DISTRIBUTED);
                        createdStyle.setIndention((short) 0);
                        cell.setCellStyle(createdStyle);
                        break;
                    case "XLHALIGNGENERAL":
                    case "GENERAL":
                        createdStyle.setAlignment(HorizontalAlignment.GENERAL);
                        cell.setCellStyle(createdStyle);
                        break;
                    case "XLHALIGNJUSTIFY":
                    case "JUSTIFY":
                        createdStyle.setAlignment(HorizontalAlignment.JUSTIFY);
                        cell.setCellStyle(createdStyle);
                        break;
                    case "XLHALIGNFILL":
                    case "FILL":
                        createdStyle.setAlignment(HorizontalAlignment.FILL);
                        cell.setCellStyle(createdStyle);
                        break;
                    default:
                        createdStyle.setAlignment(HorizontalAlignment.LEFT);
                        cell.setCellStyle(createdStyle);
                        break;
                }
                break;
            case "VERTICALALIGNMENT":
                switch (attributeValue.toUpperCase()) {
                    case "XLVALIGNCENTER":
                    case "CENTER":
                        createdStyle.setVerticalAlignment(VerticalAlignment.CENTER);
                        cell.setCellStyle(createdStyle);
                        break;
                    case "XLVALIGNTOP":
                    case "TOP":
                        createdStyle.setVerticalAlignment(VerticalAlignment.TOP);
                        cell.setCellStyle(createdStyle);
                        break;
                    case "XLVALIGNDISTRIBUTED":
                    case "DISTRIBUTED":
                        createdStyle.setVerticalAlignment(VerticalAlignment.DISTRIBUTED);
                        cell.setCellStyle(createdStyle);
                        break;
                    case "XLVALIGNJUSTIFY":
                    case "JUSTIFY":
                        createdStyle.setVerticalAlignment(VerticalAlignment.JUSTIFY);
                        cell.setCellStyle(createdStyle);
                        break;
                    default:
                        createdStyle.setVerticalAlignment(VerticalAlignment.BOTTOM);
                        cell.setCellStyle(createdStyle);
                        break;
                }
                break;
            case "WRAPTEXT":
                if (attributeValue.equalsIgnoreCase("true")) createdStyle.setWrapText(true);
                else createdStyle.setWrapText(false);
                cell.setCellStyle(createdStyle);
                break;
            case "INDENTLEVEL":
                if (!createdStyle.getAlignmentEnum().equals(HorizontalAlignment.LEFT) &&
                        !createdStyle.getAlignmentEnum().equals(HorizontalAlignment.RIGHT) &&
                        !createdStyle.getAlignmentEnum().equals(HorizontalAlignment.DISTRIBUTED)) {
                    createdStyle.setAlignment(HorizontalAlignment.LEFT);
                }
                short indent = 0;
                try {
                    indent = Short.parseShort(attributeValue);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                createdStyle.setIndention(indent);
                cell.setCellStyle(createdStyle);
                break;
            case "FONT.NAME":
                //Создаем новый шрифт и копируем в него параметры старого шрифта
                HSSFFont fontName = workbook.createFont();
                fontName.setColor(createdStyle.getFont(workbook).getColor());
                fontName.setCharSet(createdStyle.getFont(workbook).getCharSet());
//                fontName.setFamily(createdStyle.getFont().getFamily());  Почему из за этого аттрибута не открывает файл отчет
                fontName.setFontHeight(createdStyle.getFont(workbook).getFontHeight());
//                fontName.setScheme(createdStyle.getFont().getScheme());
                fontName.setBold(createdStyle.getFont(workbook).getBold());
                fontName.setItalic(createdStyle.getFont(workbook).getItalic());
                fontName.setStrikeout(createdStyle.getFont(workbook).getStrikeout());
                fontName.setUnderline(createdStyle.getFont(workbook).getUnderline());
                fontName.setTypeOffset(createdStyle.getFont(workbook).getTypeOffset());

                fontName.setFontName(attributeValue);
                createdStyle.setFont(fontName);
                cell.setCellStyle(createdStyle);
                break;
            case "FONT.SIZE":
                //Создаем новый шрифт и копируем в него параметры старого шрифта
                HSSFFont fontSize = workbook.createFont();
                fontSize.setColor(createdStyle.getFont(workbook).getColor());
                fontSize.setCharSet(createdStyle.getFont(workbook).getCharSet());
//                fontSize.setFamily(createdStyle.getFont().getFamily());  Почему из за этого аттрибута не открывает файл отчет
//                fontSize.setScheme(createdStyle.getFont().getScheme());
                fontSize.setFontName(createdStyle.getFont(workbook).getFontName());
                fontSize.setBold(createdStyle.getFont(workbook).getBold());
                fontSize.setItalic(createdStyle.getFont(workbook).getItalic());
                fontSize.setStrikeout(createdStyle.getFont(workbook).getStrikeout());
                fontSize.setUnderline(createdStyle.getFont(workbook).getUnderline());
                fontSize.setTypeOffset(createdStyle.getFont(workbook).getTypeOffset());
                short fontHeight = createdStyle.getFont(workbook).getFontHeight();
                try {
                    double fontSizeTmp = Double.parseDouble(attributeValue) * 20;
                    if (fontSizeTmp > Short.MAX_VALUE) fontSizeTmp = Short.MAX_VALUE;
                    fontHeight = (short) fontSizeTmp;
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                fontSize.setFontHeight(fontHeight);
                createdStyle.setFont(fontSize);
                cell.setCellStyle(createdStyle);
                break;
            case "FONT.FONTSTYLE":
                //Создаем новый шрифт и копируем в него параметры старого шрифта
                HSSFFont font = workbook.createFont();
                font.setColor(createdStyle.getFont(workbook).getColor());
                font.setCharSet(createdStyle.getFont(workbook).getCharSet());
//                font.setFamily(createdStyle.getFont().getFamily());  Почему из за этого аттрибута не открывает файл отчет
                font.setFontHeight(createdStyle.getFont(workbook).getFontHeight());
//                font.setScheme(createdStyle.getFont(workbook).getScheme());
                font.setFontName(createdStyle.getFont(workbook).getFontName());
                font.setBold(createdStyle.getFont(workbook).getBold());
                font.setItalic(createdStyle.getFont(workbook).getItalic());
                font.setStrikeout(createdStyle.getFont(workbook).getStrikeout());
                font.setUnderline(createdStyle.getFont(workbook).getUnderline());
                font.setTypeOffset(createdStyle.getFont(workbook).getTypeOffset());
                switch (attributeValue.toUpperCase()) {
                    case "BOLD":
                        font.setItalic(false);
                        font.setBold(true);
                        break;
                    case "ITALIC":
                        font.setItalic(true);
                        font.setBold(false);
                        break;
                    case "BOLD ITALIC":
                    case "ITALIC BOLD":
                        font.setItalic(true);
                        font.setBold(true);
                        break;
                    case "REGULAR":
                    default:
                        font.setItalic(false);
                        font.setBold(false);
                        break;
                }
                createdStyle.setFont(font);
                cell.setCellStyle(createdStyle);
                break;
            case "FONT.UNDERLINE":
                //Создаем новый шрифт и копируем в него параметры старого шрифта
                HSSFFont underlineFont = workbook.createFont();
                underlineFont.setColor(createdStyle.getFont(workbook).getColor());
                underlineFont.setCharSet(createdStyle.getFont(workbook).getCharSet());
//                underlineFont.setFamily(createdStyle.getFont().getFamily());  Почему из за этого аттрибута не открывает файл отчет
                underlineFont.setFontHeight(createdStyle.getFont(workbook).getFontHeight());
//                underlineFont.setScheme(createdStyle.getFont().getScheme());
                underlineFont.setFontName(createdStyle.getFont(workbook).getFontName());
                underlineFont.setBold(createdStyle.getFont(workbook).getBold());
                underlineFont.setItalic(createdStyle.getFont(workbook).getItalic());
                underlineFont.setStrikeout(createdStyle.getFont(workbook).getStrikeout());
                underlineFont.setTypeOffset(createdStyle.getFont(workbook).getTypeOffset());

                switch (attributeValue.toUpperCase()) {
                    case "XLUNDERLINESTYLEDOUBLE":
                    case "DOUBLE":
                        underlineFont.setUnderline(FontUnderline.DOUBLE.getByteValue());
                        break;
                    case "XLUNDERLINESTYLEDOUBLEACCOUNTING":
                    case "DOUBLEACCOUNTING":
                        underlineFont.setUnderline(FontUnderline.DOUBLE_ACCOUNTING.getByteValue());
                        break;
                    case "XLUNDERLINESTYLESINGLE":
                    case "SINGLE":
                        underlineFont.setUnderline(FontUnderline.SINGLE.getByteValue());
                        break;
                    case "XLUNDERLINESTYLESINGLEACCOUNTING":
                    case "SINGLEACCOUNTING":
                        underlineFont.setUnderline(FontUnderline.SINGLE_ACCOUNTING.getByteValue());
                        break;
                    default:
                        underlineFont.setUnderline(FontUnderline.NONE.getByteValue());
                        break;
                }
                createdStyle.setFont(underlineFont);
                cell.setCellStyle(createdStyle);
                break;
            case "FONT.COLORINDEX":
            case "FONT.COLOR":
                //Создаем новый шрифт и копируем в него параметры старого шрифта
                HSSFFont fontColor = workbook.createFont();
                fontColor.setCharSet(createdStyle.getFont(workbook).getCharSet());
//                fontColor.setFamily(createdStyle.getFont().getFamily());  Почему из за этого аттрибута не открывает файл отчет
                fontColor.setFontHeight(createdStyle.getFont(workbook).getFontHeight());
//                fontColor.setScheme(createdStyle.getFont().getScheme());
                fontColor.setFontName(createdStyle.getFont(workbook).getFontName());
                fontColor.setBold(createdStyle.getFont(workbook).getBold());
                fontColor.setItalic(createdStyle.getFont(workbook).getItalic());
                fontColor.setStrikeout(createdStyle.getFont(workbook).getStrikeout());
                fontColor.setUnderline(createdStyle.getFont(workbook).getUnderline());
                fontColor.setTypeOffset(createdStyle.getFont(workbook).getTypeOffset());
                createdColor = getColorByString(attributeValue);
                fontColor.setColor(createdColor.getIndex());
                createdStyle.setFont(fontColor);
                cell.setCellStyle(createdStyle);
                break;
            case "INTERIOR.COLORINDEX":
            case "INTERIOR.COLOR":
                createdColor = getColorByString(attributeValue);
                createdStyle.setFillForegroundColor(createdColor.getIndex());
                createdStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                cell.setCellStyle(createdStyle);
                break;
        }
    }

    private BorderStyle getBorderStyle(String attributeValue) {
        switch (attributeValue.toUpperCase()) {
            case "XLCONTINUOUS":
                return BorderStyle.THIN;
            case "XLDASH":
                return BorderStyle.DASHED;
            case "XLDASHDOT":
                return BorderStyle.DASH_DOT;
            case "XLDASHDOTDOT":
                return BorderStyle.DASH_DOT_DOT;
            case "XLDOUBLE":
                return BorderStyle.DOUBLE;
            case "XLSLANTDASHDOT":
                return BorderStyle.SLANTED_DASH_DOT;
            case "XLDOT":
                return BorderStyle.DOTTED;
            default:
                return BorderStyle.NONE;
        }
    }

    private BorderStyle getBorderWeigh(BorderStyle oldBorderStyle, String weight) {
        if (weight.trim().equals("1")) weight = "XLHAIRLINE";
        if (weight.trim().equals("2")) weight = "XLTHIN";
        if (weight.trim().equals("3")) weight = "XLMEDIUM";
        if (weight.trim().equals("4")) weight = "XLTHICK";
        switch (weight.trim().toUpperCase()) {
            case "XLHAIRLINE":
                return BorderStyle.HAIR;
            case "XLTHIN":
                switch (oldBorderStyle) {
                    case DASHED:
                    case MEDIUM_DASHED:
                        return BorderStyle.DASHED;
                    case DASH_DOT:
                    case MEDIUM_DASH_DOT:
                        return BorderStyle.DASH_DOT;
                    case DASH_DOT_DOT:
                    case MEDIUM_DASH_DOT_DOT:
                        return BorderStyle.DASH_DOT_DOT;
                    case DOTTED:
                        return BorderStyle.DOTTED;
                    default:
                        return BorderStyle.THIN;
                }
            case "XLMEDIUM":
                switch (oldBorderStyle) {
                    case DASHED:
                        return BorderStyle.MEDIUM_DASHED;
                    case DASH_DOT:
                        return BorderStyle.MEDIUM_DASH_DOT;
                    case DASH_DOT_DOT:
                        return BorderStyle.MEDIUM_DASH_DOT_DOT;
                    case SLANTED_DASH_DOT:
                        return BorderStyle.SLANTED_DASH_DOT;
                    default:
                        return BorderStyle.MEDIUM;
                }
            case "XLTHICK":
                switch (oldBorderStyle) {
                    case DOUBLE:
                        return BorderStyle.DOUBLE;
                    default:
                        return BorderStyle.THICK;
                }
        }
        return oldBorderStyle;
    }

    private HSSFColor getHSSFColorByString(String attributeValue) {
        HSSFColor result = new HSSFColor();
        if (attributeValue.contains(",")) {
            String[] array = attributeValue.split(",");
            if (array.length == 3) {
                try {
                    byte red = (byte) Integer.parseInt(array[0].trim());
                    byte green = (byte) Integer.parseInt(array[1].trim());
                    byte blue = (byte) Integer.parseInt(array[2].trim());
                    result = getCustomColor(lastColorIndex++, red, green, blue);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    private HSSFColor getCustomColor(short colorIndex, byte red, byte green, byte blue) {
        HSSFPalette palette = workbook.getCustomPalette();
        HSSFColor color = null;
        try {
            color = palette.findColor(red, green, blue);
            if (color == null) {
                palette.setColorAtIndex(colorIndex, red, green, blue);
                color = palette.getColor(colorIndex);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return color;
    }

    private HSSFColor getColorByString(String value) {
        HSSFColor result = new HSSFColor();
        if (value.contains(",")) {
            result = getHSSFColorByString(value);
        } else {
            try {
                short colorIndex = Short.parseShort(value);
                switch (colorIndex) {
                    case 1:
                        result = getCustomColor(colorIndex, (byte) 0 ,(byte) 0 ,(byte) 0);
                        break;
                    case 2:
                        result = getCustomColor(colorIndex, (byte) 255 ,(byte) 255 ,(byte) 255);
                        break;
                    case 3:
                        result = getCustomColor(colorIndex, (byte) 255 ,(byte) 0 ,(byte) 0);
                        break;
                    case 4:
                        result = getCustomColor(colorIndex, (byte) 0 ,(byte) 255 ,(byte) 0);
                        break;
                    case 5:
                        result = getCustomColor(colorIndex, (byte) 0 ,(byte) 0 ,(byte) 255);
                        break;
                    case 6:
                        result = getCustomColor(colorIndex, (byte) 255 ,(byte) 255 ,(byte) 0);
                        break;
                    case 7:
                        result = getCustomColor(colorIndex, (byte) 255 ,(byte) 0 ,(byte) 255);
                        break;
                    case 8:
                        result = getCustomColor(colorIndex, (byte) 0 ,(byte) 255 ,(byte) 255);
                        break;
                    case 9:
                        result = getCustomColor(colorIndex, (byte) 128 ,(byte) 0 ,(byte) 0);
                        break;
                    case 10:
                        result = getCustomColor(colorIndex, (byte) 0 ,(byte) 128 ,(byte) 0);
                        break;
                    case 11:
                        result = getCustomColor(colorIndex, (byte) 0 ,(byte) 0 ,(byte) 128);
                        break;
                    case 12:
                        result = getCustomColor(colorIndex, (byte) 128 ,(byte) 128 ,(byte) 0);
                        break;
                    case 13:
                        result = getCustomColor(colorIndex, (byte) 128 ,(byte) 0 ,(byte) 128);
                        break;
                    case 14:
                        result = getCustomColor(colorIndex, (byte) 0 ,(byte) 128 ,(byte) 128);
                        break;
                    case 15:
                        result = getCustomColor(colorIndex, (byte) 192 ,(byte) 192 ,(byte) 192);
                        break;
                    case 16:
                        result = getCustomColor(colorIndex, (byte) 128 ,(byte) 128 ,(byte) 128);
                        break;
                    case 17:
                        result = getCustomColor(colorIndex, (byte) 153 ,(byte) 153 ,(byte) 255);
                        break;
                    case 18:
                        result = getCustomColor(colorIndex, (byte) 153 ,(byte) 51 ,(byte) 102);
                        break;
                    case 19:
                        result = getCustomColor(colorIndex, (byte) 255 ,(byte) 255 ,(byte) 204);
                        break;
                    case 20:
                        result = getCustomColor(colorIndex, (byte) 204 ,(byte) 255 ,(byte) 255);
                        break;
                    case 21:
                        result = getCustomColor(colorIndex, (byte) 102 ,(byte) 0 ,(byte) 102);
                        break;
                    case 22:
                        result = getCustomColor(colorIndex, (byte) 255 ,(byte) 128 ,(byte) 128);
                        break;
                    case 23:
                        result = getCustomColor(colorIndex, (byte) 0 ,(byte) 102 ,(byte) 204);
                        break;
                    case 24:
                        result = getCustomColor(colorIndex, (byte) 204 ,(byte) 204 ,(byte) 255);
                        break;
                    case 25:
                        result = getCustomColor(colorIndex, (byte) 0 ,(byte) 0 ,(byte) 128);
                        break;
                    case 26:
                        result = getCustomColor(colorIndex, (byte) 255 ,(byte) 0 ,(byte) 255);
                        break;
                    case 27:
                        result = getCustomColor(colorIndex, (byte) 255 ,(byte) 255 ,(byte) 0);
                        break;
                    case 28:
                        result = getCustomColor(colorIndex, (byte) 0 ,(byte) 255 ,(byte) 255);
                        break;
                    case 29:
                        result = getCustomColor(colorIndex, (byte) 128 ,(byte) 0 ,(byte) 128);
                        break;
                    case 30:
                        result = getCustomColor(colorIndex, (byte) 128 ,(byte) 0 ,(byte) 0);
                        break;
                    case 31:
                        result = getCustomColor(colorIndex, (byte) 0 ,(byte) 128 ,(byte) 128);
                        break;
                    case 32:
                        result = getCustomColor(colorIndex, (byte) 0 ,(byte) 0 ,(byte) 255);
                        break;
                    case 33:
                        result = getCustomColor(colorIndex, (byte) 0 ,(byte) 204 ,(byte) 255);
                        break;
                    case 34:
                        result = getCustomColor(colorIndex, (byte) 204 ,(byte) 255 ,(byte) 255);
                        break;
                    case 35:
                        result = getCustomColor(colorIndex, (byte) 204 ,(byte) 255 ,(byte) 204);
                        break;
                    case 36:
                        result = getCustomColor(colorIndex, (byte) 255 ,(byte) 255 ,(byte) 153);
                        break;
                    case 37:
                        result = getCustomColor(colorIndex, (byte) 153 ,(byte) 204 ,(byte) 255);
                        break;
                    case 38:
                        result = getCustomColor(colorIndex, (byte) 255 ,(byte) 153 ,(byte) 204);
                        break;
                    case 39:
                        result = getCustomColor(colorIndex, (byte) 204 ,(byte) 153 ,(byte) 255);
                        break;
                    case 40:
                        result = getCustomColor(colorIndex, (byte) 255 ,(byte) 204 ,(byte) 153);
                        break;
                    case 41:
                        result = getCustomColor(colorIndex, (byte) 51 ,(byte) 102 ,(byte) 255);
                        break;
                    case 42:
                        result = getCustomColor(colorIndex, (byte) 51 ,(byte) 204 ,(byte) 204);
                        break;
                    case 43:
                        result = getCustomColor(colorIndex, (byte) 153 ,(byte) 204 ,(byte) 0);
                        break;
                    case 44:
                        result = getCustomColor(colorIndex, (byte) 255 ,(byte) 204 ,(byte) 0);
                        break;
                    case 45:
                        result = getCustomColor(colorIndex, (byte) 255 ,(byte) 153 ,(byte) 0);
                        break;
                    case 46:
                        result = getCustomColor(colorIndex, (byte) 255 ,(byte) 102 ,(byte) 0);
                        break;
                    case 47:
                        result = getCustomColor(colorIndex, (byte) 102 ,(byte) 102 ,(byte) 153);
                        break;
                    case 48:
                        result = getCustomColor(colorIndex, (byte) 150 ,(byte) 150 ,(byte) 150);
                        break;
                    case 49:
                        result = getCustomColor(colorIndex, (byte) 0 ,(byte) 51 ,(byte) 102);
                        break;
                    case 50:
                        result = getCustomColor(colorIndex, (byte) 51 ,(byte) 153 ,(byte) 102);
                        break;
                    case 51:
                        result = getCustomColor(colorIndex, (byte) 0 ,(byte) 51 ,(byte) 0);
                        break;
                    case 52:
                        result = getCustomColor(colorIndex, (byte) 51 ,(byte) 51 ,(byte) 0);
                        break;
                    case 53:
                        result = getCustomColor(colorIndex, (byte) 153 ,(byte) 51 ,(byte) 0);
                        break;
                    case 54:
                        result = getCustomColor(colorIndex, (byte) 153 ,(byte) 51 ,(byte) 102);
                        break;
                    case 55:
                        result = getCustomColor(colorIndex, (byte) 51 ,(byte) 51 ,(byte) 153);
                        break;
                    case 56:
                        result = getCustomColor(colorIndex, (byte) 51 ,(byte) 51 ,(byte) 51);
                        break;
                }
            } catch (IllegalArgumentException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

    private void lineCellAttributeSet(ReportCommand command) {
        String sheetName = command.getSheetName();
        String linkNameRow = command.getArguments()[0];
        String linkName = command.getArguments()[1];
        String parentLineLink = command.getArguments()[2];
        Cell linkCell = getCellByLinkLine(sheetName, linkName, linkNameRow, parentLineLink);
        if (linkCell instanceof HSSFCell) {
            changeCellAttribute((HSSFCell) linkCell, command);
        }
    }

    private void columnCellAttributeSet(ReportCommand command) {
        String sheetName = command.getSheetName();
        String linkNameColumn = command.getArguments()[0];
        String linkName = command.getArguments()[1];
        String parentColumnLink = command.getArguments()[2];
        HSSFCell linkCell = getCellByLinkColumn(sheetName, linkName, linkNameColumn, parentColumnLink);
        if (linkCell instanceof HSSFCell) {
            changeCellAttribute((HSSFCell) linkCell, command);
        }
    }

    private void sheetCopy(ReportCommand command) {
        int sheetIndex = workbook.getSheetIndex(command.getSheetName());
        if (sheetIndex >= 0) {
            HSSFSheet copiedSheet = workbook.getSheetAt(sheetIndex);
            String newSheetName = command.getArguments()[0];
            HSSFSheet createdSheet = workbook.cloneSheet(sheetIndex);
            workbook.setSheetName(workbook.getSheetIndex(createdSheet), newSheetName);

            //Проверяем что лист видимый
            if (!workbook.getSheetVisibility(sheetIndex).equals(SheetVisibility.VISIBLE))
                workbook.setSheetVisibility(workbook.getSheetIndex(createdSheet), workbook.getSheetVisibility(sheetIndex));

            //Коприуем параметры печати из старого листа
            createdSheet.getPrintSetup().setPaperSize(copiedSheet.getPrintSetup().getPaperSize());
            createdSheet.getPrintSetup().setValidSettings(copiedSheet.getPrintSetup().getValidSettings());
            createdSheet.getPrintSetup().setCopies(copiedSheet.getPrintSetup().getCopies());
            createdSheet.getPrintSetup().setDraft(copiedSheet.getPrintSetup().getDraft());
            createdSheet.getPrintSetup().setFitHeight(copiedSheet.getPrintSetup().getFitHeight());
            createdSheet.getPrintSetup().setFitWidth(copiedSheet.getPrintSetup().getFitWidth());
            createdSheet.getPrintSetup().setFooterMargin(copiedSheet.getPrintSetup().getFooterMargin());
            createdSheet.getPrintSetup().setHeaderMargin(copiedSheet.getPrintSetup().getHeaderMargin());
            createdSheet.getPrintSetup().setHResolution(copiedSheet.getPrintSetup().getHResolution());
            createdSheet.getPrintSetup().setLandscape(copiedSheet.getPrintSetup().getLandscape());
            createdSheet.getPrintSetup().setLeftToRight(copiedSheet.getPrintSetup().getLeftToRight());
            createdSheet.getPrintSetup().setNoColor(copiedSheet.getPrintSetup().getNoColor());
            createdSheet.getPrintSetup().setNoOrientation(copiedSheet.getPrintSetup().getNoOrientation());
            createdSheet.getPrintSetup().setNotes(copiedSheet.getPrintSetup().getNotes());
            createdSheet.getPrintSetup().setPageStart(copiedSheet.getPrintSetup().getPageStart());
            createdSheet.getPrintSetup().setScale(copiedSheet.getPrintSetup().getScale());
            createdSheet.getPrintSetup().setUsePage(copiedSheet.getPrintSetup().getUsePage());
            createdSheet.getPrintSetup().setVResolution(copiedSheet.getPrintSetup().getVResolution());

            createdSheet.getPrintSetup().setOptions(copiedSheet.getPrintSetup().getOptions());

            Map<String, AreaReference> tmpSheetLinkMap = new HashMap<>();
            for (Name name : workbook.getAllNames()) {
                if (!name.getRefersToFormula().contains("#REF") && name.getSheetName().equalsIgnoreCase(command.getSheetName())) {
                    String key = name.getNameName().toUpperCase();
                    AreaReference reference = new AreaReference(name.getRefersToFormula(), SpreadsheetVersion.EXCEL2007);
                    tmpSheetLinkMap.put(key, reference);
                }
            }
            //Заполняем ссылки по адресу
            for (ReportCommand c : commands) {
                if (c.getSheetName() != null && c.getSheetName().equalsIgnoreCase(newSheetName)) {
                    String address = c.getArguments()[2];
                    if (address != null &&
                            !address.trim().isEmpty() && (
                            Pattern.compile(CellHelper.PATTERN_LINE_ADDRESS).matcher(address).find() ||
                                    Pattern.compile(CellHelper.PATTERN_COLUMN_ADDRESS).matcher(address).find())) {
                        tmpSheetLinkMap.put(address, new AreaReference(address, SpreadsheetVersion.EXCEL2007));
                    }
                }
            }
            tempLinkMap.put(newSheetName.toUpperCase(), tmpSheetLinkMap);
            sheetHelper.copySheet(command.getSheetName(), newSheetName);
        }
    }

    private void sheetDelete(ReportCommand command) {
        int sheetIndex = workbook.getSheetIndex(command.getSheetName());
        if (sheetIndex >= 0) {
            workbook.removeSheetAt(sheetIndex);
            String sheetName = command.getSheetName().toUpperCase();
            tempLinkMap.remove(sheetName);
            sheetHelper.deleteSheet(command.getSheetName());
        }
        //При удалении активным делаем только видимый лист, иначе невидимый лист станет видимым
        if (!workbook.getSheetVisibility(workbook.getActiveSheetIndex()).equals(SheetVisibility.VISIBLE)) {
            int activeSheetIndex = workbook.getActiveSheetIndex();
            int numberSheets = workbook.getNumberOfSheets();
            int count = 0;
            SheetVisibility sheetVisibility = workbook.getSheetVisibility(activeSheetIndex);
            while (!sheetVisibility.equals(SheetVisibility.VISIBLE) && count < numberSheets) {
                activeSheetIndex++;
                count++;
                if (activeSheetIndex >= numberSheets) activeSheetIndex -= numberSheets;
                sheetVisibility = workbook.getSheetVisibility(activeSheetIndex);
            }
            workbook.setActiveSheet(activeSheetIndex);
        }
    }

    private void sheetPageSetup(ReportCommand command) {
        String param = command.getArguments()[0];
        switch (param.toUpperCase()) {
            case "VISIBLE":
                String value = command.getArguments()[1];
                int sheetIndex = workbook.getSheetIndex(command.getSheetName());
                if (sheetIndex >= 0) {
                    switch (value.toUpperCase()) {
                        case "HIDDEN":
                        case "VERY_HIDDEN":
                        case "VERYHIDDEN":
                            if (value.toUpperCase().contains("VERY")) {
                                workbook.setSheetVisibility(sheetIndex, SheetVisibility.VERY_HIDDEN);
                            } else {
                                workbook.setSheetVisibility(sheetIndex, SheetVisibility.HIDDEN);
                            }
                            //Нельзя что бы скрываемый лист был активным
                            if (workbook.getActiveSheetIndex() == sheetIndex) {
                                int activeSheetIndex = workbook.getActiveSheetIndex();
                                int numberSheets = workbook.getNumberOfSheets();
                                int count = 0;
                                SheetVisibility sheetVisibility = workbook.getSheetVisibility(activeSheetIndex);
                                while (!sheetVisibility.equals(SheetVisibility.VISIBLE) && count < numberSheets) {
                                    activeSheetIndex++;
                                    count++;
                                    if (activeSheetIndex >= numberSheets) activeSheetIndex -= numberSheets;
                                    sheetVisibility = workbook.getSheetVisibility(activeSheetIndex);
                                }
                                workbook.setActiveSheet(activeSheetIndex);
                            }
                            break;
                        case "VISIBLE":
                            workbook.setSheetVisibility(sheetIndex, SheetVisibility.VISIBLE);
                            break;
                    }
                }
                break;
        }
    }

    private void bookSetup(ReportCommand command) {
        String param = command.getArguments()[0];
        switch (param.toUpperCase()) {
            case "FILENAME":
                if (!TextUtils.isEmpty(command.getArguments()[1])) fileName = command.getArguments()[1];
                break;
        }
    }

    private String getFileName() {
        return fileName + fileType;
    }

    private HSSFWorkbook readWorkbook(String file) {
        try (InputStream inputStream = new FileInputStream(file)) {
            return new HSSFWorkbook(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new NullPointerException("Ошибка чтения файла " + file);
    }

    public byte[] getByteArray(Workbook workbook) {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();) {
            workbook.write(bos);
            byte[] bytes = bos.toByteArray();
            return bytes;
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new NullPointerException("Ошибка записи книги в byte code");
    }

    public void saveWorkbook(String fileName) {
        try (FileOutputStream outputStream = new FileOutputStream(fileName)) {
            workbook.write(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
