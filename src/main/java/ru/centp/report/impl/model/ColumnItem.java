package ru.centp.report.impl.model;

import java.util.Objects;

public class ColumnItem {
    private String caption;
    private final String id;
    private final String table;
    private ColumnType type;
    private boolean isVisible;

    public ColumnItem(String id, String table) {
        this(id, table, false);
    }

    public ColumnItem(String id, String table, boolean isVisible) {
        this.id = id;
        this.table = table;
        this.isVisible = isVisible;
        this.type = ColumnType.STRING;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getId() {
        return id;
    }

    public ColumnType getType() {
        return type;
    }

    public void setType(ColumnType type) {
        this.type = type;
    }

    public String getTable() {
        return table;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    @Override
    public String toString() {
        return "ColumnItem{" +
                "caption='" + caption + '\'' +
                ", id='" + id + '\'' +
                ", table='" + table + '\'' +
                ", type=" + type +
                ", isVisible=" + isVisible +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return this.hashCode() == o.hashCode();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, table);
    }
}
