package ru.centp.report.impl.model;

import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.ss.util.CellReference;

public class FormulaElement {

    private String sheetName = null;
    private StringBuilder template;
    private static final String sheetNameDummy = "%{_sheet_name}";
    private static final String rowDummy = "%{_row}";
    private static final String columnDummy = "%{_column}";
    private int row;
    private int column;
    private boolean anchorRow = false;
    private boolean anchorColumn = false;

    public FormulaElement(String element) {
        template = new StringBuilder(element);
        String cellAddress;
        if (element.contains("!")) {
            sheetName = element.substring(0, element.indexOf("!"));
            if (sheetName.startsWith("'")) sheetName = sheetName.substring(1);
            if (sheetName.endsWith("'")) sheetName = sheetName.substring(0, sheetName.length() - 1);
            cellAddress = element.substring(element.indexOf("!") + 1);
        } else cellAddress = element;
        CellAddress address = new CellAddress(cellAddress.replace("$", ""));
        row = address.getRow();
        column = address.getColumn();
        String columnName = CellReference.convertNumToColString(column);

        if (element.contains("$" + (row + 1))) anchorRow = true;
        if (element.contains("$" + columnName)) anchorColumn = true;

        String tmp;
        int startIndex;
        int endIndex;

        if (sheetName != null) {
            startIndex = template.indexOf(sheetName);
            endIndex = startIndex + sheetName.length();
            template.replace(startIndex, endIndex, sheetNameDummy);
        }

        if (!anchorRow) {
            tmp = Integer.toString(row + 1);
            startIndex = template.indexOf(tmp);
            endIndex = startIndex + tmp.length();
            template.replace(startIndex, endIndex, rowDummy);
        }

        if (!anchorColumn) {
            tmp = columnName;
            startIndex = template.indexOf(tmp);
            endIndex = startIndex + tmp.length();
            template.replace(startIndex, endIndex, columnDummy);
        }
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public String getCellAddress() {
        return CellReference.convertNumToColString(this.column) + (this.row + 1);
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public void setRow(int row) {
        if (!anchorRow) this.row = row;
    }

    public void setColumn(int column) {
        if (!anchorColumn) this.column = column;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder(template);
        int startIndex;
        int endIndex;

        startIndex = result.indexOf(rowDummy);
        if (startIndex >= 0) {
            endIndex = startIndex + rowDummy.length();
            result.replace(startIndex, endIndex, Integer.toString(row + 1));
        }

        startIndex = result.indexOf(columnDummy);
        if (startIndex >= 0) {
            endIndex = startIndex + columnDummy.length();
            result.replace(startIndex, endIndex, CellReference.convertNumToColString(column));
        }

        if (sheetName != null) {
            startIndex = result.indexOf(sheetNameDummy);
            endIndex = startIndex + sheetNameDummy.length();
            if (startIndex >= 0) result.replace(startIndex, endIndex, sheetName);
        }
        return result.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (!(o instanceof CellAddress)) {
            return false;
        } else {
            CellAddress other = (CellAddress)o;
            return this.toString().equals(other.toString());
        }
    }

    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }
}
