package ru.centp.report.impl.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ru.centp.report.impl.mapper.ItemSetSerializer;

import java.util.*;
import java.util.logging.Logger;

@JsonSerialize(using = ItemSetSerializer.class)
public class ItemProvider {

    //private final Logger logger = Logger.getLogger("43656");
    //Для заголовков
    private final Map<Integer, String> tableMap;
    private final Map<Integer, String> headerMap;

    //Список всех таблиц
    private final Set<String> tableList;
    private int columnIndex = 0;
    private int lastPosition = 99;
    private Map<String, ItemContainer> tmpColumnMap;

    private final Set<ItemContainer> resultSet;
    private final Map<String, List<ItemContainer>> mapForSerialize;
    private String databaseName = "vegaplus";

    public ItemProvider() {
        this.tableMap = new HashMap<>();
        this.headerMap = new HashMap<>();
        this.resultSet = new LinkedHashSet<>();
        this.tableList = new LinkedHashSet<>();
        mapForSerialize = new LinkedHashMap<>();
    }

    public void setDatabaseName(String databaseName) {
        if (databaseName != null) {
            this.databaseName = databaseName;
        }
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void addHeader(String header) {
        String[] tmpArray = header.split("\\$", 2);
        String tableName = tmpArray[0];
        String headerName = tmpArray[1];

        tableMap.put(columnIndex, tableName);
        headerMap.put(columnIndex, headerName);
        tableList.add(tableName);
        columnIndex++;
    }

    public void addItem(String value, int position) {
        if (position < lastPosition) {
            if (tmpColumnMap != null) addItemsToResult();
            tmpColumnMap = new LinkedHashMap<>();
            for (String tableName : tableList) {
                tmpColumnMap.put(tableName, new ItemContainer(tableName));
            }
        }
        String tableName = tableMap.get(position);
        String columnName = headerMap.get(position);

        if (tmpColumnMap.get(tableName) != null)
            tmpColumnMap.get(tableName).addParam(columnName, value);

        lastPosition = position;
    }

    public Set<ItemContainer> getResultSet() {
        addItemsToResult();
        return resultSet;
    }

    private void addItemsToResult() {
        ItemContainer lastContainer = null;
        for (Map.Entry<String, ItemContainer> entry : tmpColumnMap.entrySet()) {
            ItemContainer currentContainer = getContainerFromResultSet(entry.getValue(), resultSet);
            if (lastContainer == null) {
                resultSet.add(currentContainer);
            } else {
                lastContainer.addChildren(currentContainer);
            }
            lastContainer = currentContainer;
        }
    }

    private ItemContainer getContainerFromResultSet(ItemContainer targetContainer, Set<ItemContainer> containerSet) {
        ItemContainer result = targetContainer;
        Iterator<ItemContainer> containerIterator = containerSet.iterator();
        while (containerIterator.hasNext()) {
            ItemContainer container = containerIterator.next();
            if (targetContainer.equals(container)) return container;
            if (container.getChildrenSet() != null && !container.getChildrenSet().isEmpty())
                result = getContainerFromResultSet(targetContainer, container.getChildrenSet());
        }
        return result;
    }

    public Map<String, List<ItemContainer>> getItemList() {
        addItemsToResult();
        for (String tableName : tableList) {
            mapForSerialize.put(tableName, new ArrayList<>());
        }
        addToSerializeMap(resultSet);

        return mapForSerialize;
    }

    private void addToSerializeMap(Set<ItemContainer> containerSet) {
        for (ItemContainer container : containerSet) {
            mapForSerialize.get(container.getTableName()).add(container);
            if (!container.getChildrenSet().isEmpty()) addToSerializeMap(container.getChildrenSet());
        }
    }
}
