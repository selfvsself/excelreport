package ru.centp.report.impl.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class ReportCommand {

    private int ident;
    private int id;
    private String command;
    private String sheetName;
    private String[] arguments = new String[11];
    private double dataType;
    private String strValue;
    private double numValue;
    private LocalDateTime dateValue;
    private byte[] blobValue;
    private String clobValue;

    public ReportCommand() {
    }

    public int getIdent() {
        return ident;
    }

    public void setIdent(int ident) {
        this.ident = ident;
    }
    public void setIdent(String ident) {
        if (ident != null) this.ident = Integer.parseInt(ident);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public void setId(String id) {
        if (id != null) this.id = Integer.parseInt(id);
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public String[] getArguments() {
        return arguments;
    }

    public void setArguments(String[] arguments) {
        if (arguments.length == this.arguments.length)
            this.arguments = arguments;
        else throw new IllegalArgumentException("В SqlCommand должно быть "+this.arguments.length+" агрументов,а не "+arguments.length);
    }

    public double getDataType() {
        return dataType;
    }

    public void setDataType(double dataType) {
        this.dataType = dataType;
    }
    public void setDataType(String dataType) {
        if (dataType != null) this.dataType = Double.parseDouble(dataType);
    }

    public String getStrValue() {
        return strValue;
    }

    public void setStrValue(String strValue) {
        this.strValue = strValue;
    }

    public double getNumValue() {
        return numValue;
    }

    public void setNumValue(double numValue) {
        this.numValue = numValue;
    }
    public void setNumValue(String numValue) {
        if (numValue != null) this.numValue = Double.parseDouble(numValue);
    }

    public Date getDateValue() {
        return Date.from(dateValue.toLocalDate().atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public void setDateValue(LocalDateTime dateValue) {
        this.dateValue = dateValue;
    }

    public byte[] getBlobValue() {
        return blobValue;
    }

    public void setBlobValue(byte[] blobValue) {
        this.blobValue = blobValue;
    }

    public String getClobValue() {
        return clobValue;
    }

    public void setClobValue(String clobValue) {
        this.clobValue = clobValue;
    }
}
