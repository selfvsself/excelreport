package ru.centp.report.impl.model;

import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;

public class DataValidationContainer {

    private final DataValidationConstraint dataValidationConstraint;
    private final CellRangeAddressList cellRangeAddressList;
    private boolean isEmptyCellAllowed;
    private String errorBoxText;
    private String errorBoxTitle;
    private int errorStyle;
    private String promptBoxTitle;
    private String promptBoxText;
    private boolean isShowErrorBox;
    private boolean isShowPromptBox;
    private boolean isSuppressDropDownArrow;

    public DataValidationContainer(DataValidation dataValidation) {
        this(dataValidation.getValidationConstraint(), dataValidation.getRegions());
        setEmptyCellAllowed(dataValidation.getEmptyCellAllowed());
        setErrorBoxText(dataValidation.getErrorBoxText());
        setErrorBoxTitle(dataValidation.getErrorBoxTitle());
        setErrorStyle(dataValidation.getErrorStyle());
        setPromptBoxText(dataValidation.getPromptBoxText());
        setPromptBoxTitle(dataValidation.getPromptBoxTitle());
        setShowErrorBox(dataValidation.getShowErrorBox());
        setShowPromptBox(dataValidation.getShowPromptBox());
        setSuppressDropDownArrow(dataValidation.getSuppressDropDownArrow());
    }

    public DataValidationContainer(DataValidationContainer dataValidation) {
        this(dataValidation.getDataValidationConstraint(), dataValidation.getCellRangeAddressList());
        setEmptyCellAllowed(dataValidation.isEmptyCellAllowed());
        setErrorBoxText(dataValidation.getErrorBoxText());
        setErrorBoxTitle(dataValidation.getErrorBoxTitle());
        setErrorStyle(dataValidation.getErrorStyle());
        setPromptBoxText(dataValidation.getPromptBoxText());
        setPromptBoxTitle(dataValidation.getPromptBoxTitle());
        setShowErrorBox(dataValidation.isShowErrorBox());
        setShowPromptBox(dataValidation.isShowPromptBox());
        setSuppressDropDownArrow(dataValidation.isSuppressDropDownArrow());
    }

    public DataValidationContainer(DataValidationContainer dataValidation, CellRangeAddressList cellRangeAddressList) {
        this(dataValidation.getDataValidationConstraint(), cellRangeAddressList);
        setEmptyCellAllowed(dataValidation.isEmptyCellAllowed());
        setErrorBoxText(dataValidation.getErrorBoxText());
        setErrorBoxTitle(dataValidation.getErrorBoxTitle());
        setErrorStyle(dataValidation.getErrorStyle());
        setPromptBoxText(dataValidation.getPromptBoxText());
        setPromptBoxTitle(dataValidation.getPromptBoxTitle());
        setShowErrorBox(dataValidation.isShowErrorBox());
        setShowPromptBox(dataValidation.isShowPromptBox());
        setSuppressDropDownArrow(dataValidation.isSuppressDropDownArrow());
    }

    private DataValidationContainer(DataValidationConstraint dataValidationConstraint, CellRangeAddressList _cellRangeAddressList) {
        this.dataValidationConstraint = dataValidationConstraint;
        cellRangeAddressList = new CellRangeAddressList();
        for (CellRangeAddress address : _cellRangeAddressList.getCellRangeAddresses()) {
            cellRangeAddressList.addCellRangeAddress(address);
        }
    }

    public DataValidationConstraint getDataValidationConstraint() {
        return dataValidationConstraint;
    }

    public CellRangeAddressList getCellRangeAddressList() {
        return cellRangeAddressList;
    }

    public void addCellRangeAddress(CellRangeAddress cellRangeAddress) {
        cellRangeAddressList.addCellRangeAddress(cellRangeAddress);
    }

    public void removeCellRangeAddress(CellRangeAddress cellRangeAddress) {
        int sizeCellAddress = cellRangeAddressList.countRanges();
        for (int i = 0; i < sizeCellAddress; i++) {
            if (cellRangeAddressList.getCellRangeAddress(i).equals(cellRangeAddress)) {
                cellRangeAddressList.remove(i);
                break;
            }
        }
    }

    public boolean isEmptyCellAllowed() {
        return isEmptyCellAllowed;
    }

    public void setEmptyCellAllowed(boolean emptyCellAllowed) {
        isEmptyCellAllowed = emptyCellAllowed;
    }

    public String getErrorBoxText() {
        return errorBoxText;
    }

    public void setErrorBoxText(String errorBoxText) {
        this.errorBoxText = errorBoxText;
    }

    public String getErrorBoxTitle() {
        return errorBoxTitle;
    }

    public void setErrorBoxTitle(String errorBoxTitle) {
        this.errorBoxTitle = errorBoxTitle;
    }

    public int getErrorStyle() {
        return errorStyle;
    }

    public void setErrorStyle(int errorStyle) {
        this.errorStyle = errorStyle;
    }

    public String getPromptBoxTitle() {
        return promptBoxTitle;
    }

    public void setPromptBoxTitle(String promptBoxTitle) {
        this.promptBoxTitle = promptBoxTitle;
    }

    public String getPromptBoxText() {
        return promptBoxText;
    }

    public void setPromptBoxText(String promptBoxText) {
        this.promptBoxText = promptBoxText;
    }

    public boolean isShowErrorBox() {
        return isShowErrorBox;
    }

    public void setShowErrorBox(boolean showErrorBox) {
        isShowErrorBox = showErrorBox;
    }

    public boolean isShowPromptBox() {
        return isShowPromptBox;
    }

    public void setShowPromptBox(boolean showPromptBox) {
        isShowPromptBox = showPromptBox;
    }

    public boolean isSuppressDropDownArrow() {
        return isSuppressDropDownArrow;
    }

    public void setSuppressDropDownArrow(boolean suppressDropDownArrow) {
        isSuppressDropDownArrow = suppressDropDownArrow;
    }
}
