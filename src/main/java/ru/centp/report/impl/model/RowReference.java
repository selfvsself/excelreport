package ru.centp.report.impl.model;

import org.apache.poi.ss.usermodel.Name;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RowReference {

    //Пример АО!$18:$18 или АО!$A$20:$XFD$20
    private static final Pattern ROW_REF_PATTERN = Pattern.compile("^.+!(\\$\\w+)?\\$\\d+:(\\$\\w+)?\\$\\d+$");
    private int beginRow;
    private int endRow;
    private String sheetName;

    public RowReference(String rowRef) {
        parseRowRef(rowRef);
    }
    public RowReference(String sheetName, int beginRow, int endRow) {
        this.sheetName = sheetName;
        this.beginRow = beginRow;
        this.endRow = endRow;
    }

    public int getBeginRow() {
        return beginRow;
    }

    public int getEndRow() {
        return endRow;
    }

    public String getSheetName() {
        return sheetName;
    }

    public int getSizeSelectedRows() {
        return endRow - beginRow + 1;
    }

    private void parseRowRef(String rowRef) {
        Matcher matcher = ROW_REF_PATTERN.matcher(rowRef);
        if (!matcher.find())
            throw new IllegalArgumentException("Не правильная ссылка для RowReference: " + rowRef);

        Matcher sheetMatcher = Pattern.compile(".+!").matcher(rowRef);
        if (sheetMatcher.find()) {
            sheetName = rowRef.substring(sheetMatcher.start(), sheetMatcher.end() - 1);
        } else throw new IllegalArgumentException("Не правильная ссылка для RowReference: " + rowRef);
        try {
            Matcher rowMatcher = Pattern.compile("\\$\\d+").matcher(rowRef);
            if (rowMatcher.find()) {
                beginRow = Integer.parseInt(rowRef.substring(rowMatcher.start() + 1, rowMatcher.end()));
            } else throw new IllegalArgumentException("Не правильная ссылка для RowReference: " + rowRef);
            if (rowMatcher.find()) {
                endRow = Integer.parseInt(rowRef.substring(rowMatcher.start() + 1, rowMatcher.end()));
            } else throw new IllegalArgumentException("Не правильная ссылка для RowReference: " + rowRef);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Не правильная ссылка для RowReference: " + rowRef);
        }
    }

    public String getRefersToFormula() {
        return String.format("%s!$%d:$%d", sheetName, getBeginRow(), getEndRow());
    };


    public boolean isRowReference(Name name) {
        Matcher matcher = ROW_REF_PATTERN.matcher(name.getRefersToFormula());
        return matcher.find();
    }
}
