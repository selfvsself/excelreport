package ru.centp.report.impl.model;

import java.util.*;

public class ItemContainer {

    private final Map<String, String> params;
    private final String tableName;
    private final Set<ItemContainer> childrenSet;
    private ItemContainer parent = null;

    public ItemContainer(String tableName) {
        this.params = new HashMap<>();
        this.childrenSet = new LinkedHashSet<>();
        this.tableName = tableName;
    }

    public void addParam(String column, String value) {
        params.put(column, value);
    }

    public void addChildren(ItemContainer itemContainer) {
        childrenSet.add(itemContainer);
        itemContainer.setParent(this);
    }

    protected void setParent(ItemContainer parent) {
        this.parent = parent;
    }

    public ItemContainer getParent() {
        return parent;
    }

    public String getTableName() {
        return tableName;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public Set<ItemContainer> getChildrenSet() {
        return childrenSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemContainer that = (ItemContainer) o;
        return Objects.equals(params, that.params);
    }

    @Override
    public int hashCode() {
        return Objects.hash(params);
    }
}
