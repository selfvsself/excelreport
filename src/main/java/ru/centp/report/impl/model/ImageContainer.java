package ru.centp.report.impl.model;

import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.util.CellReference;

import java.util.*;

public class ImageContainer {

    private final byte[] image;
    private final List<AreaReference> cells;
    private final String sheetName;

    public ImageContainer(byte[] image, String sheetName) {
        if (image == null) throw new NullPointerException("Ошибка чтения изображения, массив байтов равно null");
        if (sheetName == null) throw new NullPointerException("Ошибка чтения изображения, название стараницы равно null");
        this.image = image;
        this.sheetName = sheetName;
        cells = new LinkedList<>();
    }

    public void addCellReference(AreaReference reference) {
        cells.add(reference);
    }

    public byte[] getImage() {
        return image;
    }

    public String getSheetName() {
        return sheetName;
    }

    public List<AreaReference> getCells() {
        return cells;
    }

    public void addRows(int createdRowNum, int startCopyRowNum, int endCopyRowNum) {
        int addedRowsSize = endCopyRowNum - startCopyRowNum + 1;
        ListIterator<AreaReference> areaIterator = cells.listIterator();
        while (areaIterator.hasNext()) {
            AreaReference reference = areaIterator.next();
            int firstRefRow = reference.getFirstCell().getRow();
            int lastRefRow = reference.getLastCell().getRow();
            if (firstRefRow >= createdRowNum) {
                areaIterator.remove();
                reference = new AreaReference(
                        new CellReference(firstRefRow + addedRowsSize, reference.getFirstCell().getCol()),
                        new CellReference(lastRefRow + addedRowsSize, reference.getLastCell().getCol())
                );
                areaIterator.add(reference);
            }

        }
    }

    public void removeRows(int delRowNum, int delRowSize) {
        ListIterator<AreaReference> areaIterator = cells.listIterator();
        while (areaIterator.hasNext()) {
            AreaReference reference = areaIterator.next();
            int firstRefRow = reference.getFirstCell().getRow();
            int lastRefRow = reference.getLastCell().getRow();
            if (firstRefRow >= delRowNum) {
                areaIterator.remove();
                reference = new AreaReference(
                        new CellReference(firstRefRow - delRowSize, reference.getFirstCell().getCol()),
                        new CellReference(lastRefRow - delRowSize, reference.getLastCell().getCol())
                );
                areaIterator.add(reference);
            }
        }
    }

    public void addCols(int createdColNum, int startCopyColNum, int endCopyColNum) {
        int addedRowsSize = endCopyColNum - startCopyColNum + 1;
        ListIterator<AreaReference> areaIterator = cells.listIterator();
        while (areaIterator.hasNext()) {
            AreaReference reference = areaIterator.next();
            int firstRefCol = reference.getFirstCell().getCol();
            int lastRefCol = reference.getLastCell().getCol();
            if (firstRefCol >= createdColNum) {
                areaIterator.remove();
                reference = new AreaReference(
                        new CellReference(reference.getFirstCell().getRow(), firstRefCol + addedRowsSize),
                        new CellReference(reference.getLastCell().getRow(), lastRefCol + addedRowsSize)
                );
                areaIterator.add(reference);
            }

        }
    }

    public void removeCols(int delColNum, int delColSize) {
        ListIterator<AreaReference> areaIterator = cells.listIterator();
        while (areaIterator.hasNext()) {
            AreaReference reference = areaIterator.next();
            int firstRefCol = reference.getFirstCell().getCol();
            int lastRefCol = reference.getLastCell().getCol();
            if (firstRefCol >= delColNum) {
                areaIterator.remove();
                reference = new AreaReference(
                        new CellReference(reference.getFirstCell().getRow(), firstRefCol - delColSize),
                        new CellReference(reference.getLastCell().getRow(), lastRefCol - delColSize)
                );
                areaIterator.add(reference);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImageContainer that = (ImageContainer) o;
        return Arrays.equals(image, that.image) &&
                sheetName.equals(that.sheetName);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(sheetName);
        result = 31 * result + Arrays.hashCode(image);
        return result;
    }
}
