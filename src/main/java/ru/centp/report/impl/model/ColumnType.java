package ru.centp.report.impl.model;

public enum ColumnType {
    STRING,
    NUMBER,
    BOOLEAN,
    DATE,
    DATETIME,
    TIME,
    HIDE_TEXT
}
