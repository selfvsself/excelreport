package ru.centp.report.impl.model;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormulaParser {

    private List<FormulaElement> elements;
    private StringBuilder template;
    private final String patternElement = "([^&,/!(=:;*+\\-\"]+!)?\\$?[A-Z]+\\$?\\d+";

    public FormulaParser(String formula) {
        elements = new ArrayList<>();
        List<Integer> startIndices = new ArrayList<>();
        List<Integer> endIndices = new ArrayList<>();
        Pattern pattern = Pattern.compile(patternElement);
        Matcher matcher = pattern.matcher(formula);
        template = new StringBuilder(formula);
        while (matcher.find()) {
            String element = formula.substring(matcher.start(), matcher.end());
            startIndices.add(matcher.start());
            endIndices.add(matcher.end());
            FormulaElement formulaElement = new FormulaElement(element);
            if (matcher.start() > 0) {
                char isRange = formula.charAt(matcher.start() - 1);
                if (formulaElement.getSheetName() == null
                        && isRange == ':')
                    formulaElement.setSheetName(elements.get(elements.size() - 1).getSheetName());
            }
            elements.add(formulaElement);
        }
        for (int i = startIndices.size() - 1; i >= 0; i--) {
            int startIndex = startIndices.get(i);
            int endIndex = endIndices.get(i);
            template.replace(startIndex, endIndex, "%{№" + i + "}");
        }
    }

    public List<FormulaElement> getElements() {
        return elements;
    }

    public String formatAsString() {
        StringBuilder result = new StringBuilder(template);
        for (int i = 0; i < elements.size(); i++) {
            String tmp = "%{№" + i + "}";
            int startIndex = result.indexOf(tmp);
            int endIndex = startIndex + tmp.length();
            FormulaElement element = elements.get(i);
            result.replace(startIndex, endIndex, element.toString());
        }
        return result.toString();
    }
}
