package ru.centp.report.impl.model;

import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.util.CellReference;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ColumnReference {

    //Пример АО!$B:$B или АО!$A$20:$XFD$20
    private static final Pattern ROW_REF_PATTERN = Pattern.compile("^.+!\\$[^\\d$]+(\\$\\d+)?:\\$[^\\d$]+(\\$\\d+)?$");
    private int beginColumn;
    private int endColumn;
    private String sheetName;

    public ColumnReference(String columnRef) {
        parseColumnRef(columnRef);
    }

    private ColumnReference(String sheetName, int beginRow, int endRow) {
        this.sheetName = sheetName;
        this.beginColumn = beginRow;
        this.endColumn = endRow;
    }

    public ColumnReference getClone(ColumnReference srcColumnReference) {
        return new ColumnReference(srcColumnReference.getSheetName(), srcColumnReference.getBeginColumn(), srcColumnReference.getEndColumn());
    }

    public int getBeginColumn() {
        return beginColumn;
    }

    public int getEndColumn() {
        return endColumn;
    }

    public void setBeginColumn(int beginColumn) {
        this.beginColumn = beginColumn;
    }

    public void setEndColumn(int endColumn) {
        this.endColumn = endColumn;
    }

    public String getSheetName() {
        return sheetName;
    }

    public int getSizeSelectedColumns() {
        return endColumn - beginColumn + 1;
    }

    private void parseColumnRef(String columnRef) {
        Matcher matcher = ROW_REF_PATTERN.matcher(columnRef);
        if (!matcher.find())
            throw new IllegalArgumentException("Не правильная ссылка для ColumnReference: " + columnRef);

        Matcher sheetMatcher = Pattern.compile(".+!").matcher(columnRef);
        if (sheetMatcher.find()) {
            sheetName = columnRef.substring(sheetMatcher.start(), sheetMatcher.end() - 1);
        } else throw new IllegalArgumentException("Не правильная ссылка для ColumnReference: " + columnRef);
        Matcher columnMatcher = Pattern.compile("\\$[^\\d$:]+").matcher(columnRef);
        if (columnMatcher.find()) {
            String refColumn = columnRef.substring(columnMatcher.start() + 1, columnMatcher.end());
            beginColumn = CellReference.convertColStringToIndex(refColumn);
        } else throw new IllegalArgumentException("Не правильная ссылка для ColumnReference: " + columnRef);
        if (columnMatcher.find()) {
            String refColumn = columnRef.substring(columnMatcher.start() + 1, columnMatcher.end());
            endColumn = CellReference.convertColStringToIndex(refColumn);
        } else throw new IllegalArgumentException("Не правильная ссылка для ColumnReference: " + columnRef);
    }

    public boolean isColumnReference(Name name) {
        Matcher matcher = ROW_REF_PATTERN.matcher(name.getRefersToFormula());
        return matcher.find();
    }
}
