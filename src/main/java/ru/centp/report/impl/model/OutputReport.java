package ru.centp.report.impl.model;

public class OutputReport {

    private byte[] content;
    private final String fileName;

    public OutputReport(byte[] content, String fileName) {
        this.content = content;
        this.fileName = fileName;
    }

    public byte[] getContent() {
        return content;
    }

    public String getFileName() {
        return fileName;
    }
}
