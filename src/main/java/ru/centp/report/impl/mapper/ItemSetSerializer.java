package ru.centp.report.impl.mapper;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import ru.centp.report.impl.model.ItemContainer;
import ru.centp.report.impl.model.ItemProvider;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class ItemSetSerializer extends StdSerializer<ItemProvider> {

    public ItemSetSerializer() {
        this(null);
    }

    public ItemSetSerializer(Class<ItemProvider> t) {
        super(t);
    }

    @Override
    public void serialize(ItemProvider itemProvider, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {

        jsonGenerator.writeStartObject();

        jsonGenerator.writeArrayFieldStart("DATABASE_NAME");
        jsonGenerator.writeString(itemProvider.getDatabaseName());
        jsonGenerator.writeEndArray();

        for (Map.Entry<String, List<ItemContainer>> containerList : itemProvider.getItemList().entrySet()) {
            jsonGenerator.writeArrayFieldStart(containerList.getKey());

            for (ItemContainer container : containerList.getValue()) {
                jsonGenerator.writeStartObject();
                for (Map.Entry<String, String> entry : container.getParams().entrySet()) {
                    jsonGenerator.writeStringField(entry.getKey(), entry.getValue());
                }
                jsonGenerator.writeEndObject();
            }

            jsonGenerator.writeEndArray();
        }
        jsonGenerator.writeEndObject();
    }
}
