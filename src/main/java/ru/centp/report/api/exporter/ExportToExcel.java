package ru.centp.report.api.exporter;

public interface ExportToExcel {

    byte[] getContent();
    void setSheetName(String sheetName);
}
