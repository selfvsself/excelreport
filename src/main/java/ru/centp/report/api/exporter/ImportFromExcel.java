package ru.centp.report.api.exporter;

public interface ImportFromExcel {

    String getJsonQuery();
}
