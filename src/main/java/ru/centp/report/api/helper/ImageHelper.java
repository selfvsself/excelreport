package ru.centp.report.api.helper;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.AreaReference;

public interface ImageHelper {

    void addImage(byte[] image, AreaReference reference, String sheetName);

    void addRows(String sheetName, AreaReference copiedReference, int createdRowNum);
    void removeRows(String sheetName, int delRowNum, int delRowSize);
    void addCols(String sheetName, AreaReference copiedReference, int createdColNum);
    void removeCols(String sheetName, int delColNum, int delColSize);

    void renderAllImage(Workbook workbook);
}
