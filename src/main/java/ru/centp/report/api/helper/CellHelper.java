package ru.centp.report.api.helper;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public interface CellHelper {
    public static final String PATTERN_RELATIVE_FORMULA = "R(\\[-?\\d+])?C(\\[-?\\d+])?";
    public static final String PATTERN_RELATIVE_CELL_ADDRESS = "^R(\\[?-?\\d+]?)?C(\\[?-?\\d+]?)?$";
    public static final String PATTERN_CELL_ADDRESS = "^[A-Z]+[0-9]+$";
    public static final String PATTERN_LINE_ADDRESS = "^\\d+:\\d+$";
    public static final String PATTERN_COLUMN_ADDRESS = "^[A-Z]+:[A-Z]$";

    Cell getCellByName(Workbook workbook, String cellName);
    Cell getCellByNameAndIndexRow(Workbook workbook, String cellName, String link);
    Cell getCellByNameAndIndexColumn(Workbook workbook, String cellName, String link);
    Cell getCellByReference(Workbook workbook, CellReference reference);
    default Cell getCellByRelativeReference(Workbook workbook, String sheetName, String relativeReference) {
        relativeReference = relativeReference.trim();

        Pattern patternRow = Pattern.compile("R\\[?-?\\d+]?");
        Matcher matcherRow = patternRow.matcher(relativeReference);
        int relRow = 0;
        if (matcherRow.find()) {
            String tmpValue = relativeReference.trim().substring(matcherRow.start(), matcherRow.end())
                    .replace("R", "")
                    .replace("[", "")
                    .replace("]","");
            if (tmpValue.isEmpty()) {
                tmpValue = "1";
            }
            relRow = Integer.parseInt(tmpValue) - 1;
        }
        Pattern patternColumn = Pattern.compile("C\\[?-?\\d+]?");
        Matcher matcherColumn = patternColumn.matcher(relativeReference);
        int relCol = 0;
        if (matcherColumn.find()) {
            String tmpValue = relativeReference.trim().substring(matcherColumn.start(), matcherColumn.end())
                    .replace("C", "")
                    .replace("[", "")
                    .replace("]","");
            if (tmpValue.isEmpty()) {
                tmpValue = "1";
            }
            relCol = Integer.parseInt(tmpValue) - 1;
        }

        Sheet sheet = workbook.getSheet(sheetName);
        Row row = sheet.getRow(relRow);
        if (row == null) row = sheet.createRow(relRow);
        Cell cell = row.getCell(relCol);
        if (cell == null) cell = row.createCell(relCol);

        return cell;
    }
    String covertRelativeFormulaToAbsolute(Cell destCell, String relativeFormula);
    CellStyle setUpDiagonalBorder(BorderStyle borderStyle, Cell cell);
    CellStyle setBottomDiagonalBorder(BorderStyle borderStyle, Cell cell);
    BorderStyle getDiagonalBorderStyle(Cell cell);
    CellStyle setDiagonalColor(Cell cell, Color color);
}
