package ru.centp.report.api.helper;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public interface SheetHelper {
    void restoreMergedRegions(Workbook workbook);
    void restoreAllFormulas(Workbook workbook);
    void restoreAllDataValidation(Workbook workbook);
    Cell getHelperCellForAutoHeightMergedCell(String sheetName, Cell cell);
    void deleteFormula(Cell cell);
    void addFormula(Cell cell);
    void insertRows(Sheet _sheet, int desRow, int n);
    void copyRows(Sheet _sheet, int starRow, int endRow, int destRowNum);
    void insertColumns(Sheet _sheet, int desColumn, int n);
    void copyColumns(Sheet _sheet, int startColumn, int endColumn, int destColumnNum);
    void removeRows(Sheet _sheet, int destRowNum, int n);
    void removeColumns(Sheet _sheet, int destColumnNum, int n);
    void copySheet(String sheetName, String newSheetName);
    void deleteSheet(String sheetName);
}
