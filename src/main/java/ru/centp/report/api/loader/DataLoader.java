package ru.centp.report.api.loader;

import ru.centp.report.impl.model.ReportCommand;

import java.util.List;

public interface DataLoader {

    List<ReportCommand> getCommands();
}
