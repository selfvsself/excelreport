package ru.centp.report.api.reporting;

import ru.centp.report.impl.model.OutputReport;

public interface Reporting {

    OutputReport getOutputReport();
}
