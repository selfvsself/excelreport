package ru.centp.report;

import java.util.*;

public class TestExporter {

    private static Map<String, Map<String, String>> table = new LinkedHashMap();
    private static List<Map<String,String>> recordDto = new ArrayList<>();

    public static void main(String[] args) {
        fillTable();
        fillList();



    }

    private static void fillList() {

    }

    private static void fillTable() {
        Map<String, String> convTable = new LinkedHashMap<>();
        convTable.put("1", "111");
        convTable.put("2", "456");
        Map<String, String> convactsTable = new LinkedHashMap<>();
        convactsTable.put("1", "123");
        convactsTable.put("3", "234");
        convactsTable.put("4", "fgfgfg");
        Map<String, String> convactparamsTable = new LinkedHashMap<>();
        convactparamsTable.put("7", "ID");
        convactparamsTable.put("8", "HID");
        convactparamsTable.put("25", "ID");
        convactparamsTable.put("26", "HID");
        convactparamsTable.put("37", "ID");
        convactparamsTable.put("38", "HID");
        table.put("convTable", convTable);
        table.put("convactsTable", convactsTable);
        table.put("convactparamsTable", convactparamsTable);
    }
}
