package ru.centp.report;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.thirdparty.json.JSONArray;
import com.google.gwt.thirdparty.json.JSONException;
import org.apache.poi.ss.usermodel.SheetVisibility;
import org.postgresql.ds.PGPoolingDataSource;
import ru.centp.report.impl.model.ColumnItem;
import ru.centp.report.impl.model.ColumnType;
import ru.centp.report.impl.model.FormulaElement;
import ru.centp.report.impl.model.FormulaParser;
import sun.text.normalizer.UTF16;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OtherTest {


    public static void main(String[] args) throws InterruptedException, JSONException, IOException {
        String ident = null;

        String answer = (ident != null && !ident.isEmpty()) ? ident : "0" ;

        System.out.println(answer);


    }

    public static String getStr() {
        try {
            String str = "hello";
            return str;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return "333";
        }
    }

    private static List<String> idList = new ArrayList<>();

    private static void setIdList(List<String> result) {
        //Для подсчета процента выполнения
        int size = result.size();
        int step = (int) Math.ceil((double) size / 2);
        int stepProgress = 0;

        StringBuilder stringId = new StringBuilder();
        for (String map : result) {

            //Добавляем id в лист, что бы потом удалить эту запись в excel_commands
            if (stringId.length() == 0) stringId.append(map);
            else stringId.append(",").append(map);

            //Подсчитываем процент выполнения
            stepProgress++;
            if (stepProgress >= step) {
                stepProgress = 0;
                idList.add(stringId.toString());
                stringId.setLength(0);
            }
        }
    }

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }
}
