package ru.centp.report;

import org.postgresql.ds.PGPoolingDataSource;
import ru.centp.report.api.loader.DataLoader;
import ru.centp.report.impl.loader.SqlQueryDataLoader;
import ru.centp.report.impl.reporting.XLSReporting;
import ru.centp.report.impl.reporting.XLSXReporting;

import java.io.File;
import java.time.LocalDateTime;

public class XLSTestingRun {

    static final File fileMaket = new File("C:\\webapps\\data\\reports\\intmoveGoogs.xls");
    static final File savedInFile = new File("C:\\webapps\\data\\reports\\test3.xls");

    public static void main(String[] args) {
        PGPoolingDataSource source = new PGPoolingDataSource();
        source.setDataSourceName("testPool");
        source.setServerName("127.0.0.1:5432");
        source.setDatabaseName("vegaplus");
        source.setUser("magicbox");
        source.setPassword("magicbox");
        source.setMaxConnections(10);

        System.out.println(LocalDateTime.now().toString());
        System.out.println("Read file: " + fileMaket.getPath());
        DataLoader dataLoader = new SqlQueryDataLoader(source);
        XLSReporting reporting = new XLSReporting(dataLoader, fileMaket);
        reporting.getOutputReport();
        reporting.saveWorkbook(savedInFile.getPath());
        System.out.println("Saved in file: " + savedInFile.getPath());
        System.out.println(LocalDateTime.now().toString());




    }
}
